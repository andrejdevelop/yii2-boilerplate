<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 10:55 AM
 */

namespace tests\codeception\unit\models;

use app\models\User;
use taktwerk\yiiboilerplate\modules\import\models\ImportProgress;
use taktwerk\yiiboilerplate\modules\import\models\Import;
use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\user\models\Profile;
use taktwerk\yiiboilerplatetests\fixtures\UserImportFixtures;
use yii\codeception\TestCase;
use taktwerk\yiiboilerplate\modules\import\commands\ImportCommand;

class ImporterTest extends TestCase
{
    public $appConfig = '@tests/codeception/_config/unit.php';

    protected $uploadedFile;

    public function fixtures()
    {
        return [
            'User' => UserImportFixtures::className(),
        ];
    }

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Test user import process and job queue
     */
    public function testUserBackgroundImport()
    {
        Notification::deleteAll();
        ImportProgress::deleteAll();
        Profile::deleteAll();
        $this->uploadedFile = 'user_csv_import.csv';
        $this->uploadFile();
        $data = [
            'model' => User::className(),
            'file_name' => $this->uploadedFile,
            'delimiter' => ',',
            'attributes' => $this->getMap(),
            'foreign' => [],
            'method' => 'insert',
        ];
        $job = new QueueJob();
        $job->command = 'taktwerk\yiiboilerplate\modules\import\commands\ImportCommand';
        $job->title = 'Import test';
        $job->parameters = json_encode(array_merge(['user_id' => User::find()->one()->id], $data));
        $job->status = QueueJob::STATUS_QUEUED;
        $job->save();

        $command = new ImportCommand();
        $command->run($job);
        $users = User::find()->all();
        // Verifiy there are more than 4 users
        $this->assertGreaterThanOrEqual(4, count($users));
        // Check if job is set to finished
        $this->assertEquals(QueueJob::STATUS_FINISHED, $job->status);
    }

    /**
     *
     */
    public function testUserFrontendImport()
    {
        Notification::deleteAll();
        ImportProgress::deleteAll();
        Profile::deleteAll();
        $this->uploadedFile = 'user_csv_import_2.csv';
        $this->uploadFile();
        $userId = User::find()->one()->id;
        $importer = new Import();
        $importer->userId = $userId;
        $importer->setPostData([
            'model' => User::className(),
            'file_name' => $this->uploadedFile,
            'delimiter' => 'auto',
            'attributes' => $this->getMap(),
            'foreign' => [],
            'method' => 'insert',
        ]);
        $result = $importer->process();
        // Is return true
        $this->assertEquals('success', $result['success']);
        // Is added entries equal 4
        $this->assertEquals(4, $result['total']);
        $importProgress = ImportProgress::find()->andWhere(['user_id' => $userId, 'model' => User::className()])->one();
        $this->assertEquals(ImportPRogress::STATUS_FINISHED, $importProgress->status);
    }

    /**
     * Test if notification is sent
     */
    public function testNotification()
    {
        $userId = User::find()->one()->id;
        $notification = Notification::find()->andWhere(['user_id' => $userId])->one();
        $this->assertNotFalse($notification);
    }

    /**
     * Imitate upload file by coping to desired destination
     */
    protected function uploadFile()
    {
        $userId = User::find()->one()->id;
        @mkdir(\Yii::getAlias('@root/runtime/uploads'));
        @mkdir(\Yii::getAlias('@root/runtime/uploads/' . $userId));
        copy(
            __DIR__ . '/../data/user/' . $this->uploadedFile,
            \Yii::getAlias('@root/runtime/uploads/' . $userId . '/' . md5($this->uploadedFile))
        );
    }

    /**
     * Map attributes to file columns in proper way
     * @return array
     */
    protected function getMap()
    {
        $mappedAttributes = [];
        $attributes = Import::getModelAttributes(User::className());
        foreach ($attributes as $key => $attribute) {
            $mappedAttributes[$attribute['name']] = $key;
        }
        return $mappedAttributes;
    }
}