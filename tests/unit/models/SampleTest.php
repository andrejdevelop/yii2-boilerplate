<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 10:55 AM
 */

namespace tests\codeception\unit\models;

use app\models\User;
use taktwerk\yiiboilerplate\modules\import\models\ImportProgress;
use taktwerk\yiiboilerplate\modules\import\models\Import;
use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\user\models\Profile;
use tests\codeception\fixtures\UserFixtures;
use yii\codeception\TestCase;
use taktwerk\yiiboilerplate\modules\import\commands\ImportCommand;

class SampleTest extends TestCase
{
    public $appConfig = '@tests/codeception/_config/unit.php';

    protected function setUp()
    {
        parent::setUp();
    }

    public function testSample()
    {
        $this->assertEquals(1, 1);
    }

}