<?php

namespace taktwerk\yiiboilerplatetests\unit\models;

use app\models\User;
use taktwerk\yiiboilerplatetests\fixtures\UserImportFixtures;
use yii\codeception\TestCase;

class UserTest extends TestCase
{

    public $appConfig = '@tests/codeception/_config/unit.php';

    public function fixtures()
    {
        return [
            'User' => 'taktwerk\yiiboilerplatetests\fixtures\UserImportFixtures',
        ];
    }

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * @group mandatory
     */
    public function testUserLogin()
    {

        $identity = User::find()->one();
        $this->assertTrue(\Yii::$app->user->login($identity, 3600));
    }

//    /**
//     * @group mandatory
//     */
//    public function testNonExistingUserModel()
//    {
//        $identity = User::findIdentity(99999);
//        $this->assertNull($identity);
//    }
//
//    /**
//     * @group mandatory
//     */
//    public function testUserLogout()
//    {
//        $this->assertTrue(\Yii::$app->user->logout());
//    }

}
