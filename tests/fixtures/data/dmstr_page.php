<?php
use taktwerk\pages\models\Tree;

return [
    'Tree1' => [
        'id' => '1',
        'root' => 'en',
        'name' => 'name 1',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
    ],
    'Tree2' => [

        'id' => '2',
        'root' => 'en',
        'name' => 'name 2',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
    ],
    'Tree3' => [

        'id' => '3',
        'root' => 'en',
        'name' => 'name 3',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
    ],
    'Tree4' => [

        'id' => '4',
        'root' => 'en',
        'name' => 'name 4',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
    ],
];
