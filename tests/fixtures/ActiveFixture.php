<?php

namespace taktwerk\yiiboilerplatetests\fixtures;

use yii\test\ActiveFixture as BaseActiveFixture;

class ActiveFixture extends BaseActiveFixture
{

    public function beforeLoad()
    {
        parent::beforeLoad();
        $this->db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 0')->execute();
    }

//    public function afterLoad() {
//        parent::afterLoad();
//        $this->db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 1')->execute();
//    }
}