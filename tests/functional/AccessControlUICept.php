<?php

// @group mandatory

use tests\codeception\_pages\LoginPage;

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that access control UI works');

$I->amOnPage('/');
$I->dontSeeLink('/backend', '.nav');
$I->dontSee('','.fa.fa-cogs');

LoginPage::openBy($I)->login('admin', 'passepartout.0');
$I->amOnPage('/backend');
$I->see('','.fa.fa-cogs');
