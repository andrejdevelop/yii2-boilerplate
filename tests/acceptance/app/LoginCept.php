<?php

// @group mandatory
/**
 * @var \taktwerk\yiiboilerplate\modules\user\Module $userModule
 */
use tests\codeception\_pages\LoginPage;

$I = new AcceptanceTester($scenario);

$fixtures = new \tests\codeception\_support\FixtureHelper();
$fixtures->unloadFixtures();
$fixtures->loadFixtures();

$I->wantTo('ensure login page works');

$I->amGoingTo('try to view login page');
$I->amOnPage('/user/login');
//$I->canSeeResponseCodeIs(200);
$I->seeElement('button', ['type' => 'submit']);
$I->seeElement('input', ['name' => 'login-form[login]']);
$I->seeElement('input', ['name' => 'login-form[password]']);



$I->wantTo('ensure login page by username works');
$I->amGoingTo('try to view login page by username');
$I->amOnPage('/user/login-username');
$userModule = Yii::$app->getModule('user');
//$I->canSeeResponseCodeIs(200);
if ($userModule->enableUsernameOnlyLogin) {
    $method = 'seeElement';
} else {
    $method = 'dontSeeElement';
}
$I->$method('button', ['class' => 'btn btn-default number-button', 'data-num' => 'DEL']);


$I->wantTo('ensure login page by selection works');
$I->amGoingTo('try to view login page by selection');
$I->amOnPage('/user/login-selection');
$userModule = Yii::$app->getModule('user');
//$I->canSeeResponseCodeIs(200);
if ($userModule->enableSelectionLogin) {
    $method = 'seeElement';
} else {
    $method = 'dontSeeElement';
}
$I->$method('div', ['class' => 'list-view']);


$autoLogin = new \tests\codeception\_support\AutoLogin($I);
$autoLogin->adminLogin();

$fixtures->unloadFixtures();
