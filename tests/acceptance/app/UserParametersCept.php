<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/11/2017
 * Time: 9:45 AM
 */
use tests\codeception\_pages\LoginPage;

$I = new AcceptanceTester($scenario);

$fixtures = new \tests\codeception\_support\FixtureHelper();
$fixtures->unloadFixtures();
$fixtures->loadFixtures();

$autoLogin = new \tests\codeception\_support\AutoLogin($I);
$autoLogin->adminLogin();

$I->wantTo('ensure User Parameters is working');
$I->amOnPage('/user/settings/profile');

if (Yii::$app->hasModule('user-parameter')) {
    $I->seeInSource(Yii::t('app', 'User Parameters'));
} else {
    $I->dontSeeInSource(Yii::t('app', 'User Parameters'));
}

$I->wantTo('ensure User Parameters showing fields if they are declared in config');
if (Yii::$app->hasModule('user-parameter')) {
    $I->amOnPage('/user/settings/parameters');
    foreach (Yii::$app->getModule('user-parameter')->params as $name => $param) {
        $I->seeInSource('name="' . $name . '"');
    }
}

$I->wantTo('ensure that saving working');
$I->click('button[type=submit]');
$I->seeInSource('<div class="error-summary alert alert-error" style="display:none;"');
$fixtures->unloadFixtures();
