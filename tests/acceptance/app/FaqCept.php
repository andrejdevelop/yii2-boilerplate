<?php

use tests\codeception\_pages\LoginPage;

$I = new AcceptanceTester($scenario);

$fixtures = new \tests\codeception\_support\FixtureHelper();
$fixtures->unloadFixtures();
$fixtures->loadFixtures();

$autoLogin = new \tests\codeception\_support\AutoLogin($I);
$autoLogin->adminLogin();

$I->wantTo('ensure FAQ module is working');
$I->amOnPage('/backend');

if (Yii::$app->hasModule('faq')) {
    $I->seeInSource('<span>Faq Manager</span>');
    $I->seeInSource('<span>Faq</span>');
} else {
    $I->dontSeeInSource('<span>Faq Manager</span>');
    $I->dontSeeInSource('<span>Faq</span>');
}

$fixtures->unloadFixtures();
