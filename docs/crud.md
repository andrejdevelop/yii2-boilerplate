# Cruds

## Import
To enable the model importer on a model's index, add the following in the model controller file, usually located in `src/controllers`.
```php
/**
 * Show import option
 * @var bool
 */
protected $importer = true;
```
## Export
Export is always enabled in model datagrids. 

### Customize
@todo

### Disable
@todo

### ACL
@todo

## Modal Editing
To enable modal form editing on a model, add the following in the model controller file, usually located in `src/controllers`.
```php
/**
 * Whether to enable modal editing / viewing
 * @var boolean
 */
protected $useModal = true;
```

## Inline Multi Form
To enable inline form editing on a model, add the following in the model controller file, usually located in `src/controllers`.
```php
/**
 * Whether to enable inline sub-forms
 * @var boolean
 */
protected $inlineForm = true;
```