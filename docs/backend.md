# Backend

The `Backend` module represents part of the application that has limited access to it through the `en/backend` url.
