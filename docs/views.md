# Views

Boilerplate apps are split between a frontend in the `@app` folder, and the backend in the `@vendor/taktwerk/yiiboilerplate/modules/backend` folder.

All backend views are aliased using the `@taktwerk-backend-views` alias, and the backend modules should use the `@taktwerk-backend-views/layouts/main` layout.

## Replacing the main layout

To replace the main layout, copy the `@vendor/taktwerk/yiiboilerplate/src/modules/backend/views/layoiut/main.php` file in `@app/modules/backend/views/layouts` and edit it to your liking.

### Replacing backend menus etc
All other files in the backend (menus, navigation, container) can be edited in the same way.