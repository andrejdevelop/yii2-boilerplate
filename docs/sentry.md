# Sentry

Sentry is used to log errors in our applications. If you wish to activate Sentry on your new application, add the `SENTRY_DSN` variables in your `.env` file, as well as the following in the `main.php`:

```php
'log' => [
    'targets' => [
        // Sentry
        [
            'class' => 'sentry\SentryTarget',
            'enabled' => getenv('SENTRY_ENABLED'),
            'levels' => ['error', 'warning'],
            'dsn' => getenv('SENTRY_DSN')
        ],
```
