# taktwerk Yii2 Boilerplate
This is the official README of taktwerk's Yii2 Boilerplate.

## Installation
### 1. Requirements
The project needs to start from a phundament app.
```bash
composer create-project yiisoft/yii2-app-advanced . 2.0.6
```

### 2. Boilerplate
Add the Boilerplate to the composer.json file
```json
"require": [{
    "taktwerk/yii-boilerplate": "*"
}]
...
"repositories": [
	{
	  "type": "git",
	  "url": "git@bitbucket.org:taktwerk/tw-yii-boilerplate.git",
	  "reference": "package"
	},
    {
        "type": "git",
        "url": "https://bitbucket.org/taktwerk/tw-pages.git",
        "reference": "package"
    }
]
```

#### 2.1. Composer update
Run ```composer update``` in your project-root

### 3 Binding
You need to tell your project to use the Boilerplate in several places.

#### 3.1 config/main.php
Add the following value to the configuration of your app.

```php
'log' => [
    'targets' => [
        // Sentry
        [
            'class' => 'sentry\SentryTarget',
            'enabled' => getenv('SENTRY_ENABLED'),
            'levels' => ['error', 'warning'],
            'dsn' => getenv('SENTRY_DSN')

        ],
```
#### 3.2 config/assets-prod.php
```php
'bundles' => [
	\taktwerk\yiiboilerplate\assets\TwAsset::className()
]
```

#### 3.3 config/bootstrap.php
Add the following at the end of the file
```php
// classmap
Yii::$classMap['yii\base\ArrayableTrait'] = '@app/../vendor/taktwerk/yii-boilerplate/src/traits/ArrayableTrait.php';
Yii::$classMap['yii\helpers\ArrayHelper'] = '@app/vendor/taktwerk/yii-boilerplate/src/helpers/ArrayHelper.php';
```

#### 3.4 Sentry Config (optional)
If you wish to activate Sentry on your new application, add the SENTRY_DSN variables in your `.env` file.

### 4. Migration
Run the migrations with following command in your project-root:
```bash
$ php yii migrate/up
```
> **SPECIAL** the behavior-columns like 'created_at', 'created_by', 'deleted_at', 'deleted_by' will be auto generated per default for every new created migration. You must not add these columns to your new createTable(/* .. */) Calls .

### 5. Domain for API and Backend
Create two domains local on your personal machine and in the virutal machine.

Edit your vagrant-nginx-Config under **config/nginx-config/sites/default.conf** and add following lines:
```bash
server {
   listen       80;
   listen       443 ssl;
   server_name  local.{your-project}.dev;
   root         /srv/www/{your-project}/web;
   include      /etc/nginx/nginx-wp-common.conf;
}
server {
   listen       80;
   listen       443 ssl;
   server_name  local.{your-project}.api;
   root         /srv/www/{your-project}/web;
   include      /etc/nginx/nginx-wp-common.conf;
}
```
#### 5.1 Add Host on Windows
1. Add following lines to your host-File located at: C:\windows\system32\drivers\etc\hosts
```bash
# taktwerk local dev
192.168.50.4 local.taktwerk-{your-project-name}.dev
# taktwerk local api
192.168.50.4 local.taktwerk-{your-project-name}.api
```
> Make sure to replace {your-project-name} with your project-id like for example 'pq-tool' or 'nassag'

##### 5.2 Add Host on Linux/Unix
@todo
### 6 Browser-Ready
Make sure to have pretty url enabled in your .env-File:
```bash
APP_PRETTY_URLS=1
```
Open your dev-Host as defined in 5 like for example:
```http://local.taktwerk-tw-boilerplate.dev```

## Usage
### 1 Login
Login into backend with the following credentials:
> username:  admin
> password: admin

### 2 Giiant Model & CRUD Generation
Open your gii-site like for example: 
```http://local.taktwerk-erp.dev/de/gii```

Here you have to create Models / CRUDs with the **Giiant Model** and **Giiant CRUD Generator**.
> **IMPORTANT**: Please follow our MySQL-Conventions for best CRUD-Generator Support!

### 3 Adding your controllers to the config
For your controllers to be detected by Yii, you need to add your them in the `src\config\api.php` urlManager->rules->controller array. Follow the config conventions already set as examples.

### 4 API Controllers
API Controllers will auto-generated once with Giiant CRUD Generator. You have to define a Controler-Module which is per default 'v1'. You can test your generated APIs with for example: ```[GET] http://local.taktwerk-erp.api/v1/person```

## Development
- use the [flystem](https://github.com/creocoder/yii2-flysystem) filesystem adapters for file modification, also for local!