<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/31/2017
 * Time: 9:41 AM
 */

namespace taktwerk\yiiboilerplate\components\dav;

use League\Flysystem\AdapterInterface;
use Sabre\DAV\ICollection;
use Sabre\DAV\FS\Node;
use Sabre\HTTP\URLUtil;

class Directory extends Node implements ICollection
{
    /**
     * @var AdapterInterface
     */
    protected $fs;

    /**
     *
     */
    protected $name;

    /**
     *
     */
    protected $component;

    /**
     *
     */
    public function __construct($component, $path = null)
    {
        $this->path = $path == null ? ($component['path'] == null ? '/' : $component['path']) : $path;
        $this->fs = \Yii::$app->get($component['component']);
        $this->component = $component;
        $this->name = isset($component['name']) ? $component['name'] : null;
        unset($this->component['name']);
    }

    /**
     *
     */
    public function createFile($name, $data = null)
    {
        $this->fs->writeStream($this->path . '/' . $name, $data);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function createDirectory($name)
    {
        $this->fs->createDir($this->path . '/' . $name);
    }

    /**
     * @param string $name
     * @return Directory|File
     * @throws \Sabre\DAV\Exception\NotFound
     */
    public function getChild($name)
    {
        $path = $this->path . '/' . $name;
        // We have to throw a NotFound exception if the file didn't exist
        if (!$this->fs->has($path)) {
            throw new \Sabre\DAV\Exception\NotFound('The file with name: ' . $path . ' could not be found');
        }
        // Some added security
        if ($name[0] == '.') {
            throw new \Sabre\DAV\Exception\NotFound('Access denied');
        }
        $type = $this->fs->getMetadata($path);
        if ($type['type'] == 'dir') {
            return new self($this->component, $path);
        } else {
            return new File($path, $this->fs);
        }
    }

    /**
     * @return array
     * @throws \Sabre\DAV\Exception\NotFound
     */
    public function getChildren()
    {
        $children = array();
        // Loop through the directory, and create objects for each node
        foreach ($this->fs->listContents($this->path) as $node) {
            // Ignoring files staring with .
            if ($node['basename'] === '.') {
                continue;
            }
            if ($node['basename'] === '/') {
                continue;
            }
            $children[] = $this->getChild($node['basename']);
        }
        return $children;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function childExists($name)
    {
        return $this->fs->has($this->path . '/' . $name);
    }

    /**
     * @return mixed
     */
    public function delete()
    {
        $this->fs->deleteDir($this->path);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function setName($name)
    {
        list($parentPath, ) = URLUtil::splitPath($this->path);
        list(, $newName) = URLUtil::splitPath($name);
        $newPath = $parentPath . '/' . $newName;
        $this->fs->rename($this->path, $newPath);
        $this->path = $newPath;
    }

    /**
     * @return null
     */
    public function getLastModified()
    {
        return $this->path == '/' ? null : $this->fs->getMetadata($this->path)['timestamp'];
    }

    public function getName()
    {
        if ($this->name !== null) {
            return $this->name;
        }
        return parent::getName();
    }

}