<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/31/2017
 * Time: 9:30 AM
 */

namespace taktwerk\yiiboilerplate\components\dav\auth;

use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use \Sabre\DAV\Auth\Backend\AbstractBasic;
use \Yii;

class Auth extends AbstractBasic
{

    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    protected function validateUserPass($username, $password)
    {
        $user = Yii::$container->get(Finder::className())->findUserByUsernameOrEmail($username);
        if ($user !== null && Password::validate($password, $user->password_hash)) {
            Yii::$app->user->login($user);
            return true;
        }
        return false;
    }
}