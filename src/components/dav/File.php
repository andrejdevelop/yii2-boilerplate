<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/31/2017
 * Time: 9:46 AM
 */

namespace taktwerk\yiiboilerplate\components\dav;

use League\Flysystem\AdapterInterface;
use Sabre\DAV\File as BaseFile;

class File extends BaseFile
{
    /**
     * @var
     */
    protected $myPath;
    /**
     * @var AdapterInterface
     */
    protected $fs;

    /**
     * File constructor.
     * @param $myPath
     * @param $fs
     */
    public function __construct($myPath, $fs)
    {
        $this->myPath = $myPath;
        $this->fs = $fs;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return basename($this->myPath);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $file = $this->fs->readStream($this->myPath);
        return $file;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->fs->getMetadata($this->myPath)['size'];
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->fs->getMimetype($this->myPath);
    }

    /**
     * @param resource|string $data
     * @return mixed
     */
    public function put($data)
    {
        return $this->fs->writeStream($this->myPath, $data);
    }

    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->fs->getMetadata($this->myPath)['timestamp'];
    }

    /**
     * @return string
     */
    public function getETag()
    {
        return '"' . sha1(
            $this->getSize() .
            $this->getLastModified()
        ) . '"';
    }

    /**
     * @return bool
     */
    public function delete()
    {
        return $this->fs->delete($this->myPath);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function setName($name)
    {

        return $this->fs->rename($this->myPath, pathinfo($this->myPath)['dirname'] . '/' . $name);
    }

}
