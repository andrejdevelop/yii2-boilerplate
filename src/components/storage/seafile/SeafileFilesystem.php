<?php
/**
 * Seafile filesystem
 * To setup in config:
 *
 * 'components' => [
 * 'fs' => [
 * 'class' => 'taktwerk\yiiboilerplate\components\storage\seafile\SeafileFilesystem',
 * 'token' => "d9cae0377e3f466dfd7e4982105b2dd2049eba56",
 * 'library' => "test",
 * 'server' => "https://tw-cloud.taktwerk.ch"
 * ],
 * ]
 *
 * Usage in application, examples on https://github.com/creocoder/yii2-flysystem
 */

namespace taktwerk\yiiboilerplate\components\storage\seafile;

use creocoder\flysystem\Filesystem;
use taktwerk\yiiboilerplate\components\storage\seafile\adapter\SeafileAdapter;
use yii\base\InvalidConfigException;

class SeafileFilesystem extends Filesystem
{

    /**
     * @var string
     */
    public $library;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $server;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->token === null) {
            throw new InvalidConfigException('The "token" property must be set.');
        }

        if ($this->library === null) {
            throw new InvalidConfigException('The "library" property must be set.');
        }

        parent::init();
    }

    /**
     * @return SeafileAdapter
     */
    protected function prepareAdapter()
    {
        return new SeafileAdapter($this->library, $this->token, $this->server);
    }
}