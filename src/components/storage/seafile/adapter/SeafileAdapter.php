<?php

namespace taktwerk\yiiboilerplate\components\storage\seafile\adapter;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Adapter\Polyfill\NotSupportingVisibilityTrait;
use League\Flysystem\Adapter\Polyfill\StreamedTrait;
use League\Flysystem\Config;
use League\Flysystem\Util;

class SeafileAdapter extends AbstractAdapter
{

    use NotSupportingVisibilityTrait;
    use StreamedTrait;

    protected $library;
    protected $token;
    protected $libraryID;
    protected $url;
    protected $server = "https//tw-could.taktwerk.ch";
    protected $outputStream;

    /**
     *
     * @param string $library
     * @param string $token
     */
    public function __construct($library, $token, $server)
    {
        $this->library = $library;
        $this->token = $token;
        $this->server = $server;
        $this->initLibrary();
    }

    /**
     * Initialize library, if not exist, create new one and setup libraryID
     * @return void
     */
    protected function initLibrary()
    {
        $found = false;
        $response = $this->call("repos", "GET");
        if ($response['code'] == 200) {
            foreach ($response['result'] as $library) {
                if ($library->name === $this->library) {
                    $found = true;
                    $this->libraryID = $library->id;
                    break;
                }
            }
            if (!$found) {
                $data = ['name' => $this->library];
                $library = $this->call('repos', "POST", $data);
                $this->libraryID = $library->repo_id;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function listContents($directory = '', $recursive = false)
    {
        $contents = [];
        $location = $this->applyPathPrefix($directory);
        $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $location);
        if ($response['code'] == 200) {
            $contents = [];
            foreach ($response['result'] as $object) {
                $path = $location . '/' . $object->name;
                $contents[] = $this->normalizeResponse($object, $path);
                if ($recursive && $object->type === 'dir') {
                    $deeper = $this->listContents($path, true);
                    if ($deeper) {
                        $contents = array_merge($contents, $deeper);
                    }
                }
            }
            return $contents;
        }
        return false;
    }

    public function createDir($path, Config $config)
    {
        $path = $this->applyPathPrefix($path);
        $data = ['operation' => 'mkdir'];
        if (substr_count($path, '/') > 1) {
            // Recursive
            $dirs = explode('/', $path);
            $current = '';
            foreach ($dirs as $dir) {
                $dir = $this->applyPathPrefix($dir);
                $current .= $dir;
                $current = '/' . ltrim($current, '/');
                if ($this->has($current) || $current == '/') {
                    continue;
                }
                $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $current, 'POST', $data);
            }
        } else {
            $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $path, 'POST', $data);
        }
        if ($response['code'] == 201) {
            return true;
        }
        return false;
    }

    public function deleteDir($path)
    {
        $path = $this->applyPathPrefix($path);
        $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $path, 'DELETE');
        if ($response['code'] == 200) {
            return true;
        }
        return false;
    }

    public function read($path)
    {
        $location = $this->applyPathPrefix($path);
        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . $location);
        if ($response['code'] == 200) {
            $contents = $this->getFile($response['result']);
            return compact('contents', 'path');
        }
        return false;
    }

//    public function readStream($path)
//    {
//        $location = $this->applyPathPrefix($path);
//        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . $location);
//        if ($response['code'] == 200) {
//            $stream = fopen('php://temp', 'w+');
//            $this->getFile($response['result'], $stream);
//            return compact('stream', 'path');
//        }
//        return false;
//    }

    public function write($path, $contents, Config $config)
    {
        $path = $this->applyPathPrefix($path);
        if ($this->has($path)) {
            return false;
        }
        if (substr_count($path, '/') > 1) {
            $directory = substr($path, 0, strrpos($path, '/') + 1);
            $this->createDir($directory, $config);
        }
        if ($this->upload($path, $contents)) {
            $metaData = $this->getMetadata($path);
            $size = $metaData['size'];
            $type = 'file';
            $result = compact('contents', 'type', 'size', 'path');
            return $result;
        }
        return false;
    }

//    public function writeStream($path, $resource, Config $config)
//    {
//        $path = $this->applyPathPrefix($path);
//        if ($this->has($path)) {
//            return false;
//        }
//        if ($this->update($path, $resource, $config)) {
//            return compact('path');
//        }
//        return false;
//    }

    public function update($path, $contents, Config $config)
    {
        $path = $this->applyPathPrefix($path);
        if (!$this->has($path)) {
            return false;
        }
        if ($this->upload($path, $contents)) {
            $metaData = $this->getMetadata($path);
            $size = $metaData['size'];
            return compact('path', 'size', 'contents');
        }
        return false;
    }

//    public function updateStream($path, $resource, Config $config)
//    {
//        return $this->writeStream($path, $resource, $config);
//    }

    /**
     * Return metaData of file or directory
     * Since limitation of Seafile API, we assume that file must contain . (dot).
     * If there is no . (dot) input param will be processed as dir
     * @param type $path
     * @return bool|array
     */
    public function getMetadata($path)
    {
        $found = false;
        $path = $this->applyPathPrefix($path);
        // Directory, checking first if there are directory with that name because of Seafile API limitation
        // Going up level up for listing directories, since Seafile don't have direct API for directory info
        $parentPath = substr($path, 0, strrpos($path, '/') + 1);
        $name = substr($path, strrpos($path, '/') + 1);
        $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $parentPath . '&t=d');
        if ($response['code'] == 200) {
            foreach ($response['result'] as $dir) {
                if ($dir->name == $name) {
                    $found = true;
                    break;
                }
            }
        }
        // If there is no folder with that name, try to find a file
        if (!$found) {
            $responese = $this->call('repos/' . $this->libraryID . '/file/detail/?p=' . $path);
            if ($responese['code'] == 200) {
                $found =(array)$responese['result'];
            } else {
                $found = false;
            }
        }
        return $found;
//        $path = $this->applyPathPrefix($path);
//        if (strpos($path, '.')) {
//            // File
//            $responese = $this->call('repos/' . $this->libraryID . '/file/detail/?p=' . $path);
//            if ($responese['code'] == 200) {
//                return (array)$responese['result'];
//            } else {
//                return false;
//            }
//        } else {
//            // Directory
//            // Going up level up for listing directories, since Seafile don't have direct API for directory info
//            $parentPath = substr($path, 0, strrpos($path, '/') + 1);
//            $name = substr($path, strrpos($path, '/') + 1);
//            $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $parentPath . '&t=d');
//            if ($response['code'] == 200) {
//                foreach ($response['result'] as $dir) {
//                    if ($dir->name == $name) {
//                        return (array)$dir;
//                    }
//                }
//                //@TODO: Not returning false, double check why
//                return false;
//            }
//            return false;
//        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMimetype($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getSize($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getTimestamp($path)
    {
        return $this->getMetadata($path);
    }

    public function rename($path, $newpath)
    {
        $path = $this->applyPathPrefix($path);
        if ($this->has($newpath)) {
            return false;
        }
        $newfile = explode('/', $newpath);
        $newfile = $newfile[count($newfile) - 1];
        $data = ['operation' => 'rename', 'newname' => $newfile];
        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . urlencode($path), 'POST', $data);
        return $response;
    }

    public function copy($path, $newpath)
    {
        $path = $this->applyPathPrefix($path);
        $newpath = $this->applyPathPrefix($newpath);

        $source = pathinfo($path);
        $destination = pathinfo($newpath);
        $sourceFilename = $source['basename'];
        $destinationFilename = $destination['basename'];
        $sourceDir = $source['dirname'];
        $destinationDir = $destination['dirname'];
        $sourceMetaData = $this->getMetadata($path);
        if ($sourceMetaData['type'] == 'file'
            && $sourceFilename != $destinationFilename
            && $sourceDir == $destinationDir
        ) {
            $tmpDir = '/' . md5($sourceFilename);
            $this->createDir($tmpDir, new Config());
            $this->copy($path, $tmpDir . '/' . $sourceFilename);
            $this->rename($tmpDir . '/' . $sourceFilename, $tmpDir . '/' . $destinationFilename);
            $result = $this->copy($tmpDir . '/' . $destinationFilename, $destinationDir . '/' . $destinationFilename);
            $this->deleteDir($tmpDir);
            return $result;
        } else {
            $directory = substr($newpath, 0, strrpos($newpath, '/') + 1);
            $name = substr($newpath, strrpos($newpath, '/') + 1);
            $data = [
                'operation' => 'copy',
                'dst_repo' => $this->libraryID,
                'dst_dir' => $directory,
                'file_names' => $name
            ];
            $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . $path, 'POST', $data);
            if ($response['code'] == 200) {
                return true;
            }
            return false;
        }
    }

    public function delete($path)
    {
        $path = $this->applyPathPrefix($path);
        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . urlencode($path), 'DELETE');
        if ($response['code'] == 200) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function has($path)
    {
        $path = $this->applyPathPrefix($path);
        return $this->getMetadata($path);
        return !$this->getMetadata($path) ? false : true;
    }

    /**
     * Seafile API Call
     * @param string $url like 'repos' or some other API-Action
     * @param string $type 'GET','POST','PUT','DELETE'
     * @param null $data array()|null, with POST-Data
     * @return mixed returned json-object
     */
    protected function call($url, $type = "GET", $data = null)
    {
        $curl = curl_init();
        $url = $this->server . "/api2/$url";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($type == "POST") {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
        } elseif ($type == "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
        }
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Authorization: Token ' . $this->token,
            'Accept: application/json; indent=4'
        ]);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $ch_error = curl_error($curl);
        curl_close($curl);
        return [
            'result' => json_decode($result),
            'code' => $httpcode,
            'error' => $ch_error
        ];
    }

    /**
     * Apply the path prefix.
     *
     * @param string $path
     *
     * @return string prefixed path
     */
    public function applyPathPrefix($path)
    {

        $path = parent::applyPathPrefix($path);

        return '/' . ltrim(rtrim($path, '/'), '/');
    }

    protected function upload($path, $content, $stream = false)
    {
        if ($this->has($path)) {
            $this->delete($path);
        }
        $directory = substr($path, 0, strrpos($path, '/') + 1);
        $fileName = substr($path, strrpos($path, '/') + 1);
        $response = $this->call('repos/' . $this->libraryID . '/upload-link/?p=' . $directory);
        if ($response['code'] == 200) {
            $uploadLink = $response['result'];
            if (!$stream) {
                $tmpFile = tmpfile();
                fwrite($tmpFile, $content);
                fseek($tmpFile, 0);
            } else {
                $tmpFile = $content;
            }
            $metaDatas = stream_get_meta_data($tmpFile);
            $tmpFilename = $metaDatas['uri'];
            $tmpFilenameType = $metaDatas['wrapper_type'];
            $data = ['filename' => $fileName, 'parent_dir' => $directory];
            $data['file'] = new \CurlFile($tmpFilename, $tmpFilenameType, $fileName);
            $url = $uploadLink;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Token ' . $this->token));
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $result = curl_exec($curl);
            $ch_error = curl_error($curl);
            curl_close($curl);
            if (!$stream) {
                fclose($tmpFile);
            }
            return $result;
        }
        return false;
    }

    protected function getFile($url, $stream = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        $ch_error = curl_error($curl);
        curl_close($curl);
        if ($stream) {
            fwrite($stream, $result);
            rewind($stream);
        } else {
            return $result;
        }
    }

    /**
     * Normalize a Seafile response.
     *
     * @param array $response
     *
     * @return array
     */
    protected function normalizeResponse($response, $path = '')
    {
        $result = ['path' => ltrim($path, '/')];

        if (isset($response->mtime)) {
            $result['timestamp'] = $response->mtime;
        }
        $result = array_merge($result, get_object_vars($response));

        return $result;
    }

}
