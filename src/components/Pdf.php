<?php

namespace taktwerk\yiiboilerplate\components;

use yii\base\Component;
use kartik\mpdf\Pdf as Generator;
use yii\base\InvalidConfigException;

class Pdf extends Component
{

    /**
     * @var null
     */
    public $filename;

    /**
     * @var null
     */
    public $content;

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var Generator;
     */
    protected $generator;

    /**
     * Pdf constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->options = $config;
        parent::__construct($config);
    }

    /**
     * @return mixed
     */
    public function generate()
    {
        $this->setApi();
        return $this->generator->render();
    }

    /**
     *
     */
    protected function setApi()
    {
        $this->options['destination'] = Generator::DEST_FILE;
        if ($this->filename) {
            $this->options['filename'] = $this->filename;
        }
        if ($this->content) {
            $this->options['content'] = $this->content;
        }
        $this->check();
        $this->makeDir();
        $this->generator = new Generator($this->options);
    }

    /**
     *
     */
    protected function makeDir()
    {
        $path = pathinfo($this->options['filename']);
        if (!is_dir($path['dirname'])) {
            mkdir($path['dirname'], 0777, true);
        }
    }

    /**
     * @throws InvalidConfigException
     */
    protected function check()
    {
        if (!isset($this->options['filename'])) {
            throw new InvalidConfigException(\Yii::t('app', 'Filename must be set'));
        }
        if (!isset($this->options['content'])) {
            throw new InvalidConfigException(\Yii::t('app', 'Content must be set'));
        }
    }
}