<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/27/2017
 * Time: 3:59 PM
 */

namespace taktwerk\yiiboilerplate\components;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\UriInterface;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Console;

class Crawler extends Component
{

    /**
     * @var
     */
    public $url;

    /**
     * @var
     */
    public $verbose = false;

    public $username;

    public $password;

    /**
     * @var \Symfony\Component\DomCrawler\Crawler
     */
    protected $crawler;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * @var
     */
    protected $currentUrl;

    /**
     * @var array
     */
    protected $visitedUrl = [];

    /**
     * @var array
     */
    protected $crawledUrl = [];

    /**
     * @var array
     */
    public $errors = [];

    /**
     * @var int
     */
    public static $total = 0;

    /**
     * @var int
     */
    public static $totalSuccess = 0;

    /**
     * @var int
     */
    public static $totalErrors = 0;

    /**
     * @var array
     */
    protected $spinner = ['/', '-', '\\', '|'];

    /**
     * @var int
     */
    protected static $spinnerCounter = 0;

    /**
     * @var
     */
    protected $baseUrl;

    /**
     * @var array
     */
    protected $headers = [];

    public function __construct(array $config)
    {
        if ($config['url'] == null && !isset($config['url'])) {
            throw new InvalidConfigException('Url is not set');
        }
        $this->url = $config['url'];
        $this->verbose = $config['verbose'];
        $url = parse_url($this->url);
        $baseUrl = $url['scheme'] . '://' . $url['host'];
        $this->baseUrl = $baseUrl;
        $this->crawler = new \Symfony\Component\DomCrawler\Crawler(null, $baseUrl);
        $this->httpClient = new Client([
            'http_errors' => false,
            'verify' => false,
        ]);
        if (isset($config['username'], $config['password'])
            && !empty($config['username'])
            && !empty($config['password'])
        ) {
            $this->headers = [
                'headers' => [
                    'Action' => 'login',
                    'Username' => $config['username'],
                    'Password' => $config['password'],
                ],
            ];
        }
        parent::__construct($config);
    }

    /**
     *
     */
    public function run()
    {
        $this->currentUrl = $this->url;
        // Get links from entry page
        foreach ($this->getLinks() as $link) {
            if (is_string($link)) {
                $url = $this->appendBaseUrl($link);
            } else {
                $url = $link->getUri();
            }
            if ($this->internalLink($url)
                && $this->isHttp($url)
                && is_string($url)
                && !$this->isRiskyLink($url)
            ) {
                $this->crawledUrl[] = $this->stripHash($url);
            }
        }

        $this->crawledUrl = array_unique($this->crawledUrl);
        reset($this->crawledUrl);
        while (!empty($this->crawledUrl)) {
            $this->currentUrl = current($this->crawledUrl);
            if (!empty($this->currentUrl) &&
                is_string($this->currentUrl) ||
                $this->currentUrl instanceof UriInterface
            ) {
                // If we already visited page, skip
                if (in_array($this->currentUrl, $this->visitedUrl)) {
                    $this->goToNext();
                    continue;
                }
                try {
                    $response = $this->httpClient
                        ->request('GET', $this->currentUrl, array_merge(['allow_redirects' => false], $this->headers))
                        ->getStatusCode();
                } catch (\Exception $e) {
                    $fail = Console::ansiFormat("[FAILED - {$e->getMessage()}]", [Console::FG_RED]);
                    if ($this->verbose) {
                        Console::stderr("$fail - {$this->currentUrl}\n");
                    }
                    $this->errors[$this->currentUrl] = $e->getMessage();
                    $this->goToNext();
                    self::$totalErrors++;
                    continue;
                }
                if (in_array($response, [301, 302])) {
                    $status = Console::ansiFormat("[REDIRECT - $response]", [Console::FG_YELLOW]);
                } elseif ($response != 200) {
                    $fail = Console::ansiFormat("[FAILED - $response]", [Console::FG_RED]);
                    if ($this->verbose) {
                        Console::stderr("$fail - {$this->currentUrl}\n");
                    }
                    $this->errors[$this->currentUrl] = $response;
                    $this->goToNext();
                    self::$totalErrors++;
                    continue;
                } else {
                    $status = Console::ansiFormat("[OK - $response]", [Console::FG_GREEN]);
                }

                // Add links from current crawled page
                foreach ($this->getLinks() as $link) {
                    if (is_string($link)) {
                        $url = $this->appendBaseUrl($link);
                    } else {
                        $url = $this->stripHash($link->getUri());
                    }
                    // If there is already link ready for crawling or already visited skip
                    if (!(in_array($url, $this->crawledUrl) || in_array($url, $this->visitedUrl))
                        && $this->internalLink($url)
                        && $this->isHttp($url)
                        && is_string($url)
                        && !$this->isRiskyLink($url)
                    ) {
                        $this->crawledUrl[] = $url;
                    }
                }
                self::$totalSuccess++;
                if ($this->verbose) {
                    Console::stdout("$status - {$this->currentUrl}\n");
                }
            }
            self::$total++;
            $this->goToNext();
        }
    }

    /**
     *
     */
    protected function goToNext()
    {
        $this->addToVisited();
        $key = array_search($this->currentUrl, $this->crawledUrl);
        next($this->crawledUrl);
        unset($this->crawledUrl[$key]);
        $this->spin();
    }

    /**
     * Add current page to already visited pages, along with same pages on other languages
     */
    protected function addToVisited()
    {
        $languages = explode(',', getenv('APP_LANGUAGES'));
        preg_match('#.*\/([a-z]{2,3})\/#i', $this->currentUrl, $matches);
        if (isset($matches[1])) {
            foreach ($languages as $language) {
                if ($language != $matches[1]) {
                    $this->visitedUrl[] = str_replace('/' . $matches[1] . '/', "/$language/", $this->currentUrl);
                }
            }
        }
        $this->visitedUrl[] = $this->currentUrl;
    }

    /**
     * @return array|\Symfony\Component\DomCrawler\Link[]
     */
    protected function getLinks()
    {
        try {
            $html = $this->httpClient->request('GET', $this->currentUrl, $this->headers)
                ->getBody()->getContents();
        } catch (\Exception $e) {
            $fail = Console::ansiFormat("[FAILED - {$e->getMessage()}]", [Console::FG_RED]);
            if ($this->verbose) {
                Console::stderr("$fail - {$this->currentUrl}\n");
            }
            $this->errors[$this->currentUrl] = $e->getMessage();
            return [];
        }
        $this->crawler->addHtmlContent($html);
        return array_merge($this->crawler->filterXPath('//img')->extract(['src']), $this->crawler->filter('a')->links());
    }

    /**
     * @param $link
     * @return string
     */
    protected function appendBaseUrl($link)
    {
        if ($link[0] == '' || strpos($link, 'http') === false) {
            return $this->baseUrl . $link;
        }
    }
    /**
     * @return bool
     */
    protected function internalLink($link)
    {
        $currentHost = parse_url($link);
        // If scheme is not http or https
        if (!$this->isHttp($link)) {
            return false;
        }
        $currentHost = $currentHost['host'];
        $baseHost = parse_url($this->url);
        $baseHost = $baseHost['host'];
        return $currentHost == $baseHost;
    }

    /**
     * @param $link
     * @return bool
     */
    protected function isHttp($link)
    {
        $schema = parse_url($link);
        if (isset($schema['scheme'])) {
            return in_array($schema['scheme'], ['http', 'https']);
        }
        return false;
    }

    /**
     * @param $link
     * @return mixed
     */
    protected function stripHash($link)
    {
        return explode('#', $link)[0];
    }

    /**
     *
     */
    protected function spin()
    {
        if (!$this->verbose) {
            Console::stdout("\r" .
                $this->spinner[self::$spinnerCounter] .
                " Checking" .
                str_repeat('.', self::$spinnerCounter) .
                str_repeat(' ', count($this->spinner) - self::$spinnerCounter - 1));
            self::$spinnerCounter++;
            if (self::$spinnerCounter > (count($this->spinner) - 1)) {
                self::$spinnerCounter = 0;
            }
        }
    }

    /**
     * Links that we don't want to process and test
     * @param $url
     * @return bool
     */
    protected function isRiskyLink($url)
    {
        return (
            strpos($url, 'delete') !== false
            || strpos($url, 'block') !== false
            || strpos($url, 'remove') !== false
            || strpos($url, 'logout') !== false
            || strpos($url, 'adminer') !== false
            // Used for copy
            || strpos($url, '/create?') !== false
            || strpos($url, 'debug') !== false
            || strpos($url, 'scan') !== false
            || strpos($url, 'index?') !== false
            || strpos($url, 'view?') !== false
            || strpos($url, 'update?') !== false
            || strpos($url, 'translate?') !== false
            || strpos($url, 'sort=') !== false
        );
    }

}