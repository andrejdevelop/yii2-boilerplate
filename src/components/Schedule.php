<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 9:15 AM
 */

namespace taktwerk\yiiboilerplate\components;

use omnilight\scheduling\Event;

class Schedule extends \omnilight\scheduling\Schedule
{
    /**
     * Add a new cli command event to the schedule.
     *
     * @param  string  $command
     * @return Event
     */
    public function command($command)
    {
        $exec = PHP_BINARY . ' ' . $this->cliScriptName . ' ' . $command;
        // Checking if we run under Windows
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $exec = '"' . PHP_BINARY . '"' . ' ' . '"' . $this->cliScriptName . '"' . ' ' . $command;
        }
        return $this->exec($exec);
    }
}