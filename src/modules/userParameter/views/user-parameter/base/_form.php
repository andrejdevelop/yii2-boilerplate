<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/04f0b2ff7bd97130b071fc9ab4e68ec0
 *
 * @package default
 */


use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter $model
 * @var yii\widgets\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */
$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;
?>
<div class="user-parameter-form">
    <?php $form = ActiveForm::begin([
		'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
		'id' => 'UserParameter' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
		'layout' => 'default',
		'enableClientValidation' => true,
		'errorSummaryCssClass' => 'error-summary alert alert-error',
		'action' => $useModal ? $action : '',
		'options' => [
			'name' => 'UserParameter',
		],
	]);
?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            <?php if ($multiple) : ?>
                <?php echo Html::hiddenInput('update-multiple', true)?>
                <?php foreach ($pk as $id) :?>
                    <?php echo Html::hiddenInput('pk[]', $id)?>
                <?php endforeach;?>
            <?php endif;?>

        <div class="col-md-6">
            <?php if (!$multiple || ($multiple && isset($show['user_id']))) :?>
            <?php echo $form->field(
		$model,
		'user_id'
	)
	->widget(
		Select2::classname(),
		[
			'data' => app\models\User::find()->count() > 50 ? null : ArrayHelper::map(app\models\User::find()->all(), 'id', 'toString'),
			'initValueText' => app\models\User::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(app\models\User::find()->andWhere(['id' => $model->user_id])->all(), 'id', 'toString') : '',
			'options' => [
				'placeholder' => Yii::t('app', 'Select a value...'),
				'id' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
			],
			'pluginOptions' => [
				'allowClear' => false,
				(app\models\User::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
				(app\models\User::find()->count() > 50 ? 'ajax' : '') => [
					'url' => \yii\helpers\Url::to(['list']),
					'dataType' => 'json',
					'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'User\'
                                        };
                                    }')
				],
			],
			'pluginEvents' => [
				"change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
				Url::toRoute('taktwerk/yiiboilerplate/modules/userParameter/user/entry-details?id=', true) .
				"' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#user_id_well').show();
                                                $('#user_id_well').html(json.data);
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
			],
			'addon' => (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal)) ? [
				'append' => [
					'content' => [
						RelatedForms::widget([
								'relatedController' => 'taktwerk/yiiboilerplate/modules/userParameter/user',
								'type' => $relatedTypeForm,
								'selector' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
								'primaryKey' => 'id',
							]),
					],
					'asButton' => true
				],
			] : []
		]
	)
?>

            <div id="user_id_well" class="well col-sm-6"
                style="margin-left: 8px;
                <?php echo
	app\models\User::findOne(['id' => $model->user_id])
	->entryDetails == '' ?
		' display:none;' :
		''
	?>">
                <?php echo
	app\models\User::findOne(['id' => $model->user_id])->entryDetails != '' ?
		app\models\User::findOne(['id' => $model->user_id])->entryDetails :
		''?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
            </div>
            <?php
	?>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php if (!$multiple || ($multiple && isset($show['key']))) :?>
            <?php echo $form->field(
		$model,
		'key',
		[
			'selectors' => [
				'input' => '#' .
				Html::getInputId($model, 'key') . $owner
			]
		]
	)
	->textInput(
		[
			'maxlength' => true,
			'id' => Html::getInputId($model, 'key') . $owner
		]
	)
?>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php if (!$multiple || ($multiple && isset($show['value']))) :?>
            <?php echo $form->field(
		$model,
		'value',
		[
			'selectors' => [
				'input' => '#' .
				Html::getInputId($model, 'value') . $owner
			]
		]
	)
	->textarea(
		[
			'rows' => 6,
			'id' => Html::getInputId($model, 'value') . $owner
		]
	)
?>
            <?php endif; ?>
        </div>                                            </p>
        <?php $this->endBlock(); ?>

        <?php echo ($relatedType != RelatedForms::TYPE_TAB) ?
	Tabs::widget([
		'encodeLabels' => false,
		'items' => [
			[
				'label' => Yii::t('app', Inflector::camel2words('UserParameter')),
				'content' => $this->blocks['main'],
				'active' => true,
			],
		]
	])
	: $this->blocks['main']
?>
        <div class="col-md-12">
        <hr/>

        </div>

        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <div class="col-md-6"<?php echo !$relatedForm ? ' id="main-submit-buttons"' : ''?>>
        <?php echo Html::submitButton(
	'<span class="glyphicon glyphicon-check"></span> ' .
	($model->isNewRecord && !$multiple ?
		Yii::t('cruds', 'Create') :
		Yii::t('cruds', 'Save')),
	[
		'id' => 'save-' . $model->formName(),
		'class' => 'btn btn-success',
		'name' => 'submit-default'
	]
);
?>

        <?php if ((!$relatedForm && !$useModal) && !$multiple) { ?>
            <?php echo Html::submitButton(
		'<span class="glyphicon glyphicon-check"></span> ' .
		($model->isNewRecord && !$multiple ?
			Yii::t('cruds', 'Create & New') :
			Yii::t('cruds', 'Save & New')),
		[
			'id' => 'save-' . $model->formName(),
			'class' => 'btn btn-default',
			'name' => 'submit-new'
		]
	);
?>
            <?php echo Html::submitButton(
		'<span class="glyphicon glyphicon-check"></span> ' .
		($model->isNewRecord && !$multiple ?
			Yii::t('cruds', 'Create & Close') :
			Yii::t('cruds', 'Save & Close')),
		[
			'id' => 'save-' . $model->formName(),
			'class' => 'btn btn-default',
			'name' => 'submit-close'
		]
	);
?>

            <?php if (!$model->isNewRecord) { ?>
                <?php echo Html::a(
			'<span class="glyphicon glyphicon-trash"></span> ' .
			Yii::t('cruds', 'Delete'),
			['delete', 'id' => $model->id],
			[
				'class' => 'btn btn-danger',
				'data-confirm' => '' . Yii::t('cruds', 'Are you sure to delete this item?') . '',
				'data-method' => 'post',
			]
		);
?>
            <?php } ?>
        <?php } elseif ($multiple) { ?>
                <?php echo Html::a(
		'<span class="glyphicon glyphicon-exit"></span> ' .
		Yii::t('cruds', 'Close'),
		$useModal ? false : [],
		[
			'id' => 'save-' . $model->formName(),
			'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
			'name' => 'close'
		]
	);
?>
        <?php } else { ?>
            <?php echo Html::a(
		'<span class="glyphicon glyphicon-exit"></span> ' .
		Yii::t('cruds', 'Close'),
		['#'],
		[
			'class' => 'btn btn-danger',
			'data-dismiss' => 'modal',
			'name' => 'close'
		]
	);
?>
        <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php echo ($relatedForm && $relatedType == \taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms::TYPE_MODAL) ?
	'<div class="clearfix"></div>' :
	''
?>
        <div class="clearfix"></div>
    </div>
</div>
<?php
if ($useModal) {
	\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
?>
