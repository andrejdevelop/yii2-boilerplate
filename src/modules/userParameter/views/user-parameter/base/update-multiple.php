<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/b18644106c982bf7c91ad36ce7219022
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter $model
 * @var array $pk
 * @var array $show
 */
$this->title = 'User Parameter ' . $model->id . ', ' . Yii::t('cruds', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => 'User Parameters', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body user-parameter-update">

        <h1>
            <?php echo Yii::t('cruds', 'User Parameter') ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
		'model' => $model,
		'pk' => $pk,
		'show' => $show,
		'multiple' => true,
		'useModal' => $useModal,
		'action' => $action,
	]); ?>

    </div>
</div>
