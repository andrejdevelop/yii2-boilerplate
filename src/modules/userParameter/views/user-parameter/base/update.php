<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/375156467ad52c46593e305e25d54430
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter $model
 * @var string $relatedTypeForm
 */
$this->title = Yii::t('app', 'User Parameter') . ' ' . $model->id . ', ' . Yii::t('cruds', 'Edit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Parameters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body user-parameter-update">

        <div class="crud-navigation">
            <?php echo Html::a(
	'<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('cruds', 'View'),
	['view', 'id' => $model->id],
	['class' => 'btn btn-default']
) ?>
        </div>

        <?php echo $this->render('_form', [
		'model' => $model,
		'inlineForm' => $inlineForm,
		'relatedTypeForm' => $relatedTypeForm,
	]); ?>

    </div>
</div>
