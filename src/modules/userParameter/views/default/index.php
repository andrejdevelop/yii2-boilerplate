<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/6/2017
 * Time: 10:23 AM
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View                                                    $this
 * @var \taktwerk\yiiboilerplate\modules\userParam\models\UserParamForm $model
 * @var string                                                          $relatedTypeForm
 */
$this->title = Yii::t('app', 'User Parametars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Parametars'), 'url' => ['index']];
?>
<div class="box box-default">
    <div class="giiant-crud box-body user-param-update">

        <?php echo $this->render('_form', [
            'model' => $model,
        ]); ?>

    </div>
</div>
