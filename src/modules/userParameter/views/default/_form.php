<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * /srv/www/nassi-v2/src/../runtime/giiant/04f0b2ff7bd97130b071fc9ab4e68ec0
 *
 * @package default
 */


use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 *
 * @var yii\web\View                                                   $this
 * @var taktwerk\yiiboilerplate\modules\userParam\models\UserParamForm $model
 */
?>
<div class="user-param-form">
    <?= Html::beginForm('', 'post', [
        'id' => 'UserParam',
        'class' => 'form-horizontal',
    ]) ?>


    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>

            <?php foreach ($model->attributes as $key => $value) : ?>
        <div class="form-group">
            <label class="control-label col-sm-3" for="<?= $key ?>"><?= $model->attributesLabels[$key] ?></label>
            <div class="col-sm-6">
                <?= $model->generateField($key); ?>
            </div>
        </div>
    <?php endforeach; ?>


        </p>
        <?php $this->endBlock(); ?>

        <?= $this->blocks['main'] ?>

        <div class="clearfix"></div>

        <div class="col-md-6">
            <?php echo Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' .
                Yii::t('cruds', 'Save'),
                [
                    'id' => 'save-',
                    'class' => 'btn btn-success',
                    'name' => 'submit-default'
                ]
            );
            ?>

        </div>

        <?= Html::endForm() ?>
        <div class="clearfix"></div>
    </div>
</div>
