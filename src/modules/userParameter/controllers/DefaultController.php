<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/6/2017
 * Time: 9:44 AM
 */

namespace taktwerk\yiiboilerplate\modules\userParameter\controllers;

use taktwerk\yiiboilerplate\modules\userParameter\models\userParameterForm;
use yii\filters\AccessControl;
use yii\web\Controller;

class DefaultController extends Controller
{

    /**
     *
     * @inheritdoc
     * @return unknown
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new userParameterForm();
        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->save();
        }
        return $this->render('index', ['model' => $model]);
    }
}