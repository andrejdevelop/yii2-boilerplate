<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/49eb2de82346bc30092f584268252ed2
 *
 * @package default
 */


namespace taktwerk\yiiboilerplate\modules\userParameter\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "UserParameterController".
 */
class UserParameterController extends \taktwerk\yiiboilerplate\modules\userParameter\controllers\base\UserParameterController
{

	//    /**
	//     * Additional actions for controllers, uncomment to use them
	//     * @inheritdoc
	//     */
	//    public function behaviors()
	//    {
	//        return ArrayHelper::merge(parent::behaviors(), [
	//            'access' => [
	//                'class' => AccessControl::className(),
	//                'rules' => [
	//                    [
	//                        'allow' => true,
	//                        'actions' => [
	//                            'list-of-additional-actions',
	//                        ],
	//                        'roles' => ['@']
	//                    ]
	//                ]
	//            ]
	//        ]);
	//    }

}
