<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/6/2017
 * Time: 8:10 AM
 */

namespace taktwerk\yiiboilerplate\modules\userParameter;

class Module extends \yii\base\Module
{
    const DROPDOWN = 1;
    const STRING = 2;
    const CHECKBOX = 3;
    const INTEGER = 4;

    public $params;

}