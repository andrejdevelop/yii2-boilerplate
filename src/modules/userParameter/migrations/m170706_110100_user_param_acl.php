<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170706_110100_user_param_acl extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('user-param_default');
        $permission->description = 'User Parameter default controller';
        $auth->add($permission);

        $user = $auth->getRole('Public');
        $permission = $auth->getPermission('user-param_default');
        $auth->addChild($user, $permission);
    }

    public function down()
    {
        echo "m170706_110100_user_param_acl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
