<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170707_090133_change_user_param_to_parameters extends TwMigration
{
    public function up()
    {
        $this->renameTable('{{%user_param}}', '{{%user_parameter}}');

        $auth = $this->getAuth();
        $auth->remove($auth->getPermission('user-param_default'));

        $permission = $auth->createPermission('user-parameter_default');
        $permission->description = 'User Parameter default controller';
        $auth->add($permission);

        $permission = $auth->createPermission('user-parameter');
        $permission->description = 'User Parameter module';
        $auth->add($permission);

        $user = $auth->getRole('Public');
        $permission = $auth->getPermission('user-parameter_default');
        $auth->addChild($user, $permission);

        $user = $auth->getRole('Administrator');
        $permission = $auth->getPermission('user-parameter');
        $auth->addChild($user, $permission);

    }

    public function down()
    {
        echo "m170707_090133_change_user_param_to_parameters cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
