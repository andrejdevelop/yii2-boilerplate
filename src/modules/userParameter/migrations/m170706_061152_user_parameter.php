<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170706_061152_user_parameter extends TwMigration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user_param}}',
            [
                'id' => Schema::TYPE_PK,
                'user_id' => Schema::TYPE_INTEGER . "(11) NOT NULL",
                'key' => Schema::TYPE_STRING . '(255)',
                'value' => Schema::TYPE_TEXT,
            ],
            $tableOptions
        );

        $this->createIndex('user_id_idx', '{{%user_param}}', 'user_id');
        $this->createIndex('key_idx', '{{%user_param}}', 'key');

        $this->addForeignKey('fk_user_param_user_id', '{{%user_param}}', 'user_id', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_param_user_id', '{{%user_param}}');
        $this->dropIndex('user_id_idx', '{{%user_param}}');
        $this->dropIndex('key_idx', '{{%user_param}}');
        $this->dropTable('{{%user_param}}');
    }
}
