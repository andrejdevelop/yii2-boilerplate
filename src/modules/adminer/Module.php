<?php

namespace taktwerk\yiiboilerplate\modules\adminer;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\adminer\controllers';

    public function init()
    {
        parent::init();

        $this->defaultRoute = 'adminer';
    }
}
