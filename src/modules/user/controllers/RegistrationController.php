<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/13/2017
 * Time: 11:30 AM
 */

namespace taktwerk\yiiboilerplate\modules\user\controllers;

class RegistrationController extends \dektrium\user\controllers\RegistrationController
{
    public $layout = false;

}