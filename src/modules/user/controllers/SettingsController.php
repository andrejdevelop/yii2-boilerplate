<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/11/2017
 * Time: 2:21 PM
 */

namespace taktwerk\yiiboilerplate\modules\user\controllers;

use taktwerk\yiiboilerplate\modules\user\models\Profile;
use taktwerk\yiiboilerplate\modules\userParameter\models\UserParameterForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class SettingsController extends \dektrium\user\controllers\SettingsController
{
    public $layout = "@taktwerk-backend-views/layouts/main";

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'parameters',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * Shows profile settings form.
     *
     * @return string|\yii\web\Response
     */
    public function actionProfile()
    {
        /**
         * @var $model Profile
         */
        $model = Profile::find()->andWhere(['user_id' => \Yii::$app->user->identity->getId()])->one();
        if ($model == null) {
            $model = \Yii::createObject(Profile::className());
            $model->link('user', \Yii::$app->user->identity);
        }

        $event = $this->getProfileEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->uploadFile(
                'picture',
                $_FILES['Profile']['tmp_name'],
                $_FILES['Profile']['name']
            );
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Your profile has been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    public function actionParameters()
    {
        $model = new UserParameterForm();
        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->save();
        }
        return $this->render('paramaters', ['model' => $model]);
    }
}