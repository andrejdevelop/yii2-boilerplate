<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: ben-g
 * Date: 18.01.2016
 * Time: 10:27
 */
/**
 * @property \taktwerk\yiiboilerplate\modules\share\models\FlysystemUser[] $flysystemUsers
 * @property \taktwerk\yiiboilerplate\modules\share\models\Flysystem[]     $flysystems
 * @property \taktwerk\yiiboilerplate\modules\user\models\UserTwData       $userTwData
 * @property \taktwerk\yiiboilerplate\modules\user\models\Profile          $profile
 */
namespace taktwerk\yiiboilerplate\modules\user\models;

use taktwerk\yiiboilerplate\rest\Token;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use Yii;

class User extends \dektrium\user\models\User
{
    /**
     * todo: determine if this is the best way to handle this. It is similar in dektrium, which seems ridiculous.
     * @var UserTwData
     */
    private $_userTwData;

    /**
     * User's toString method. Can be overridden.
     * @return mixed
     */
    public function toString()
    {
        return !empty($this->profile->name) && !empty($this->profile->name) ? $this->profile->name : $this->username;
    }

    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * @param mixed $token
     * @param null  $type
     * @return User
     * @throws UnauthorizedHttpException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /** @var Token $t */
        $tokenModel = Token::find()->where(['code' => $token])->one();

        if ($tokenModel == null) {
            throw new UnauthorizedHttpException("not a valid access token");
        }
        if ($tokenModel->isExpired) {
            $tokenModel->delete();
            throw new UnauthorizedHttpException("access token is expired");
        }

        $tokenModel->created_at = time();
        $tokenModel->save(false, ['created_at']);

        /** @var User $u */
        $user = User::findOne($tokenModel->user_id);

        return $user;
    }

    /**
     * Check if user is admin
     * @return bool
     */
    public function getIsAdmin()
    {
        return parent::getIsAdmin() || $this->checkAdminRoles();
    }

    /**
     * Check if user has admin roles
     * @return bool
     */
    private function checkAdminRoles()
    {
        foreach (Yii::$app->authManager->getRolesByUser($this->id) as $role) {
            if (in_array($role->name, Yii::$app->user->rootRoles)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return '';
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        return array_merge([
            'flysystemUsers',
            'flysystems',
            'userTwData',
        ], parent::extraFields());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlysystemUsers()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::className(),
            ['user_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlysystems()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\share\models\Flysystem::className(),
            ['id' => 'user_id']
        )
            ->viaTable(\taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::tableName(), ['fs_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTwData()
    {
        return $this->hasOne($this->module->modelMap['UserTwData'], ['user_id' => 'id']);
    }

    /**
     * @param null $options
     * @return mixed|null
     * @throws \Exception
     */
    public function getProfilePicture($options = null)
    {
        if ($this->profile == null) {
            return null;
        }
        if ($this->profile->picture == null) {
            return null;
        }
        return $this->profile->getFileUrl('picture', $options);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select(['id', 'text' => Profile::tableName() . '.name'])
                ->leftJoin(Profile::tableName(), Profile::tableName() . '.user_id = ' . User::tableName() . '.id')
                ->from(self::tableName())
                ->andWhere([\app\models\User::tableName() . '.blocked_at' => null])
                ->andWhere(['like', Profile::tableName() . '.name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // This will take care of the dektrium profile
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        // Since the previous afterSave takes care of creating the profile (from dektrium), this doesn't make much sense.
        // It should allow to override the default profile class, but this condition will never be true.
        if ($insert && !$this->profile) {
            $profile = Yii::createObject($this->module->modelMap['Profile']);
            $profile->name = $this->username;
            $profile->user_id = $this->id;
            $profile->save(false);
        }

        // Make sure the schema exists first. Need to do this for projects that create users before the migration
        // creating the table exists.
        $tableSchema = Yii::$app->db->schema->getTableSchema('{{%user_tw_data}}');
        if ($tableSchema !== null) {
            if ($this->userTwData == null) {
                $this->_userTwData = Yii::createObject($this->module->modelMap['UserTwData']);
                $this->_userTwData->link('user', $this);
            }
        }
    }

    /**
     * Todo: is this really how this should be done?
     * @param UserTwData $userTwData
     */
    public function setUserTwData(UserTwData $userTwData)
    {
        $this->_userTwData = $userTwData;
    }

    /**
     * @return mixed
     */
    public function beforeDelete()
    {
        if ($this->userTwData) {
            $this->userTwData->forceDelete();
        }
        if ($this->profile) {
            $this->profile->delete();
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
