<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;
use dektrium\user\widgets\Connect;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = $this->title . ' [Login]';
taktwerk\yiiboilerplate\assets\TwAsset::register($this);
taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);


$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <?= $this->render($section, [
            'model' => $model,
            'module' => $module,
            'dataProvider' => isset($dataProvider) ? $dataProvider : null,
            'qr' => $qr,
        ]) ?>

    </div>
</div>
<?php if ($module->enableConfirmation): ?>
    <p class="text-center">
        <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
    </p>
<?php endif ?>
<?php if ($module->enableRegistration): ?>
    <p class="text-center">
        <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
    </p>
<?php endif ?>
<?= Connect::widget([
    'baseAuthUrl' => ['/user/security/auth'],
]) ?>
