<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = $this->title . ' [Login]';
taktwerk\yiiboilerplate\assets\TwAsset::register($this);
taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);

$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <div class="alert alert-info">
            <p>
                <?= Yii::t(
                    'user',
                    'In order to finish your registration, we need you to enter following fields'
                ) ?>:
            </p>
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'connect-account-form',
        ]); ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'username') ?>

        <?= Html::submitButton(Yii::t('user', 'Continue'), ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end(); ?>

    </div>
    <p class="text-center">
        <?= Html::a(
            Yii::t(
                'user',
                'If you already registered, sign in and connect this account on settings page'
            ),
            ['/user/settings/networks']
        ) ?>.
    </p>
</div>

