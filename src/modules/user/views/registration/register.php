<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = $this->title . ' [Sign up]';
taktwerk\yiiboilerplate\assets\TwAsset::register($this);
taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);

$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'registration-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'username') ?>

        <?php if ($module->enableGeneratingPassword == false): ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
        <?php endif ?>

        <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end(); ?>

    </div>
    <p class="text-center">
        <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login']) ?>
    </p>
</div>

