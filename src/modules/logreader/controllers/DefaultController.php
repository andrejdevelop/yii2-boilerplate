<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 11/7/2016
 * Time: 12:49 PM
 */

namespace taktwerk\yiiboilerplate\modules\logreader\controllers;

use taktwerk\yiiboilerplate\modules\logreader\models\Reader;
use Yii;
use zabachok\logreader\controllers\DefaultController as BaseDefaultController;

class DefaultController extends BaseDefaultController
{

    public $layout = "@taktwerk-backend-views/layouts/main";

    public function actionView($file)
    {
        $file = pathinfo($file);
        $model = new Reader($file['dirname'], $file['basename']);
        return $this->render('view', [
            'model' => $model,
            'search' => Yii::$app->getRequest()->post('search'),
        ]);
    }
}