<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\logreader;

use Yii;

class Module extends \zabachok\logreader\Module
{

    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\logreader\controllers';
    public $sources = [
        '@app/../runtime/logs',
    ];

}
