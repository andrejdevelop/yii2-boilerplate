<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/e0080b9d6ffa35acb85312bf99a557f2
 *
 * @package default
 */


namespace taktwerk\yiiboilerplate\modules\notification\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\notification\models\Notification as NotificationModel;

/**
 * Notification represents the model behind the search form about `taktwerk\yiiboilerplate\modules\notification\models\Notification`.
 */
class Notification extends NotificationModel{

	use SearchTrait;

	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'message',
					'read_at',
					'link',
					'created_by',
					'created_at',
					'updated_by',
					'updated_at',
					'deleted_by',
					'deleted_at'
				],
				'safe'
			],
		];
	}


	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}


	/**
	 * Creates data provider instance with search query applied
	 *
	 *
	 * @param array   $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = NotificationModel::find();

		$this->parseSearchParams(NotificationModel::className(), $params);

		$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'sort' => [
					'defaultOrder' => $this->parseSortParams(NotificationModel::className()),
				],
				'pagination' => [
					'params' => [
						'page' => $this->parsePageParams(NotificationModel::className()),
					]
				],
			]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$this->applyHashOperator('id', $query);
		$this->applyHashOperator('user_id', $query);
		$this->applyHashOperator('created_by', $query);
		$this->applyHashOperator('updated_by', $query);
		$this->applyHashOperator('deleted_by', $query);
		$this->applyLikeOperator('message', $query);
		$this->applyLikeOperator('link', $query);
		$this->applyDateOperator('read_at', $query);
		$this->applyDateOperator('created_at', $query);
		$this->applyDateOperator('updated_at', $query);
		$this->applyDateOperator('deleted_at', $query);
		return $dataProvider;
	}


}
