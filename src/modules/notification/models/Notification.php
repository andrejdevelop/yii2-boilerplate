<?php

namespace taktwerk\yiiboilerplate\modules\notification\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\notification\models\base\Notification as BaseNotification;

/**
 * This is the model class for table "notification".
 */
class Notification extends BaseNotification
{

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * Send notification to user
     * @param $userId
     * @param $message
     * @return bool
     */
    public static function send($userId, $message)
    {
        $notification = new self();
        $notification->user_id = $userId;
        $notification->message = $message;
        return $notification->save();
    }
}
