<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 * @var $notification \taktwerk\yiiboilerplate\modules\notification\models\Notification
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/31/2017
 * Time: 11:08 AM
 */
use yii\helpers\Url;

?>
<li>
    <a href="<?= $notification->link ?
        Url::toRoute(['/notification/notification/show', 'id' => $notification->id]) :
        '#' ?>"
        <?= !$notification->link ?
            'data-toggle="modal" data-target="#notificationModal" data-id="' . $notification->id . '"' :
            'target="_blank"'
        ?>
    >
<!--        <i class="ion ion-ios-people info"></i>-->
        <?= $notification->read_at == null ? '<strong>' : '' ?>
        <?= $notification->message ?>
        <?= $notification->read_at == null ? '</strong>' : '' ?>
    </a>
</li>

