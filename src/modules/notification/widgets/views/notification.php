<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 * @var $notifications      \taktwerk\yiiboilerplate\modules\notification\models\Notification[]
 * @var $countNotifications integer
 * @var $countTasks         integer
 * @var $tasks              \taktwerk\yiiboilerplate\modules\queue\models\QueueJob[]
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/31/2017
 * Time: 11:08 AM
 */
?>
<li class="dropdown notifications-menu tw-notifications">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning"><?= $countNotifications ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?= Yii::t('app', 'You have {count} unread notifications', ['count' => $countNotifications]) ?></li>
        <li>
            <ul class="menu">
                <?php foreach ($notifications as $notification) : ?>
                    <?= $this->render('_notification_item', compact('notification')) ?>
                <?php endforeach; ?>
            </ul>
        </li>
        <li class="footer"><a href="<?= \yii\helpers\Url::toRoute(['/notification']) ?>">View all</a></li>
    </ul>
</li>

<li class="dropdown tasks-menu tw-tasks">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-danger"><?= $countTasks ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?= Yii::t('app', 'You have {count} tasks', ['count' => $countTasks]) ?></li>
        <li>
            <ul class="menu">
                <?php foreach ($tasks as $task) : ?>
                    <?= $this->render('_task_item', compact('task')) ?>
                <?php endforeach; ?>
            </ul>
        </li>
<!--        <li class="footer">-->
<!--            <a href="#">View all tasks</a>-->
<!--        </li>-->
    </ul>
</li>