<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 * @var $task \taktwerk\yiiboilerplate\modules\queue\models\QueueJob
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/31/2017
 * Time: 11:08 AM
 */
use yii\helpers\Url;

?>
<li>
    <a href="#">
        <h3>
            <?= $task->toString ?>
            <?= $task->progress != null ? '<small class="pull-right">' . $task->progress . '%</small>' : '' ?>
        </h3>
        <?php if ($task->progress != null && $task->status = \taktwerk\yiiboilerplate\modules\queue\models\QueueJob::STATUS_RUNNING) : ?>
            <div class="progress xs">
                <div class="progress-bar progress-bar-aqua active progress-bar-striped" style="width: <?= $task->progress ?>%" role="progressbar"
                     aria-valuenow="<?= $task->progress ?>" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only"><?= $task->progress ?>% Complete</span>
                </div>
            </div>
        <?php else: ?>
            <small><?= Yii::t('app', 'Queued') ?></small>
        <?php endif; ?>
    </a>
</li>
