<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\notification;

class Module extends \yii\base\Module
{

    public $defaultRoute = 'notification';
    /**
     * @var bool
     * To use Ajax request to refreshing widgets
     */
    public $useAjax = true;

    /**
     * @var int
     * Interval in seconds for Ajax calls
     */
    public $refreshInterval = 10;
}
