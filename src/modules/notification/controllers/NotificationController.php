<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/49eb2de82346bc30092f584268252ed2
 *
 * @package default
 */


namespace taktwerk\yiiboilerplate\modules\notification\controllers;

use dmstr\bootstrap\Tabs;
use taktwerk\yiiboilerplate\modules\notification\widgets\NotificationWidget;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the class for controller "NotificationController".
 */
class NotificationController extends \taktwerk\yiiboilerplate\modules\notification\controllers\base\NotificationController
{

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'refresh',
                            'show',
                            'read',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * @return string
     */
    public function actionRefresh()
    {
        $notification = new NotificationWidget();
        return $notification->renderDropdown();
    }

    /**
     * @param $id
     * @return bool|string
     * @throws \yii\web\HttpException
     */
    public function actionRead($id)
    {
        if (\Yii::$app->request->isAjax) {
            $model = $this->findModel($id);
            if ($model->user_id != \Yii::$app->user->id) {
                $result = [
                    'message' => \Yii::t('app', 'No notification found'),
                ];
            } else {
                $result = [
                    'message' => $model->message,
                ];
                if ($model->read_at == null) {
                    $model->read_at = new Expression('NOW()');
                    $model->save(false);
                }
            }
            return json_encode($result);
        }
        return false;
    }

    /**
     * @param $id
     * @return string
     * @throws Exception
     * @throws \yii\web\HttpException
     */
    public function actionShow($id)
    {
        $model = $this->findModel($id);
        if ($model->user_id != \Yii::$app->user->id) {
            throw new Exception(\Yii::t('app', 'You are not allowed'));
        } else {
            if ($model->read_at == null) {
                $model->read_at = new Expression('NOW()');
                $model->save(false);
            }
            return $this->redirect(Url::to($model->link));
        }
    }

    public function actionView($id, $viewFull = false)
    {
        $resolved = \Yii::$app->request->resolve();
        $resolved[1]['_pjax'] = null;
        $url = Url::to(array_merge(['/' . $resolved[0]], $resolved[1]));
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember($url);
        Tabs::rememberActiveState();
        $modal = $this->findModel($id);
        if ($modal->read_at == null && $modal->user_id == \Yii::$app->user->id) {
            $modal->updateAttributes(['read_at' => new Expression('NOW()')]);
        }
        $modal = $this->findModel($id);
        if ($this->useModal && !$viewFull) {
            return $this->renderAjax('view', [
                'model' => $modal,
                'useModal' => $this->useModal,
            ]);
        } else {
            return $this->render('view', [
                'model' => $modal,
                'useModal' => $this->useModal,
            ]);
        }
    }

}
