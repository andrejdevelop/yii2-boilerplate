<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170531_141734_notification_acl extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $perm = $auth->createPermission('notification');
        $perm->description = 'Notification module';
        $auth->add($perm);

        $user = $auth->getRole('Public');
        $auth->addChild($user, $auth->getPermission('notification'));
    }

    public function down()
    {
        $auth = $this->getAuth();
        $auth->remove($auth->getPermission('notification'));

        echo "m170531_141734_notification_acl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
