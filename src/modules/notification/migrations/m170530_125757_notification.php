<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170530_125757_notification extends TwMigration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%notification}}',
            [
                'id' => Schema::TYPE_UPK . "",
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'message' => Schema::TYPE_TEXT,
                'read_at' => Schema::TYPE_DATETIME,
                'link' => Schema::TYPE_STRING . '(255)',
            ],
            $tableOptions
        );

        $this->createIndex('user_idx', '{{%notification}}', 'user_id');
        $this->createIndex('read_at_idx', '{{%notification}}', 'read_at');

        $this->addForeignKey('notification_fk_user_id', '{{%notification}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%notification}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
