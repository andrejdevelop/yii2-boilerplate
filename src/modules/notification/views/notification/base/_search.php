<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/9104fc58a45fdb0cbb2d50c83525c788
 *
 * @package default
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\notification\models\search\Notification $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="notification-search">

    <?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

            <?php echo $form->field($model, 'id') ?>

        <?php echo $form->field($model, 'user_id') ?>

        <?php echo $form->field($model, 'message') ?>

        <?php echo $form->field($model, 'read_at') ?>

        <?php echo $form->field($model, 'link') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
