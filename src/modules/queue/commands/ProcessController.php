<?php

namespace taktwerk\yiiboilerplate\modules\queue\commands;

use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\queue\models\QueueMessage;
use yii\console\Controller;
use Yii;
use Exception;
use DateTime;

/**
 * Class QueueCommand
 * @package taktwerk\yiiboilerplate\modules\queue\command
 */
class ProcessController extends Controller
{
    /**
     * The command being executed
     * @var
     */
    protected $command;

    /**
     * Run a queued job
     */
    public function actionIndex()
    {
        // Cleanup dead jobs
        $this->cleanupHanging();

        // Find a queued job and process it
        $queued = QueueJob::find()
            ->andWhere(['status' => QueueJob::STATUS_QUEUED])
            ->andWhere([
                'or',
                'execute_at IS NULL',
                "execute_at >= '" . date('Y-m-d H:i:s') . "'"
            ])
            ->all();

        foreach ($queued as $job) {
            // Ignore jobs where the user is already running something in the background
            $running = QueueJob::findOne(['status' => QueueJob::STATUS_RUNNING, 'created_by' => $job->created_by]);
            if (empty($running)) {
                $this->process($job);
                // Only want to run one job in the background to avoid overworking the server
                //break;
            }
        }

        $this->stdout("Finished running jobs.\n");
        return true;
    }

    /**
     * @param QueueJob $job
     */
    protected function process(QueueJob $job)
    {
        set_time_limit(0);

        // Start working on it
        $job->status = QueueJob::STATUS_RUNNING;
        $job->save();

        // Try and execute it
        try {
            // Create the object and check it
            $this->command = Yii::createObject($job->command);

            if (empty($this->command)) {
                throw new Exception('Couldn\'t create object for ' . $job->command);
            }
            if (!$this->command instanceof BackgroundCommandInterface) {
                throw new Exception('Command ' . $job->command . ' doesn\'t implement the BackgroundCommand interface');
            }
            $this->command->run($job);

            if (!empty($this->command->log) && is_array($this->command->log)) {
                $job->log = implode("\n", $this->command->log);
            }
            $job->status = QueueJob::STATUS_FINISHED;
            $job->save();

            // Create an alert for the user
            $this->message($job, Yii::t('app', 'Success of process'));

            // Default behaviour is also to notify on success, but can be turned off by jobs by
            // providing a public property send_mail to false
            if (!isset($this->command->send_mail) || $this->command->send_mail === true) {
                if (!$this->notify($job, true)) {
                    $job->error .= "\nMail couldn't be sent.";
                    $job->save();
                }
            }

            $this->stdout('Processed job #' . $job->id . "\n");
        } catch (Exception $e) {
            if (!empty($this->command) && !empty($this->command->log)) {
                $job->log = (is_array($this->command->log) ? implode("\n", $this->command->log) : $this->command->log);
            }
            $job->status = QueueJob::STATUS_FAILED;
            $job->error = $e->getMessage() . "\n" . $e->getTraceAsString();
            $job->save();

            // Duplicate the job
            if ($job->is_reexecuted_on_error == true) {
                $duplicate = new QueueJob();
                $duplicate->duplicate($job);
            }

            $this->message($job, Yii::t('app', 'Failure of process'));

            if (!$this->notify($job)) {
                $job->error .= "\nMail couldn't be sent.";
                $job->save();
            }

            // Throw exception for Sentry to catch
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Save a message for the user
     * @param QueueJob $job
     * @param string $message
     */
    protected function message(QueueJob $job, $message = '')
    {
        $message = new QueueMessage([
            'job_id' => $job->id,
            'user_id' => $job->created_by,
            'message' => $message
        ]);
        $message->save();
    }

    /**
     * Go through running jobs and check if they are considered hanging
     */
    protected function cleanupHanging()
    {
        $queued = QueueJob::find()
            ->andWhere(['status' => QueueJob::STATUS_RUNNING])
            ->all();

        foreach ($queued as $job) {
            $now = time();

            // Use the formatter to translate to the correct datetime
            $started = Yii::$app->getFormatter()->asTimestamp($job->updated_at);

            // If the job started before now, the server timezone is wrong!
            if ($started > $now) {
                throw new Exception(Yii::t('app', 'A queue\'s updated_at is further in time than the current time(), ' .
                    'meaning the timezone settings of the app and db are not synced.'));
            }
            if ($now - $started > (15*60)) {
                $job->status = QueueJob::STATUS_FAILED;
                $job->error .= 'Timed out!';
                $job->save();

                // Create an alert for the user
                $this->message($job, Yii::t('app', 'Job timed out'));
            }
        }
    }

    /**
     * Notify the admins of a job
     * @param QueueJob $job
     * @param boolean $success
     * @return bool
     */
    protected function notify(QueueJob $job, $success = false)
    {
        $mailer = Yii::$app->mailer->compose()
            ->setFrom('support@taktwerk.ch')
            ->setTo(getenv('APP_ADMIN_EMAIL'))
            ->setSubject(getenv('APP_NAME') . ' QueueJob #' . $job->id . ' ' . ($success ? 'success' : 'error'))
            ->setTextBody(($success ? "Success" : "Failure") . " processing QueueJob #" . $job->id . ".\n\n" .
                "Command: " . $job->command . "\n\nLog: " . $job->log . "\n\n" .
                (!$success ? "Error: " . $job->error."\n\n" : null));

        return $mailer->send();
    }
}
