<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueMessage $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Queue Message ' . $model->id . ', ' . Yii::t('cruds', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => 'Queue Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body queue-message-update">

        <h1>
            <?= Yii::t('cruds', 'Queue Message') ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>