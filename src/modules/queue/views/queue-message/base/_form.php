<?php

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueMessage $model
 * @var yii\widgets\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;
?>
<div class="queue-message-form">
    <?php $form = ActiveForm::begin([
        'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
        'id' => 'QueueMessage' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
            'name' => 'QueueMessage',
                ],
    ]);
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            <?php if ($multiple) : ?>
                <?=Html::hiddenInput('update-multiple', true)?>
                <?php foreach ($pk as $id) :?>
                    <?=Html::hiddenInput('pk[]', $id)?>
                <?php endforeach;?>
            <?php endif;?>
                        
        <div class="col-md-6">
            <?php if (!$multiple || ($multiple && isset($show['job_id']))) :?>
            <?= $form->field(
                $model,
                'job_id'
            )
                    ->widget(
                        Select2::classname(),
                        [
                            'data' => ArrayHelper::map(taktwerk\yiiboilerplate\modules\queue\models\QueueJob::find()->all(), 'id', 'toString'),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select a value...'),
                                'id' => 'job_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                (count(taktwerk\yiiboilerplate\modules\queue\models\QueueJob::find()->all()) > 50 ? 'minimumInputLength' : '') => 3,
                                (count(taktwerk\yiiboilerplate\modules\queue\models\QueueJob::find()->all()) > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'QueueJob\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/queue-job/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#job_id_well').show();
                                                $('#job_id_well').html(json.data);
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal)) ? [
                                'append' => [
                                    'content' => [
                                        RelatedForms::widget([
                                            'relatedController' => '/queue-job',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'job_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    )
                    ?>
                
            <div id="job_id_well" class="well col-sm-6"
                style="margin-left: 8px;
                <?=
                taktwerk\yiiboilerplate\modules\queue\models\QueueJob::findOne(['id' => $model->job_id])
                    ->entryDetails == '' ?
                ' display:none;' :
                ''
                ?>">
                <?=
                taktwerk\yiiboilerplate\modules\queue\models\QueueJob::findOne(['id' => $model->job_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\queue\models\QueueJob::findOne(['id' => $model->job_id])->entryDetails :
                ''?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
            </div>
            <?php
            ?>
            <?php endif; ?>
        </div>            
        <div class="col-md-6">
            <?php if (!$multiple || ($multiple && isset($show['message']))) :?>
            <?= $form->field(
                $model,
                'message',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'message') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'id' => Html::getInputId($model, 'message') . $owner
                        ]
                    )
            ?>
            <?php endif; ?>
        </div>            
        <div class="col-md-6">
            <?php if (!$multiple || ($multiple && isset($show['user_id']))) :?>
            <?= $form->field(
                $model,
                'user_id'
            )
                    ->widget(
                        Select2::classname(),
                        [
                            'data' => ArrayHelper::map(app\models\User::find()->all(), 'id', 'toString'),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select a value...'),
                                'id' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                (count(app\models\User::find()->all()) > 50 ? 'minimumInputLength' : '') => 3,
                                (count(app\models\User::find()->all()) > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'User\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/user/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#user_id_well').show();
                                                $('#user_id_well').html(json.data);
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal)) ? [
                                'append' => [
                                    'content' => [
                                        RelatedForms::widget([
                                            'relatedController' => '/user',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    )
                    ?>
                
            <div id="user_id_well" class="well col-sm-6"
                style="margin-left: 8px;
                <?=
                app\models\User::findOne(['id' => $model->user_id])
                    ->entryDetails == '' ?
                ' display:none;' :
                ''
                ?>">
                <?=
                app\models\User::findOne(['id' => $model->user_id])->entryDetails != '' ?
                app\models\User::findOne(['id' => $model->user_id])->entryDetails :
                ''?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
            </div>
            <?php
            ?>
            <?php endif; ?>
        </div>                                            </p>
        <?php $this->endBlock(); ?>
        
        <?= ($relatedType != RelatedForms::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('app', Inflector::camel2words('QueueMessage')),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <div class="col-md-6"<?=!$relatedForm ? ' id="main-submit-buttons"' : ''?>>
        <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord && !$multiple ?
                Yii::t('cruds', 'Create') :
                Yii::t('cruds', 'Save')),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'submit-default'
            ]
        );
        ?>

        <?php if ((!$relatedForm && !$useModal) && !$multiple) { ?>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' .
                ($model->isNewRecord && !$multiple ?
                    Yii::t('cruds', 'Create & New') :
                    Yii::t('cruds', 'Save & New')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-default',
                    'name' => 'submit-new'
                ]
            );
            ?>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' .
                ($model->isNewRecord && !$multiple ?
                    Yii::t('cruds', 'Create & Close') :
                    Yii::t('cruds', 'Save & Close')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-default',
                    'name' => 'submit-close'
                ]
            );
            ?>

            <?php if (!$model->isNewRecord) { ?>
                <?= Html::a(
                    '<span class="glyphicon glyphicon-trash"></span> ' .
                    Yii::t('cruds', 'Delete'),
                    ['delete', 'id' => $model->id],
                    [
                        'class' => 'btn btn-danger',
                        'data-confirm' => '' . Yii::t('cruds', 'Are you sure to delete this item?') . '',
                        'data-method' => 'post',
                    ]
                );
                ?>
            <?php } ?>
        <?php } elseif ($multiple) { ?>
                <?= Html::a(
                    '<span class="glyphicon glyphicon-exit"></span> ' .
                    Yii::t('cruds', 'Close'),
                    $useModal ? false : [],
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
                        'name' => 'close'
                    ]
                );
                ?>
        <?php } else { ?>
            <?= Html::a(
                '<span class="glyphicon glyphicon-exit"></span> ' .
                Yii::t('cruds', 'Close'),
                ['#'],
                [
                    'class' => 'btn btn-danger',
                    'data-dismiss' => 'modal',
                    'name' => 'close'
                ]
            );
            ?>
        <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?= ($relatedForm && $relatedType == \taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms::TYPE_MODAL) ?
            '<div class="clearfix"></div>' :
            ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>
<?php
if ($useModal) {
    \taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
?>