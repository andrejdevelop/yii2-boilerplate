<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueJob $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('app', 'Queue Job') . ' ' . $model->id . ', ' . Yii::t('cruds', 'Edit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Queue Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body queue-job-update">

        <div class="crud-navigation">
            <?= Html::a(
                '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('cruds', 'View'),
                ['view', 'id' => $model->id],
                ['class' => 'btn btn-default']
            ) ?>
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'relatedTypeForm' => $relatedTypeForm,
        ]); ?>

    </div>
</div>