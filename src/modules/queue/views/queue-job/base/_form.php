<?php

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueJob $model
 * @var yii\widgets\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;
?>
    <div class="queue-job-form">
        <?php $form = ActiveForm::begin([
            'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
            'id' => 'QueueJob' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
            'layout' => 'default',
            'enableClientValidation' => true,
            'errorSummaryCssClass' => 'error-summary alert alert-error',
            'action' => $useModal ? $action : '',
            'options' => [
                'name' => 'QueueJob',
            ],
        ]);
        ?>

        <div class="">
            <?php $this->beginBlock('main'); ?>

            <p>
                <?php if ($multiple) : ?>
                    <?=Html::hiddenInput('update-multiple', true)?>
                    <?php foreach ($pk as $id) :?>
                        <?=Html::hiddenInput('pk[]', $id)?>
                    <?php endforeach;?>
                <?php endif;?>

            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['command']))) :?>
                    <?= $form->field(
                        $model,
                        'title',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'title') . $owner
                            ]
                        ]
                    )
                        ->textInput(
                            [
                                'id' => Html::getInputId($model, 'title') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['command']))) :?>
                    <?= $form->field(
                        $model,
                        'command',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'command') . $owner
                            ]
                        ]
                    )
                        ->textarea(
                            [
                                'rows' => 6,
                                'id' => Html::getInputId($model, 'command') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['parameters']))) :?>
                    <?= $form->field(
                        $model,
                        'parameters',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'parameters') . $owner
                            ]
                        ]
                    )
                        ->textarea(
                            [
                                'rows' => 6,
                                'id' => Html::getInputId($model, 'parameters') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['log']))) :?>
                    <?= $form->field(
                        $model,
                        'log',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'log') . $owner
                            ]
                        ]
                    )
                        ->textarea(
                            [
                                'rows' => 6,
                                'id' => Html::getInputId($model, 'log') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['error']))) :?>
                    <?= $form->field(
                        $model,
                        'error',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'error') . $owner
                            ]
                        ]
                    )
                        ->textarea(
                            [
                                'rows' => 6,
                                'id' => Html::getInputId($model, 'error') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['status']))) :?>
                    <?= $form->field(
                        $model,
                        'status',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'status') . $owner
                            ]
                        ]
                    )
                        ->widget(
                            Select2::classname(),
                            [
                                'options' => [
                                    'id' => Html::getInputId($model, 'status') . $owner
                                ],
                                'data' => [
                                    'queued' => Yii::t('app', 'Queued'),
                                    'running' => Yii::t('app', 'Running'),
                                    'finished' => Yii::t('app', 'Finished'),
                                    'failed' => Yii::t('app', 'Failed'),
                                ],
                                'hideSearch' => 'true',
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['is_reexecuted_on_error']))) :?>
                    <?= $form->field(
                        $model,
                        'is_reexecuted_on_error',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'is_reexecuted_on_error') . $owner
                            ]
                        ]
                    )
                        ->checkbox(
                            [
                                'id' => Html::getInputId($model, 'is_reexecuted_on_error') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['fail_minute_delay']))) :?>
                    <?= $form->field(
                        $model,
                        'fail_minute_delay',
                        [
                            'selectors' => [
                                'input' => '#' .
                                    Html::getInputId($model, 'fail_minute_delay') . $owner
                            ]
                        ]
                    )
                        ->textInput(
                            [
                                'id' => Html::getInputId($model, 'fail_minute_delay') . $owner
                            ]
                        )
                    ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if (!$multiple || ($multiple && isset($show['execute_at']))) :?>
                    <?= $form->field($model, 'execute_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                        'ajaxConversion' => true,
                        'options' => [
                            'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                            'pickerButton' => ['icon' => 'time'],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                                'class' => 'form-control'
                            ]
                        ],
                    ])
                    ?>
                <?php endif; ?>
            </div>   </p>
            <?php $this->endBlock(); ?>

            <?= ($relatedType != RelatedForms::TYPE_TAB) ?
                Tabs::widget([
                    'encodeLabels' => false,
                    'items' => [
                        [
                            'label' => Yii::t('app', Inflector::camel2words('QueueJob')),
                            'content' => $this->blocks['main'],
                            'active' => true,
                        ],
                    ]
                ])
                : $this->blocks['main']
            ?>
            <div class="col-md-12">
                <hr/>

            </div>

            <div class="clearfix"></div>
            <?php echo $form->errorSummary($model); ?>
            <div class="col-md-6"<?=!$relatedForm ? ' id="main-submit-buttons"' : ''?>>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> ' .
                    ($model->isNewRecord && !$multiple ?
                        Yii::t('cruds', 'Create') :
                        Yii::t('cruds', 'Save')),
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-success',
                        'name' => 'submit-default'
                    ]
                );
                ?>

                <?php if ((!$relatedForm && !$useModal) && !$multiple) { ?>
                    <?= Html::submitButton(
                        '<span class="glyphicon glyphicon-check"></span> ' .
                        ($model->isNewRecord && !$multiple ?
                            Yii::t('cruds', 'Create & New') :
                            Yii::t('cruds', 'Save & New')),
                        [
                            'id' => 'save-' . $model->formName(),
                            'class' => 'btn btn-default',
                            'name' => 'submit-new'
                        ]
                    );
                    ?>
                    <?= Html::submitButton(
                        '<span class="glyphicon glyphicon-check"></span> ' .
                        ($model->isNewRecord && !$multiple ?
                            Yii::t('cruds', 'Create & Close') :
                            Yii::t('cruds', 'Save & Close')),
                        [
                            'id' => 'save-' . $model->formName(),
                            'class' => 'btn btn-default',
                            'name' => 'submit-close'
                        ]
                    );
                    ?>

                    <?php if (!$model->isNewRecord) { ?>
                        <?= Html::a(
                            '<span class="glyphicon glyphicon-trash"></span> ' .
                            Yii::t('cruds', 'Delete'),
                            ['delete', 'id' => $model->id],
                            [
                                'class' => 'btn btn-danger',
                                'data-confirm' => '' . Yii::t('cruds', 'Are you sure to delete this item?') . '',
                                'data-method' => 'post',
                            ]
                        );
                        ?>
                    <?php } ?>
                <?php } elseif ($multiple) { ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-exit"></span> ' .
                        Yii::t('cruds', 'Close'),
                        $useModal ? false : [],
                        [
                            'id' => 'save-' . $model->formName(),
                            'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
                            'name' => 'close'
                        ]
                    );
                    ?>
                <?php } else { ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-exit"></span> ' .
                        Yii::t('cruds', 'Close'),
                        ['#'],
                        [
                            'class' => 'btn btn-danger',
                            'data-dismiss' => 'modal',
                            'name' => 'close'
                        ]
                    );
                    ?>
                <?php } ?>
            </div>
            <?php ActiveForm::end(); ?>
            <?= ($relatedForm && $relatedType == \taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms::TYPE_MODAL) ?
                '<div class="clearfix"></div>' :
                ''
            ?>
            <div class="clearfix"></div>
        </div>
    </div>
<?php
if ($useModal) {
    \taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
?>