<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueJob $model
 * @var boolean $useModal
 */
$copyParams = $model->attributes;

$this->title = Yii::t('app', 'Queue Job') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Queue Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'View');
?>
<div class="box box-default">
    <div class="giiant-crud box-body" id="queue-job-view">

        <!-- flash message -->
        <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= \Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
                <!-- menu buttons -->
                <div class='pull-left'>
                    <?= Yii::$app->getUser()->can(
                        Yii::$app->controller->module->id .
                        '_' .
                        \Yii::$app->controller->id .
                        '_update'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('cruds', 'Edit'),
                            ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info']
                        )
                        :
                        ''
                    ?>
                    <?= Yii::$app->getUser()->can(
                        Yii::$app->controller->module->id .
                        '_' .
                        \Yii::$app->controller->id .
                        '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('cruds', 'Copy'),
                            ['create', 'id' => $model->id, 'QueueJob' => $copyParams],
                            ['class' => 'btn btn-success']
                        )
                        :
                        ''
                    ?>
                    <?= Yii::$app->getUser()->can(
                        Yii::$app->controller->module->id .
                        '_' .
                        \Yii::$app->controller->id .
                        '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('cruds', 'New'),
                            ['create'],
                            ['class' => 'btn btn-success']
                        )
                        :
                        ''
                    ?>
                </div>
                <div class="pull-right">
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> ' . Yii::t('cruds', 'List QueueJobs'),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                </div>

            </div>
        <?php endif; ?>
        <?php $this->beginBlock('app\models\QueueJob'); ?>


        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'command',
                'progress',
                'parameters:ntext',
                'log:ntext',
                'error:ntext',
                'is_reexecuted_on_error:boolean',
                'fail_minute_delay',
                'execute_at',

                [
                    'attribute' => 'status',
                    'content' => function ($model) {
                        return \Yii::t('app', $model->status);
                    },
                ],
            ],
        ]); ?>


        <hr/>

        <?= Yii::$app->getUser()->can(
            Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_delete'
        ) ?
            Html::a(
                '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('cruds', 'Delete'),
                $useModal ? false : ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger' . ($useModal ? ' ajaxDelete' : ''),
                    'data-url' => Url::toRoute(['delete', 'id' => $model->id]),
                    'data-confirm' => $useModal ? false : Yii::t('cruds', 'Are you sure to delete this item?'),
                    'data-method' => $useModal ? false : 'post',
                ]
            )
            :
            ''
        ?>

        <?php $this->endBlock(); ?>



        <?php $this->beginBlock('Queue Messages'); ?>
        <?php if ($useModal !== true) : ?>
            <div style='position: relative'>
                <div style='position:absolute; right: 0px; top: 0px;'>
                    <?=Html::a(
                        '<span class="glyphicon glyphicon-list"></span>' . Yii::t('cruds', 'List All') .
                        ' ' .
                        Yii::t('cruds', 'Queue  Messages'),
                        ['queue-message/index'],
                        ['class' => 'btn text-muted btn-xs']
                    )
                    ?>
                    <?=Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('cruds', 'New') .
                        ' ' .
                        Yii::t('cruds', 'Queue  Message'),
                        [
                            'queue-message/create',
                            'QueueMessage' => [
                                'job_id' => $model->id
                            ]
                        ],
                        ['class' => 'btn btn-success btn-xs']
                    )
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
        <?php Pjax::begin(
            [
                'id' => 'pjax-QueueMessages',
                'enableReplaceState' => false,
                'linkSelector' => '#pjax-QueueMessages ul.pagination a, th a',
                'clientOptions' => [
                    'pjax:success' => 'function(){alert("yo")}'
                ]
            ]
        ) ?>
        <div class="table-responsive">
            <?=\yii\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getQueueMessages(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-queuemessages'
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => Yii::t('cruds', 'First'),
                    'lastPageLabel'  => Yii::t('cruds', 'Last')
                ],
                'columns' => [
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update}',
                        'contentOptions' => ['nowrap'=>'nowrap'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            // using the column name as key, not mapping to 'id' like the standard generator
                            $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                            $params[0] = 'queue-message' . '/' . $action;
                            return $params;
                        },
                        'buttons' => [

                        ],
                        'controller' => 'queue-message'
                    ],
                    'id',
                    'message:ntext',
                    'user_id',

                ]
            ])?>
        </div>
        <?php Pjax::end() ?>
        <?php $this->endBlock() ?>


        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['app\models\QueueJob'],
                        'active' => true,
                    ],
                    [
                        'content' => $this->blocks['Queue Messages'],
                        'label' => '<small>' .
                            Yii::t('app', 'Queue Messages') .
                            '&nbsp;<span class="badge badge-default">' .
                            count($model->getQueueMessages()->asArray()->all()) .
                            '</span></small>',
                        'active' => false,
                    ],
                ]
            ]
        );
        ?>
        <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $model]) ?>
    </div>
</div>
