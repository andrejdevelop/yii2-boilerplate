<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueJob $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Queue Job ' . $model->id . ', ' . Yii::t('cruds', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => 'Queue Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body queue-job-update">

        <h1>
            <?= Yii::t('cruds', 'Queue Job') ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>