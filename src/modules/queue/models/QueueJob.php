<?php

namespace taktwerk\yiiboilerplate\modules\queue\models;

use Yii;
use DateTime;
use \taktwerk\yiiboilerplate\modules\queue\models\base\QueueJob as BaseQueueJob;

/**
 * This is the model class for table "queue_job".
 */
class QueueJob extends BaseQueueJob
{

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    /**
     * Queue a job
     * @param string $title
     * @param string $command
     * @param array $parameters
     * @param boolean $reexecute
     * @param integer $delay
     * @return mixed
     */
    public function queue($title, $command, $parameters = array(), $reexecute = false, $delay = 15)
    {
        $this->title = $title;
        $this->command = $command;
        $this->parameters = json_encode($parameters);
        $this->status = QueueJob::STATUS_QUEUED;
        $this->is_reexecuted_on_error = $reexecute ? 1 : 0;
        $this->fail_minute_delay = $delay;
        return $this->save();
    }

    /**
     * Duplicate a job that has failed to execute it again after a specified delay
     * @param QueueJob $original
     * @return mixed
     */
    public function duplicate(QueueJob $original)
    {
        $this->title = $original->title;
        $this->command = $original->command;
        $this->parameters = $original->parameters;
        $this->status = QueueJob::STATUS_QUEUED;
        $this->created_by = $original->created_by;
        $this->fail_minute_delay = $original->fail_minute_delay;

        $date = new DateTime();
        $date->modify('+' . (!empty($original->fail_minute_delay) ? $original->fail_minute_delay : 15) . ' minutes');
        $this->execute_at = $date->format('Y-m-d H:i:s');

        return $this->save(false);
    }
}
