<?php

namespace taktwerk\yiiboilerplate\modules\queue\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\queue\models\base\QueueMessage as BaseQueueMessage;

/**
 * This is the model class for table "queue_message".
 */
class QueueMessage extends BaseQueueMessage
{

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
