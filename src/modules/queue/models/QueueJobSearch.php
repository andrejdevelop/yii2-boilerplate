<?php

namespace taktwerk\yiiboilerplate\modules\queue\models;

use taktwerk\yiiboilerplate\traits\SearchTrait;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * QueueJobSearch represents the model behind the search form about `taktwerk\yiiboilerplate\modules\queue\models\QueueJob`.
 */
class QueueJobSearch extends QueueJob
{
    use SearchTrait;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'command',
                    'parameters',
                    'status',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'title',
                    'error',
                    'progress',
                    'execute_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QueueJob::find();
        if (!isset($params['sort'])) {
            $query->OrderBy(['id' => SORT_DESC, 'title' => SORT_ASC]);
        }

        $this->parseSearchParams(QueueJob::className(), $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => $this->parseSortParams(QueueJob::className()),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(QueueJob::className()),
                'params' => [
                    'page' => $this->parsePageParams(QueueJob::className()),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyLikeOperator('command', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('error', $query);
        $this->applyLikeOperator('log', $query);
        $this->applyHashOperator('status', $query);
        $this->applyHashOperator('progress', $query);

        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);


        $this->applyDateOperator('execute_at', $query, true);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);

        return $dataProvider;
    }
}
