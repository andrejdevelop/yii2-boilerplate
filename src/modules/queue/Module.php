<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\queue;

use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

/**
 * Class Module.
 *
 * @author Tobias Munk <tobias@diemeisterei.de>
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $defaultRoute = 'index';

    /**
     * Init module
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\queue\commands';
        }
    }
}
