<?php

namespace taktwerk\yiiboilerplate\modules\queue\controllers\base;

use taktwerk\yiiboilerplate\modules\queue\models\QueueMessage;
use taktwerk\yiiboilerplate\modules\queue\models\QueueMessageSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use dmstr\bootstrap\Tabs;
use taktwerk\yiiboilerplate\traits\DependingTrait;
use taktwerk\yiiboilerplate\modules\backend\actions\RelatedFormAction;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 * QueueMessageController implements the CRUD actions for QueueMessage model.
 */
class QueueMessageController extends Controller
{

    /**
     * Depending Trait for Crud generation
     */
    use DependingTrait;

    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;

    /**
     * @var boolean whether to enable inline sub-forms
     */
    protected $inlineForm = false;

    /**
     * @var boolean whether to enable modal editing / viewing
     */
    protected $useModal = false;

    /**
     * @var boolean show import option
     */
    protected $importer = false;

    /**
     * @var string Which type of related form should show, modal or tab
     */
    protected $relatedTypeForm = RelatedForms::TYPE_TAB;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'delete-multiple',
                            'related-form',
                            'update-multiple',
                            'entry-details',
                            'list',
                            'depend'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * Lists all QueueMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QueueMessageSearch;
        $dataProvider = $searchModel->search($_GET);

        Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'useModal' => $this->useModal,
            'importer' => $this->importer,
        ]);
    }

    /**
     * Displays a single QueueMessage model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id, $viewFull = false)
    {
        $resolved = \Yii::$app->request->resolve();
        $resolved[1]['_pjax'] = null;
        $url = Url::to(array_merge(['/' . $resolved[0]], $resolved[1]));
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember($url);
        Tabs::rememberActiveState();
        if ($this->useModal && !$viewFull) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                'useModal' => $this->useModal,
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'useModal' => $this->useModal,
            ]);
        }
    }

    /**
     * Creates a new QueueMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QueueMessage;
        try {
            if ($model->load($_POST) && $model->save()) {
                if ($this->useModal) {
                    return $this->actionIndex();
                }
                if (isset($_POST['submit-default'])) {
                    return $this->redirect(['update', 'id' => $model->id]);
                } elseif (isset($_POST['submit-new'])) {
                    return $this->redirect(['create']);
                } else {
                    return $this->redirect(['index']);
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        if ($this->useModal) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'action' => Url::toRoute('create'),
                'useModal' => $this->useModal,
                'inlineForm' => $this->inlineForm,
                'relatedTypeForm' => $this->relatedTypeForm,
                ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'inlineForm' => $this->inlineForm,
                'relatedTypeForm' => $this->relatedTypeForm,
                ]);
        }
    }

    /**
     * Updates an existing QueueMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load($_POST) && $model->save()) {
            if ($this->useModal) {
                return $this->actionIndex();
            }
            if (isset($_POST['submit-default'])) {
                return $this->redirect(['update', 'id' => $model->id]);
            } elseif (isset($_POST['submit-new'])) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            if ($this->useModal) {
                return $this->renderAjax('_form', [
                    'model' => $model,
                    'action' => Url::toRoute(['update', 'id' => $model->id]),
                    'useModal' => $this->useModal,
                    'inlineForm' => $this->inlineForm,
                    'relatedTypeForm' => $this->relatedTypeForm,
                    ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'inlineForm' => $this->inlineForm,
                    'relatedTypeForm' => $this->relatedTypeForm,
                    ]);
            }
        }
    }

    /**
     * Deletes an existing QueueMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            \Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous());
        }

        if (\yii::$app->request->isAjax) {
            return $this->actionIndex();
        }

        // TODO: improve detection
        $isPivot = strstr('$id', ',');
        if ($isPivot == true) {
            return $this->redirect(Url::previous());
        } elseif (isset(\Yii::$app->session['__crudReturnUrl']) && \Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null);
            $url = \Yii::$app->session['__crudReturnUrl'];
            \Yii::$app->session['__crudReturnUrl'] = null;

            return $this->redirect($url);
        } else {
            return $this->redirect(['index']);
        }
    }


    public function actionDeleteMultiple()
    {
        $pk = \Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!empty($pk)) {
            foreach ($pk as $id) {
                $this->findModel($id)->delete();
            }
        }
        if (\yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }

    /**
     * Finds the QueueMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QueueMessage the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QueueMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }

    /**
     * Call actions
     */
    public function actions()
    {
        return [
            'related-form' => [
                'class' => RelatedFormAction::className(),
                'model' => isset($_GET['id']) ? $this->findModel($_GET['id']) : new QueueMessage(),
                'depend' => isset($_GET['depend']) ? true : false,
                'dependOn' => isset($_GET['dependOn']) ? true : false,
                'relation' => isset($_GET['relation']) ? $_GET['relation'] : '',
                'relationId' => isset($_GET['relationId']) ? $_GET['relationId'] : '',
                'relationIdValue' => isset($_GET['relationIdValue']) ? $_GET['relationIdValue'] : '',
            ]
        ];
    }


    /**
     * Update multiple models at once
     */
    public function actionUpdateMultiple()
    {
        if (($_POST['no-post'])) {
            if (!isset($_POST['id']) || empty($_POST['id'])) {
                if (\yii::$app->request->isAjax) {
                    return $this->actionIndex();
                }
                return $this->redirect('index');
            }
            $model = new QueueMessage();
            $show = [];
            foreach ($_POST as $element => $value) {
                $show[$element] = $value;
            }
            $method = 'render' . ($this->useModal ? 'Ajax' : '');
            return $this->$method('update-multiple', [
                'model' => $model,
                'show' => $show,
                'pk' => $_POST['id'],
                'useModal' => $this->useModal,
                'action' => Url::toRoute('update-multiple'),
            ]);
        } else {
            if (!isset($_POST['pk']) || isset($_POST['close'])) {
                if (\yii::$app->request->isAjax) {
                    return $this->actionIndex();
                }
                return $this->redirect('index');
            }
            foreach ($_POST['pk'] as $id) {
                $model = $this->findModel($id);
                $model->load($_POST);
                $model->save(false);
            }
            if (\yii::$app->request->isAjax) {
                return $this->actionIndex();
            }
            return $this->redirect('index');
        }
    }

    /**
     * Get details of one entry
     * @param integer $id
     */
    public function actionEntryDetails($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $output = [
                'success' => true,
                'data' => $model->entryDetails,
            ];
        } else {
            $output = [
                'success' => false,
                'message' => 'Model does not exist',
            ];
        }
        echo json_encode($output);
    }

    /**
     * Call a function dynamically
     * @param $m
     * @param null $q
     * @param null $id
     * @return mixed
     */
    public function actionList($m, $q = null, $id = null)
    {
        $function = lcfirst($m) . 'List';
        return QueueMessage::$function($q, $id);
    }
}
