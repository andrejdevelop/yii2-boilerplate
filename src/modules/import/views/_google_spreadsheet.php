<br>
<div class="form-group">
    <label class="control-label col-sm-3" for="input-file"><?= \Yii::t('app', 'Google Spreadsheet URL') ?></label>
    <div class="col-sm-7">
        <?= \yii\bootstrap\Html::input('text', 'google_url', null, ['class' => 'form-control', 'id' => 'google_url']) ?>
    </div>
    <div class="col-sm-1">
        <?= \yii\helpers\Html::button('Prepare', ['class' => 'btn btn-primary', 'id' => 'prepare-google', 'data-url' => \yii\helpers\Url::to(['/import/index/fetch-google']), 'disabled' => true]) ?>
    </div>
</div>
<?= \yii\bootstrap\Html::hiddenInput('google_import', null, ['id' => 'google_import']) ?>
<div class="col-xs-12">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                       aria-expanded="true" aria-controls="collapseOne">
                        <?= Yii::t('app', 'Previous imported URLs') ?>
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <ul>
                        <?php foreach (\taktwerk\yiiboilerplate\models\GoogleSpreadsheet::find()
                                           ->andWhere([
                                               'user_id' => Yii::$app->user->id,
                                               'type' => \taktwerk\yiiboilerplate\models\GoogleSpreadsheet::TYPE_IMPORT
                                           ])->limit(10)->all() as $imports) : ?>
                            <li><?= \yii\helpers\Html::a(
                                    $imports->title,
                                    $imports->url,
                                    ['class' => 'imported-url']
                                ) ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>