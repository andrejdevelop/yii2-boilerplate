<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/14/2017
 * Time: 9:30 AM
 */
/**
 * @var string $pjaxContainer
 * @var string $modelName
 * @var string $returnUrl
 * @var $this \yii\web\View
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\file\FileInput;
use taktwerk\yiiboilerplate\widget\Select2;
use dmstr\bootstrap\Tabs;

?>
<?php

?>
        <div class="progress" style="display: none;">
            <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="0"
                 aria-valuemin="1" aria-valuemax="100" style="min-width: 2em;">
                <span>0%</span>
            </div>
        </div>
    <div id="import-result" style="display: none"></div>
    <div class="alert alert-danger" style="display: none" id="upload-error"></div>
    <div class="alert alert-success" style="display: none" id="upload-success"></div>
<?= Html::tag(
    'p',
    Html::tag('strong', \Yii::t('app', 'Import data from .csv, .txt or .xlsx files')),
    ['class' => 'h5']
) ?>
    <hr>
    <div class="row" id="import-form">
        <div class="import-overlay kv-grid-loading" ></div>
        <?= Html::beginForm(
            [Url::to('import/index/import')],
            'POST',
            ['role' => 'form', 'class' => 'form-horizontal', 'id' => 'form-import']
        ) ?>
        <?php $this->beginBlock('upload-form'); ?>
        <br>
        <div class="form-group">
            <label class="control-label col-sm-3" for="input-file"><?= \Yii::t('app', 'Import File') ?></label>
            <div class="col-sm-7">
                <?= FileInput::widget([
                    'name' => 'input-file',
                    'pjaxContainerId' => $pjaxContainer,
                    'id' => 'input-file',
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'uploadUrl' => Url::to(['/import/index/upload']),
                        'allowedPreviewTypes' => false,
                        'allowedPreviewMimeTypes' => false,
                        'previewSettings' => false,
                        'showPreview' => false,
                        'showRemove' => false,
                        'showUpload' => true,
                        'showCancel' => false,
                        'uploadAsync' => true,
                        'allowedFileExtensions' => [
                            'csv',
                            'xlsx',
                            'xls',
                            'txt',
                        ],
                        'uploadExtraData' => new \yii\web\JsExpression("function (previewId, index) {
                    return {
                        delimiter: $('select[name=\"delimiter\"]').val(),
                    };
                }"),
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => 'function(event, data, previewId, index) {
                            // Call function from assets to process form by showing additional fields
                            if (data.response.data) {
                                prepareForm().then(function(response){
                                    parseForm(data.response);
                                });
                            } else {
                                $(importOverlay).hide();
                                $("#upload-error").html(data.response.response);
                                $("#upload-error").show();
                                removeForm();
                                $("#input-file").fileinput("clear");
                            }
                            $("#prepare-google").prop("disabled", true);
                            $("#google_url").val(null);
                            $("#google_import").val(null);
                            $(this).closest("form").find(".btn-success").prop("disabled", false);
                            $("input[name=\"file_name\"]").val(data.files[0].name.replace("(", "_").replace(")", "_").replace("-", "_"));
                        }',
                        'change' => 'function(event, data, previewId, index) {
                            // Call function from assets to process form by showing additional fields
                            $(this).closest("form").find(".btn-success").prop("disabled", true);
                            removeForm();
                        }',
                        'filepreupload' => 'function(event, data, previewId, index) {
                            $(importOverlay).show();
                        }',
//                        'filebatchselected' => 'function(event, data, previewId, index) {
//                            $(this).fileinput("upload");
//                        }',
                    ],
                ]) ?>
            </div>
        </div>

        <div class="form-group required" id="delimiter-div">
            <label class="control-label col-sm-3" for="delimiter">
                <?= \Yii::t('app', 'Delimiter') ?>
            </label>
            <div class="col-sm-7">
                <?= Select2::widget([
                    'id' => 'delimiter',
                    'pjaxContainerId' => $pjaxContainer,
                    'name' => 'delimiter',
                    'data' => [
                        'auto' => Yii::t('app', 'Auto detect'),
                        ';' => ';',
                        '|' => '|',
                        ',' => ',',
                        '\t' => '\t (Tab)'
                    ],
                    'pluginOptions' => [
                        'minimumResultsForSearch' => 'Infinity',
                    ]
                ]) ?>
            </div>
        </div>
        <?php $this->endBlock() ?>
        <?php $this->beginBlock('google-form') ?>
        <?= $this->render('_google_spreadsheet') ?>
        <?php $this->endBlock() ?>
        <?= Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => Yii::t('app', 'Upload file'),
                        'content' => $this->blocks['upload-form'],
                        'active' => true,
                    ],
                    [
                        'label' => Yii::t('app', 'Import Google Spreadsheet'),
                        'content' => $this->blocks['google-form'],
                        'active' => false,
                        'visible' => \taktwerk\yiiboilerplate\modules\import\models\Import::isGoogleEnabled(),
                    ],
                ]
            ])
        ?>
        <div id="firstRow-div" style="display:none;">
            <div class="form-group required" id="delimiter-div">
                <label class="control-label col-sm-3" for="delimiter">
                    <?= \Yii::t('app', 'Method') ?>
                </label>
                <div class="col-sm-7">
                    <?= Select2::widget([
                        'id' => 'method',
                        'pjaxContainerId' => $pjaxContainer,
                        'name' => 'method',
                        'data' => [
                            'insert' => Yii::t('app', 'Insert'),
                            'update' => Yii::t('app', 'Update'),
                        ],
                        'pluginOptions' => [
                            'minimumResultsForSearch' => 'Infinity',
                        ],
                        'pluginEvents' => [
                            'change' => 'function(){
                                if ($(this).val() == "insert") {
                                    $(".assign-info").hide();
                                    $(".assign").hide();
                                } else {
                                    $(".assign-info").show();
                                    $(".assign").show();
                                }
                            }',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="checkbox">
                        <label for="first_row">
                            <?= Html::checkbox('first_row', false, ['id' => 'first_row']) ?>
                            <?= \Yii::t('app', 'Process first row') ?>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group" id="header-row-div" style="display: none;">
            <div class="col-sm-2 text-right">
                <strong><?= \Yii::t('app', 'Attributes') ?></strong>
                <?= Html::tag(
                    'a',
                    '?',
                    [
                        'class' => 'btn btn-default btn-sm pull-right help-popover',
                        'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 20px',
                        'role' => 'button',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'focus',
                        'tabindex' => 0,
                        'data-container' => 'body',
                        'data-placement' => 'top',
                        'data-title' => \Yii::t('app', 'Attributes'),
                        'data-content' => \Yii::t('app', 'The following rows represent the fields of the model that the data will be imported into. Map as many fields as possible for the best results.'),
                    ]
                ) ?>
            </div>
            <div class="col-sm-2 assign-info" style="display: none;">
                <strong><?= \Yii::t('app', 'Operator') ?></strong>
                <?= Html::tag(
                    'a',
                    '?',
                    [
                        'class' => 'btn btn-default btn-sm pull-right help-popover',
                        'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 20px',
                        'role' => 'button',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'focus',
                        'tabindex' => 0,
                        'data-container' => 'body',
                        'data-placement' => 'top',
                        'data-title' => \Yii::t('app', 'Columns from file'),
                        'data-content' => \Yii::t('app', 'Map the columns from the imported file to the fields of the model.'),
                    ]
                ) ?>
            </div>
            <div class="col-sm-5">
                <strong><?= \Yii::t('app', 'Columns from file') ?></strong>
                <?= Html::tag(
                    'a',
                    '?',
                    [
                        'class' => 'btn btn-default btn-sm pull-right help-popover',
                        'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 20px',
                        'role' => 'button',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'focus',
                        'tabindex' => 0,
                        'data-container' => 'body',
                        'data-placement' => 'top',
                        'data-title' => \Yii::t('app', 'Columns from file'),
                        'data-content' => \Yii::t('app', 'Map the columns from the imported file to the fields of the model.'),
                    ]
                ) ?>
            </div>
            <div class="col-sm-3">
                <strong><?= \Yii::t('app', 'Foreign Lookup') ?></strong>
                <?= Html::tag(
                    'a',
                    '?',
                    [
                        'class' => 'btn btn-default btn-sm pull-right help-popover',
                        'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 20px',
                        'role' => 'button',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'focus',
                        'tabindex' => 0,
                        'data-container' => 'body',
                        'data-placement' => 'top',
                        'data-title' => \Yii::t('app', 'Foreign Lookup'),
                        'data-content' => \Yii::t('app', 'In case the model\'s property is an external model and your data doesn\'t have the
                ID of the foreign data but something else that can identify it, use these options to help us know what
                we are looking for. By default, "Auto ID" assumes the ID of the foreign model'),
                    ]
                ) ?>
            </div>
        </div>

        <div id="mappingPart"
             data-url="<?= Url::toRoute(['import/index/attributes']) ?>">
        </div>
        <?= Html::input('hidden', 'returnUrl', $returnUrl) ?>
        <?= Html::input('hidden', 'model', $modelName) ?>
        <?= Html::input('hidden', 'file_name') ?>

        <hr>

        <div class="col-sm-7 col-sm-offset-3">
            <div class="pull-right">
                <?= Html::button(
                    '<span class="glyphicon glyphicon-check"></span> ' . \Yii::t('app', 'Import'),
                    ['type' => 'submit', 'class' => 'btn btn-success', 'disabled' => true]
                ) ?>
                <?= Html::button(
                    '<span class="glyphicon glyphicon-check"></span> ' . \Yii::t('app', 'Import in background'),
                    [
                        'type' => 'submit',
                        'class' => 'btn btn-success',
                        'name' => 'background',
                        'disabled' => true,
                        'data-url' => Url::toRoute(['/import/index/background'])
                    ]
                ) ?>
            </div>
        </div>
        <?= Html::endForm() ?>
    </div>
<?php
$css = "
.btn-toolbar .btn, .btn-toolbar .btn-group, .btn-toolbar .input-group {
    float: none !important;
}
";
$this->registerCss($css);
