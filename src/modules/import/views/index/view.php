<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\import\models\Import $model
 * @var boolean $useModal
 */
$copyParams = $model->attributes;

$this->title = 'Import ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>
<div class="box box-default">
    <div class="giiant-crud box-body" id="address-view">

        <!-- flash message -->
        <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= \Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>

        <h1>
            <?= Yii::t('app', 'Import') ?>
            <small>
                <?= $model->id ?>
            </small>
        </h1>

<?php if (!$useModal) : ?>
        <div class="clearfix crud-navigation">
            <!-- menu buttons -->
            <div class='pull-left'>
            </div>
            <div class="pull-right">
                <?= Html::a(
                    '<span class="glyphicon glyphicon-list"></span> ' . Yii::t('app', 'List Imports'),
                    ['index'],
                    ['class' => 'btn btn-default']
                ) ?>
            </div>

        </div>
<?php endif; ?>
        <?php $this->beginBlock('\taktwerk\yiiboilerplate\modules\import\models\Import'); ?>

        
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'model',
                [
                    'attribute' => 'log',
                    'format' => 'html',
                    'value' => function ($model) {
                        return nl2br($model->log);
                    }
                ],
            ],
        ]); ?>

        
        <hr/>

        <?= Yii::$app->getUser()->can(
            Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_delete'
        ) ?
            Html::a(
                '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
                ['delete', 'id' => $model->id],
                [
                'class' => 'btn btn-danger',
                'data-confirm' => '' . Yii::t('app', 'Are you sure to delete this item?') . '',
                'data-method' => 'post',
                ]
            )
        :
            ''
        ?>

        <?php $this->endBlock(); ?>

        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['\taktwerk\yiiboilerplate\modules\import\models\Import'],
                        'active' => true,
                    ],
                ]
            ]
        );
        ?>
        <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $model]) ?>
    </div>
</div>
