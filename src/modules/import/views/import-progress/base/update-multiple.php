<?php
/**
 * /srv/www/nassi-v2/src/../runtime/giiant/b18644106c982bf7c91ad36ce7219022
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\import\models\ImportProgress $model
 * @var array $pk
 * @var array $show
 */
$this->title = 'Import Progress ' . $model->id . ', ' . Yii::t('app', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => 'Import Progresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body import-progress-update">

        <h1>
            <?php echo Yii::t('app', 'Import Progress') ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
		'model' => $model,
		'pk' => $pk,
		'show' => $show,
		'multiple' => true,
		'useModal' => $useModal,
		'action' => $action,
	]); ?>

    </div>
</div>
