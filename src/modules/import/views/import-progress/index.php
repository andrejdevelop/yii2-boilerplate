<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * /srv/www/nassi-v2/src/../runtime/giiant/8d7617987c202c306c0b7e366206e7e4
 *
 * @package default
 */


use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\grid\GridView;
use kartik\export\ExportMenu;
// use yii\grid\GridView;
use yii\web\View;

/**
 *
 * @var yii\web\View                                                       $this
 * @var yii\data\ActiveDataProvider                                        $dataProvider
 * @var boolean                                                            $useModal
 * @var boolean                                                            $importer
 * @var taktwerk\yiiboilerplate\modules\import\models\ImportProgressSearch $searchModel
 */
$this->title = Yii::t('app', 'Import Progresses');
$this->params['breadcrumbs'][] = $this->title;


/* ------- Multiple-Delete Batch Action ------ */
$inlineScript = 'var gridViewKey = "import-progress", useModal = ' . ($useModal ? 'true' : 'false') . ';';
$this->registerJs($inlineScript, View::POS_HEAD, 'my-inline-js');

$gridColumns = [
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_id',
        'format' => 'html',
        'content' => function ($model) {
            return Html::a($model->user->toString, ['user/view', 'id' => $model->user_id]);
        },
        'filter' => \yii\helpers\ArrayHelper::map(app\models\User::find()->all(), 'id', 'toString'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => [
                'placeholder' => '',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
    ],
    'model',
    'pid',
    [
        'attribute' => 'status',
        'content' => function ($model) {
            return \Yii::t('app', $model->status);
        },
        'filter' => [
            'Finished' => Yii::t('app', 'Finished'),
            'Running' => Yii::t('app', 'Running'),
            'Error' => Yii::t('app', 'Error'),
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'class' => '\kartik\grid\DataColumn',
        'filterWidgetOptions' => [
            'options' => [
                'placeholder' => ''
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
    ],
    [
        'format' => 'html',
        'label' => Yii::t('app', 'Progress'),
        'content' => function ($model) {
            $percentage = floor(
                (
                    $model->current_row /
                    // Prevent division by zero
                    ($model->total_rows == 0 ?
                        1 :
                        $model->total_rows)
                )
                * 100
            );

            return "<div class=\"progress-bar progress-bar-striped progress-bar-success active\" role=\"progressbar\" aria-valuenow=\"$percentage\"
                 aria-valuemin=\"1\" aria-valuemax=\"100\" style=\"min-width: 2em; width: {$percentage}%;\">
                <span>{$percentage}%</span>
            </div>
";
        },
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'urlCreator' => function ($action, $model, $key, $index) {

            /**
             *
             * @var \yii\db\ActiveRecord $model
             */
            // using the column name as key, not mapping to 'id' like the standard generator
            $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
            $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
            return Url::toRoute($params);
        },
        'contentOptions' => [
            'nowrap' => 'nowrap',
        ],
        'template' =>
            (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
                '_' .
                \Yii::$app->controller->id . '_delete') ? '{delete}' : ''),
        'buttons' => $useModal ? [
            'delete' => function ($url, $model, $key) use ($useModal) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $useModal ? '#modalForm' : $url, [
                    'title' => Yii::t('app', 'Delete'),
                    'data-url' => $url,
                    'data-pjax' => 1,
                    'class' => 'ajaxDelete',
                ]);
            },
            'stop' => function ($url, $modal, $key) use ($useModal) {
                return Html::a('<span class="glyphicon glyphicon-stop"></span>', $useModal ? '#modalForm' : $url, [
                    'title' => Yii::t('app', 'Stop'),
                    'data-toggle' => 'modal',
                    'data-url' => $url,
                    'data-pjax' => 1,
                ]);
            },
        ] : [
            'stop' => function ($url, $modal, $key) use ($useModal) {
                return Html::a('<span class="glyphicon glyphicon-stop"></span>', $url, [
                    'title' => Yii::t('app', 'Stop'),
                ]);
            },
        ],
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'headerOptions' => [
            'class' => 'kartik-sheet-style'
        ]
    ],
];
$exportColumns = $gridColumns;
foreach ($exportColumns as $column => $value) {
    // Remove checkbox and action columns from Excel export
    if (isset($value['class']) &&
        (strpos($value['class'], 'CheckboxColumn') !== false || strpos($value['class'], 'ActionColumn') !== false)
    ) {
        unset($exportColumns[$column]);
    }
}
?>
<?php $this->beginBlock('info');
\yii\bootstrap\Modal::begin([
    'header' => '<h2>' . Yii::t('app', 'Information') . '</h2>',
    'toggleButton' => [
        'tag' => 'btn',
        'label' => '?',
        'class' => 'btn btn-default',
        'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px',
    ],
]); ?><?php echo $this->render('@taktwerk-boilerplate/views/_info_modal') ?><?php \yii\bootstrap\Modal::end();
$this->endBlock(); ?>
<div class="box box-default">
    <div class="giiant-crud box-body import-progress-index">
        <?php
        // echo $this->render('_search', ['model' =>$searchModel]);
        ?>

        <div class="table-responsive">
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => [
                    'id' => 'import-progress-grid'
                ],
                'columns' => $gridColumns,
                'containerOptions' => [
                    'style' => 'overflow: auto'
                ], // only set when $responsive = false
                'headerRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'filterRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'toolbar' => [
                    [
                        'content' => $this->blocks['info'],
                    ],
                    [
                        'content' => \taktwerk\yiiboilerplate\widget\FilterWidget::widget(['model' => $searchModel, 'pjaxContainer' => 'import-progress-pjax-container'])
                    ],
                    [
                        'content' => Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            ['index', 'reset' => 1],
                            ['class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')]
                        )
                    ],
                    '{export}',
                    ($importer && Yii::$app->user->can('import')) ?
                        \taktwerk\yiiboilerplate\modules\import\widget\Importer::widget([
                            'dataProvider' => $dataProvider,
                            'header' => Yii::t('app', 'Import'),
                            'pjaxContainerId' => 'import-progress-pjax-container',
                        ]) : '',
                    '{toggleData}',
                    [
                        'content' => \yii\bootstrap\ButtonDropdown::widget([
                            'id' => 'tw-actions',
                            'encodeLabel' => false,
                            'label' => '<span class="glyphicon glyphicon-flash"></span> ' . Yii::t('app', 'Selected'),
                            'dropdown' => [
                                'options' => [
                                    'class' => 'dropdown-menu-right'
                                ],
                                'encodeLabels' => false,
                                'items' => [
                                    (Yii::$app->getUser()->can(
                                        Yii::$app->controller->module->id .
                                        '_' .
                                        \Yii::$app->controller->id .
                                        '_delete-multiple'
                                    ) ?
                                        [
                                            'url' => [
                                                false
                                            ],
                                            'options' => [
                                                'onclick' => 'deleteMultiple(this);',
                                                'data-url' => Url::toRoute('delete-multiple')
                                            ],
                                            'label' => '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' .
                                                Yii::t('app', 'Remove'),
                                        ] : ''),
                                    (Yii::$app->getUser()->can(
                                        Yii::$app->controller->module->id .
                                        '_' .
                                        \Yii::$app->controller->id .
                                        '_update-multiple'
                                    ) ?
                                        [
                                            'url' => '#edit-multiple',
                                            'linkOptions' => [
                                                'data-toggle' => 'modal',
                                            ],
                                            'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' .
                                                Yii::t('app', 'Update'),
                                        ] : ''),
                                ]
                            ],
                            'options' => [
                                'class' => 'btn-default'
                            ]
                        ])
                    ],
                ],
                'panel' => [
                    'heading' => "<h3 class=\"panel-title\"><i class=\"glyphicon glyphicon-list\"></i>  " .
                        Yii::t('app', 'Import Progresses') .
                        "</h3>" . \taktwerk\yiiboilerplate\modules\import\widget\ImportResult::widget(),
                    'type' => 'default',
                    'before' => (\Yii::$app->getUser()->can(
                            Yii::$app->controller->module->id .
                            '_' .
                            \Yii::$app->controller->id .
                            '_create'
                        ) ?
                            Html::a(
                                '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'New'),
                                [$useModal ? '#modalForm' : 'create'],
                                [
                                    'class' => 'btn btn-success',
                                    'data-pjax' => $useModal,
                                    'data-toggle' => 'modal',
                                    'data-url' => Url::toRoute('create')
                                ]
                            ) : '') . ' ' . (\Yii::$app->getUser()->can('Administrator') ?
                            \yii\bootstrap\ButtonDropdown::widget([
                                'id' => 'giiant-relations',
                                'encodeLabel' => false,
                                'label' => '<span class="glyphicon glyphicon-paperclip"></span> ' .
                                    Yii::t('app', 'Relations'),
                                'dropdown' => [
                                    'options' => [
                                    ],
                                    'encodeLabels' => false,
                                    'items' => [
                                        [
                                            'url' => ['/user/admin'],
                                            'label' => '<i class="glyphicon glyphicon-arrow-right">&nbsp;</i>' .
                                                Yii::t('app', 'User'),
                                        ],
                                    ]
                                ],
                                'options' => [
                                    'class' => 'btn-default'
                                ]
                            ])
                            : '')
                    ,
                    'after' => '{pager}',
                    'footer' => false
                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true,
                    'label' => Yii::t('kvgrid', 'Export'),
                ],
                'exportConfig' => [
                    GridView::PDF => [],
                    GridView::HTML => [],
                    GridView::CSV => [],
                    GridView::TEXT => [],
                    GridView::JSON => [],
                    GridView::EXCEL => [
                        //Override default export option with ExportMenu Widget
                        'external' => true,
                        'label' => ExportMenu::widget([
                            'asDropdown' => false,
                            'dataProvider' => $dataProvider,
                            'showColumnSelector' => false,
                            'columns' => $exportColumns,
                            'fontAwesome' => true,
                            'exportConfig' => [
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_EXCEL => false,
                                ExportMenu::FORMAT_TEXT => false,
                            ],
                        ]),
                    ],
                ],
                'striped' => true,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'import-progress-pjax-container',
                    ],
                    'clientOptions' => [
                        'method' => 'POST'
                    ]
                ],
                'hover' => true,
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => Yii::t('kvgrid', 'First'),
                    'lastPageLabel' => Yii::t('kvgrid', 'Last')
                ],
            ])
            ?>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    'header' => '<h4>' . Yii::t('app', 'Choose fields to edit') . ':</h4>',
    'id' => 'edit-multiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php echo Html::beginForm(['update-multiple'], 'POST'); ?>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="user_id">
                <input
                    type="checkbox"
                    id="user_id"
                    name="user_id"
                    value="1"
                >User
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="model">
                <input
                    type="checkbox"
                    id="model"
                    name="model"
                    value="1"
                >Model
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="pid">
                <input
                    type="checkbox"
                    id="pid"
                    name="pid"
                    value="1"
                >Pid
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="total_rows">
                <input
                    type="checkbox"
                    id="total_rows"
                    name="total_rows"
                    value="1"
                >Total Rows
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="current_row">
                <input
                    type="checkbox"
                    id="current_row"
                    name="current_row"
                    value="1"
                >Current Row
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="message">
                <input
                    type="checkbox"
                    id="message"
                    name="message"
                    value="1"
                >Message
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="status">
                <input
                    type="checkbox"
                    id="status"
                    name="status"
                    value="1"
                >Status
            </label>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<button type="submit" class="btn btn-success" id="submit-multiple">Update</button>
<?php echo Html::endForm(); ?>

<?php \yii\bootstrap\Modal::end();
?>

<?php
$js = <<<JS
jQuery.fn.addHidden = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};
$(document).ready(function () {
    $('#submit-multiple').on('click', function(e){
        e.preventDefault();
        var keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
            form = $(this).closest('form');
        // Remove old values to prevent duplicated Ids submit
        form.find('input[name="id[]"]').remove();
        form.addHidden('no-post', true);
        $.each(keys , function (key, value) {
            form.addHidden('id[]', value);
        });
        if (useModal) {
            $('body').addClass('kv-grid-loading');
            $.post(
                form.attr("action"),
                form.serialize()
            )
            .done(function (data) {
                $('#modalFormMultiple').modal('show');
                $('#modalFormMultiple').find('.modal-body').html(data);
                $('body').removeClass('kv-grid-loading');
                formSubmitEvent();
                $('.closeMultiple').on('click', function(e){
                    e.preventDefault();
                    $('#modalFormMultiple').modal('hide');
                });
            })
            return false;
        } else {
            form.submit();
        }
    });
});
JS;
$this->registerJs($js);
?>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_LARGE,
    'id' => 'modalFormMultiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \yii\bootstrap\Modal::end();
?>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_LARGE,
    'id' => 'modalForm',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \yii\bootstrap\Modal::end();
?>
<?php
if ($useModal) {
    \taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
?>
