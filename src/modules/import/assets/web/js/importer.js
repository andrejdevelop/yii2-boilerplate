/*
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by Nikola on 3/6/2017.
 */
pageReloaded = true;
initImport = function () {
    Array.prototype.diff = function (a) {
        return this.filter(function (i) {
            return a.indexOf(i) < 0;
        });
    };
    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function camel2Id(text) {
        if (text !== null && !isNumber(text)) {
            text = text.replace(new RegExp(' ', 'g'), '_');
            text = text.toLowerCase();
        }
        return text;
    }

    function getShortModelName(model) {
        var parts, modelName;
        parts = model.split('\\');
        modelName = parts[parts.length - 1].split('.');
        return modelName[0];
    }

    progress = $('.progress'),
        progressBar = $(progress).children('.progress-bar'),
        progressText = $(progressBar).children('span'),
        importResult = $('#import-result'),
        importOverlay = $('.import-overlay'),
        assignSelectTemplate =
            '<select class="select2-assign" name="assign[{name}]" style="width: 100%">' +
            '<option value="-">-</option>' +
            '<option value="=">=</option>' +
            '</select>',
        elementTemplate =
            '<div class="form-group{required}">' +
            '<label class="control-label col-sm-2">{label}</label>' +
            '<div class="col-sm-2 assign" style="display: none;">' +
            '{assign}' +
            '</div>' +
            '<div class="col-sm-5">' +
            '{element}' +
            '</div>' +
            '<div class="col-sm-3">' +
            '{foreignElement}' +
            '</div>' +
            '</div>';

    prepareForm = function () {
        return new Promise(function (resolve, reject) {
            var div = $('#mappingPart'),
                url = div.attr('data-url'),
                delimiter = $('#delimiter-div'),
                firstRow = $('#firstRow-div'),
                header = $('#header-row-div'),
                helper = $('#helper-div');

            div.html('');
            $.ajax({
                url: url + '?model=' + importModel,
                type: 'get'
            }).done(function (response) {
                var data = $.parseJSON(response);
                prepareFormParse(data);
                resolve('Success');
            }).fail(function () {
                reject('Failure');
            });
        });
    };

    prepareFormParse = function (data) {
        var div = $('#mappingPart'),
            url = div.attr('data-url'),
            delimiter = $('#delimiter-div'),
            firstRow = $('#firstRow-div'),
            header = $('#header-row-div'),
            helper = $('#helper-div');
        div.html('');
        $.each(data, function (key, value) {
            var element = elementTemplate.replace('{label}', value.label),
                select = '<select class="select2-mapping" {required} name="attributes[{name}]" style="width: 100%">' +
                    '</select>';
            if (value.foreign == true) {
                var foreignSelect = '<select class="select2-foreign" name="foreign[{name}]" style="width: 100%">' +
                    '</select>';
                foreignSelect = foreignSelect.replace('{name}', value.name);
                element = element.replace('{foreignElement}', foreignSelect);
            } else {
                element = element.replace('{foreignElement}', '');
            }
            element = element.replace('{element}', select);
            element = element.replace('{name}', value.name);
            if (value.required == true) {
                element = element.replace('{required}', ' required');
            } else {
                element = element.replace('{required}', '');
            }
            var assignSelect = assignSelectTemplate.replace('{name}', value.name);
            element = element.replace('{assign}', assignSelect);
            div.append(element);
        });

        $('.select2-mapping').select2({
            placeholder: "Select a option",
            //minimumResultsForSearch: Infinity,
            allowClear: true,
            theme: 'krajee'
        });
        $('.select2-assign').select2({
            placeholder: "Select a option",
            //minimumResultsForSearch: Infinity,
            theme: 'krajee'
        });
        fillForeign(data);
        firstRow.show();
        header.show();
        helper.show();
        $('.help-popover').popover();
    };
    prepareForeign = function (data) {

    };
    fillForeign = function (data) {
        var selectors = $('.select2-foreign');
        $.each(selectors, function (key, selector) {
            $.each(data, function (key, model) {
                if (model.foreign == true && ('foreign[' + model.name + ']') == $(selector).attr('name')) {
                    $(selector).append(('<optgroup label="' + getShortModelName(model.foreignAttributes[0].name) + '"'));
                    $.each(model.foreignAttributes, function (key, attribute) {
                        $(selector).append($('<option />', {value: attribute.name}).text(attribute.label))
                            .val(model.foreignAttributes[0].name);
                    })
                }
            });
        });
        $(selectors).select2({
            allowClear: true,
            //minimumResultsForSearch: Infinity,
            theme: 'krajee'
        });
    };

    parseForm = function (data) {
        var attributes = $('#mappingPart').find('.select2-mapping'),
            selectors = [],
            processedData = [],
            unprocessedData,
            allData = [];
        $.each(attributes, function (key, selector) {
            selectors.push($(selector).attr('name'));
            $.each(data.data, function (index, value) {
                if (isNumber(value)) {
                    value = value + ' (Column ' + index + ')';
                }
                $(selector).append($('<option />', {value: index}).text(value))
                    .val(index);
            });
        });
        $.each(data.data, function (key, value) {
            allData.push(key);
            if (value != null) {
                value = camel2Id(value);
                if (((foundSelector = selectors.indexOf('attributes[' + value + ']')) !== -1) ||
                    ((foundSelector = selectors.indexOf('attributes[' + value + '_id' + ']')) !== -1)
                ) {
                    $('select[name="' + selectors[foundSelector] + '"]').val(key).change();
                    selectors.splice(foundSelector, 1);
                    processedData.push(key);
                }
            }
        });
        // Fill the rest
        unprocessedData = allData.diff(processedData);
        $.each(unprocessedData, function (key, value) {
            $('select[name="' + selectors[key] + '"]').val(key).change();
        });
        $(importOverlay).hide();
    };

    removeForm = function () {
        var div = $('#mappingPart'),
            firstRow = $('#firstRow-div'),
            header = $('#header-row-div'),
            helper = $('#helper-div');
        div.html('');
        firstRow.hide();
        helper.hide();
        header.hide();
        hideResult();
    };

    updateProgress = function (fromModal) {
        var pjaxContainer = gridViewKey + '-pjax-container';
        $.ajax({
                type: "POST",
                url: statusUrl,
                data: {'model': importModel, 'pageReloaded': pageReloaded},
                async: true
            })
            .done(function (data) {
                pageReloaded = false;
                data = $.parseJSON(data);
                if (data.running == true) {
                    if (data.status == 'Running') {
                        $(importOverlay).show();
                        $(progress).show();
                        $(progressBar).css('width', data.progress + '%').attr('aria-valuenow', data.progress);
                        $(progressText).html(data.progress + '%');
                        setTimeout(updateProgress, interval);
                    } else if (data.status == 'Error') {
                        $(progress).hide();
                        $(importOverlay).hide();
                        var cls = 'alert alert-danger';
                        $(importResult).show();
                        $(importResult).html(data.result);
                        $(importResult).addClass(cls);
                    } else if (data.status == 'Finished') {
                        $.pjax.reload({container: '#' + $.trim(pjaxContainer)});
                        $('#' + importModal).modal('hide');
                        removeForm();
                        pageReloaded = true;
                        $(progress).hide();
                        $(importOverlay).hide();
                    }
                } else {
                    $(progress).hide();
                    $(importOverlay).hide();
                }
            })
        ;
    };

    hideResult = function () {
        $(importResult).hide();
        $(importResult).removeClass();
    };

    $('#form-import').on('submit', function (event) {
        event.preventDefault();
        var form = $(this),
            button = $(document.activeElement).attr('name');
        if (button == 'background') {
            var clickedButton = $('button[name="background"]'),
                url = $(clickedButton).attr('data-url');
            $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    async: true
                })
                .done(function (data) {
                    removeForm();
                    $('#form-import').trigger("reset");
                    $('#' + importModal).modal('hide');

                });
        } else {
            hideResult();
            importOverlay.show();
            progress.show();
            $.ajax({
                    type: "POST",
                    url: form.attr("action"),
                    data: form.serialize(),
                    async: true
                })
                .done(function (data) {
                    setTimeout(updateProgress, interval);
                });
        }
        return false;
    });

    $('#' + importModal).on('show.bs.modal', function (e) {
        if (e.target.id == $(this)[0].id) {
            // Only if is fired from outer modal
            updateProgress();
        }
    });

    $('#prepare-google').on('click', function(e) {
        $(importOverlay).show();
        removeForm();
        $("#input-file").fileinput("clear");
        var url = $(this).attr('data-url'),
            self = $(this);
        $.ajax({
                type: "POST",
                url: url,
                data: {'file_name': $('#google_url').val(), 'type': 'google'},
                async: true
            })
            .done(function (response) {
                data = $.parseJSON(response);
                prepareForm().then(function(response){
                    parseForm(data);
                });
                $(self).closest("form").find(".btn-success").prop("disabled", false);
                $(importOverlay).hide();
            });
    });
    $('#google_url').on('change', function(){
        removeForm();
        $("#input-file").fileinput("clear");
        $('#google_import').val('true');
        if ($(this).val() != '') {
            $('#prepare-google').prop('disabled', false);
        } else {
            $('#prepare-google').prop('disabled', true);
        }
    });

    $('.imported-url').on('click', function(e){
        e.preventDefault();
        $('#google_url').val($(this).attr('href'));
        $('#google_url').trigger('change');
    });
};

initImport();

$(document).on('pjax:success', function () {
    initImport();
    pageReloaded = true;
});