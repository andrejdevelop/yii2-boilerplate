<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170529_071953_import_progress extends TwMigration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%import_progress}}',
            [
                'id' => Schema::TYPE_PK . "",
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'model' => Schema::TYPE_STRING . '(255) NOT NULL',
                'pid' => Schema::TYPE_INTEGER,
                'total_rows' => Schema::TYPE_INTEGER,
                'current_row' => Schema::TYPE_INTEGER,
                'message' => Schema::TYPE_TEXT,
                'status' => "enum('Finished','Running','Error')",
            ],
            $tableOptions
        );

        $this->createIndex('user_idx', '{{%import_progress}}', 'user_id');
        $this->createIndex('model_idx', '{{%import_progress}}', 'model');
        $this->addForeignKey('import_progress_fk_user_user_id', '{{%import_progress}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        echo "m170529_071953_process_import cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
