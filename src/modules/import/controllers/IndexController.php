<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/6/2017
 * Time: 10:42 AM
 */

namespace taktwerk\yiiboilerplate\modules\import\controllers;

use taktwerk\yiiboilerplate\modules\import\models\Import;
use taktwerk\yiiboilerplate\modules\import\models\ImportProgress;
use taktwerk\yiiboilerplate\modules\import\models\ImportSearch;
use taktwerk\yiiboilerplate\modules\import\widget\ImportResult;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use dmstr\bootstrap\Tabs;

class IndexController extends Controller
{

    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;

    /**
     * @var boolean whether to enable inline sub-forms
     */
    protected $inlineForm = false;

    /**
     * @var boolean whether to enable modal editing / viewing
     */
    protected $useModal = false;

    protected $delimiter = ';';
    protected $file;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'delete',
                            'delete-multiple',
                            'upload',
                            'import',
                            'attributes',
                            'status',
                            'background',
                            'fetch-google'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Import models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImportSearch();
        $dataProvider = $searchModel->search($_GET);

        Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'useModal' => $this->useModal,
        ]);
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id, $viewFull = false)
    {
        $resolved = \Yii::$app->request->resolve();
        $resolved[1]['_pjax'] = null;
        $url = Url::to(array_merge(['/' . $resolved[0]], $resolved[1]));
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember($url);
        Tabs::rememberActiveState();
        if ($this->useModal && !$viewFull) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                'useModal' => $this->useModal,
            ]);
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            \Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous());
        }

        // TODO: improve detection
        $isPivot = strstr('$id', ',');
        if ($isPivot == true) {
            return $this->redirect(Url::previous());
        } elseif (isset(\Yii::$app->session['__crudReturnUrl']) && \Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null);
            $url = \Yii::$app->session['__crudReturnUrl'];
            \Yii::$app->session['__crudReturnUrl'] = null;

            return $this->redirect($url);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDeleteMultiple()
    {
        $pk = \Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        foreach ($pk as $id) {
            $this->findModel($id)->delete();
        }
    }

    /**
     * @return string
     */
    public function actionUpload()
    {
        $uploadedFile = UploadedFile::getInstanceByName('input-file');
        if ($uploadedFile) {
            $this->file = Import::getFile($uploadedFile->name);

            $uploadedFile->saveAs($this->file, true);
            $this->delimiter = \Yii::$app->request->post('delimiter');
            $import = new Import();
            $import->userId = \Yii::$app->user->id;
            $import->setPostData(['delimiter' => $this->delimiter, 'file_name' => $uploadedFile->name]);
            return json_encode(['data' => $import->getFirstRow(), 'type' => $import->getFileType()]);
        }
        return json_encode(['data' => false, 'response' => \Yii::t('app', 'Error during file uploading')]);
    }

    public function actionFetchGoogle()
    {
        if (\Yii::$app->request->isAjax && \Yii::$app->request->post('type') == 'google') {
            $import = new Import();
            $import->userId = \Yii::$app->user->id;
            $import->setPostData(['file_name' => \Yii::$app->request->post('file_name'), 'type' => 'google']);
            return json_encode(['data' => $import->getFirstRow(), 'type' => $import->getFileType()]);
        }
        throw new Exception(\Yii::t('app', 'Only Ajax request is allowed'));
    }

    /**
     * @param $model
     * @return string
     */
    public function actionAttributes($model)
    {
        return json_encode(Import::getModelAttributes($model));
    }

    /**
     * @return string
     * @throws HttpException
     */
    public function actionImport()
    {
//        ini_set('max_execution_time', 0);
        // Not allow regular calls, just Ajax
        if (!\Yii::$app->request->isAjax) {
            throw new HttpException(\Yii::t('app', 'Not allowed'));
        }
        $import = new Import();
        if ($import->getIsRunning()) {
            $result = [
                'status' => 'Error',
                'success' => 'danger',
                'result' => \Yii::t('app', 'Importer is already running. Please wait until finished'),
            ];
        } else {
            $data = json_encode(\Yii::$app->request->post());
            $command = 'php ../yii import --userId=' . \Yii::$app->user->identity->id . ' --data=\'' . $data . '\'' . " > /dev/null 2>/dev/null & echo $!";
//            $command = 'php ../yii import --userId=' . \Yii::$app->user->identity->id . ' --data=\'' . $data . '\'' . "";
            $result = shell_exec($command);
            return $result;
        }
        return json_encode($result);
    }

    /**
     * @return string
     * @throws HttpException
     */
    public function actionStatus()
    {
        // Not allow regular calls, just Ajax
        if (!\Yii::$app->request->isAjax) {
            throw new HttpException(\Yii::t('app', 'Not allowed'));
        }
        // Delete old processes
        if (\Yii::$app->request->post('pageReloaded') == 'true') {
            $oldProgress = ImportProgress::find()
                ->andWhere([
                    'user_id' => \Yii::$app->user->identity->id,
                    'model' => \Yii::$app->request->post('model'),
                    'status' => [ImportProgress::STATUS_ERROR, ImportProgress::STATUS_FINISHED]
                ])
                ->all();
            foreach ($oldProgress as $old) {
                $old->delete();
            }
        }
        $import = new Import();
        $import->userId = \Yii::$app->user->identity->id;
        $import->model = \Yii::$app->request->post('model');
        // Checking if there is process running
        if ($progress = ImportProgress::find()
            ->andWhere(['user_id' => \Yii::$app->user->identity->id, 'model' => \Yii::$app->request->post('model')])
            ->orderBy(['created_at' => SORT_DESC])
            ->one()) {
            $percentage = $progress->total_rows == 0 ? 0 : floor(
                (
                    $progress->current_row /
                    $progress->total_rows
                )
                * 100
            );
            $result = [
                'running' => true,
                'progress' => $percentage,
                'status' => $progress->status,
                'result' => nl2br($progress->message),
            ];
            if ($progress->status == ImportProgress::STATUS_FINISHED) {
                \Yii::$app->session->setFlash(ImportResult::SUCCESS, $progress->message);
            }
        } else {
            $result['running'] = false;
        }
        return json_encode($result);
    }

    public function actionBackground()
    {
        // Not allow regular calls, just Ajax
        if (!\Yii::$app->request->isAjax) {
            throw new HttpException(\Yii::t('app', 'Not allowed'));
        }
        $job = new QueueJob();
        $job->command = 'taktwerk\yiiboilerplate\modules\import\commands\ImportCommand';
        $job->title = 'Import ' . \Yii::$app->request->post('model');
        $job->parameters = json_encode(array_merge(['user_id' => \Yii::$app->user->id], \Yii::$app->request->post()));
        $job->status = QueueJob::STATUS_QUEUED;
        $job->save();
        return json_encode($job->errors);
    }

    /**
     * Finds the Import model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Import the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Import::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
