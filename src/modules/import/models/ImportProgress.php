<?php

namespace taktwerk\yiiboilerplate\modules\import\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\import\models\base\ImportProgress as BaseImportProgress;

/**
 * This is the model class for table "import_progress".
 */
class ImportProgress extends BaseImportProgress
{

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
