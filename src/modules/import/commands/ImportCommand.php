<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/1/2017
 * Time: 2:54 PM
 */

namespace taktwerk\yiiboilerplate\modules\import\commands;

use taktwerk\yiiboilerplate\modules\import\models\Import;
use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;

class ImportCommand implements BackgroundCommandInterface
{

    public function run(QueueJob $queue)
    {
        $import = new Import();
        $params = json_decode($queue->parameters, true);
        $import->userId = $params['user_id'];
        $import->setPostData($params);
        try {
            $result = $import->process($queue);
            if ($result['success'] == 'success') {
                $queue->status = QueueJob::STATUS_FINISHED;
            } else {
                $queue->status = QueueJob::STATUS_FAILED;
                $queue->error = $result['result'];
            }
            $queue->save();
        } catch (\Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            Notification::send($params['user_id'], 'Import process failed');
        }
        // TODO: Implement run() method.
    }
}