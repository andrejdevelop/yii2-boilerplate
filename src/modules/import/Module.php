<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/6/2017
 * Time: 8:37 AM
 */

namespace taktwerk\yiiboilerplate\modules\import;

class Module extends \yii\base\Module
{

    /**
     * @var string
     */
    public $defaultRoute = 'index';

    /**
     * @var bool To save import logs to database
     */
    public $keepLogs = true;

}