<?php

namespace taktwerk\yiiboilerplate\modules\backend\controllers;

use taktwerk\yiiboilerplate\helpers\DebugHelper;
use taktwerk\yiiboilerplate\models\ArHistory;
use taktwerk\yiiboilerplate\templates\menu\Generator;
use taktwerk\yiiboilerplate\templates\migration\TwGenerator;
use taktwerk\yiiboilerplate\TwMigration;
use Yii;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\filters\AccessControl;
use Exception;
use yii\gii\CodeFile;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * Default backend controller.
 *
 * Usually renders a customized dashboard for logged in users
 */
class SchemaCheckerController extends DefaultController
{
    /**
     * @var array of tables needed for the migration
     */
    protected $tablesMigration = [];

    /**
     * @var array of tables needed for the Crud
     */
    protected $tablesCrud = [];

    /**
     * @var array of errors
     */
    protected $errors = [];

    /**
     * @var array Default tables of Yii/boilerplate that should be ignored.
     */
    protected $defaultTables = [
        'user'
    ];

    protected $importedTables = [];

    /**
     * Actions defined in classes, eg. error page.
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Application dashboard.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new DynamicModel([
            'sql_file',
        ]);
        $model->addRule(['sql_file'], 'required', ['on' => 'validate']);

        if (Yii::$app->request->isPost) {
            if (isset($_POST['submit-default'])) {
                $model->sql_file = UploadedFile::getInstance($model, 'sql_file');
                $model->sql_file->saveAs(Yii::getAlias('@runtime/temp.sql'), true);
                $schema = file_get_contents(Yii::getAlias('@runtime/temp.sql'));
            } else {
                $schema = Yii::$app->request->post('schema');
            }
            $this->validateSchema($schema);
            $errors = $this->errors;
            if (empty($errors)) {
                if (isset($_POST['tables'])) {
                    foreach (Yii::$app->request->post('tables') as $table => $value) {
                        $this->importedTables[] = $table;
                    }
                    $this->importedTables = array_diff($this->importedTables, $this->defaultTables);
                }
                if (isset($_POST['submit-generate'])) {
                    // First we import whole schema into database
                    $this->import(Yii::$app->request->post('schema'));
                    // Then we generate migrations
                    $this->generateMigrations();
                    // After we droping those tables we imported, so migrations will generate them
                    $this->drop();
                    // Finally we applying migrations and generating models and cruds files
                    $output = $this->output();
                    // Save output in logs
                    Yii::trace($output);
                } elseif (isset($_POST['submit-crud'])) {
                    // Generate only CRUDs for selected tables
                    $output = $this->generateCrud();
                } elseif (isset($_POST['submit-model'])) {
                    // Generate only models for selected tables
                    $output = $this->generateModel();
                } elseif (isset($_POST['submit-model-crud'])) {
                    // Generate only models and CRUDs for selected tables
                    $output = $this->generateModel();
                    $output .= $this->generateCrud();
                }
                $tables = $this->tablesMigration;
                $tableMigrationList = implode(',', $this->tablesMigration);
                $tableCrudList = implode(',', $this->tablesCrud);
            }
            return $this->render(
                'index',
                compact('model', 'errors', 'tableMigrationList', 'tableCrudList', 'schema', 'tables', 'output')
            );
        }

        return $this->render('index', compact('model'));
    }

    /**
     * Drop selected tables
     * @throws \yii\db\Exception
     */
    private function drop()
    {
        $db = Yii::$app->db;
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 0')->execute();
        foreach ($this->importedTables as $table) {
            $db->createCommand('DROP TABLE IF EXISTS `' . $table . '`')->execute();
            // Delete history
            $histories = ArHistory::find()->andWhere(['table_name' => $table])->all();
            foreach ($histories as $history) {
                $history->delete();
            }
        }
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 1')->execute();
    }

    /**
     * Run applying migrations, model and cruds files generations
     * @return string
     */
    private function output()
    {
        $output = '';
        $migrate = 'php ../yii migrate --interactive=0';
        $output .= shell_exec($migrate);
        $output .= $this->generateModel();
        $output .= $this->generateCrud();
        $output .= $this->generateMenu();
        return $output;
    }

    private function generateModel()
    {
        $output = '';
        // Check first if have all tables in database and all necessary models
        $db = Yii::$app->db;
        foreach ($this->importedTables as $key => $table) {
            if ($db->getTableSchema($table) == null) {
                // There is no table in database, unset it from CRUD generation
                unset($this->importedTables[$key]);
            }
        }
        $models = 'php ../yii batch/models --tables=' . implode(',', $this->importedTables) . ' --interactive=0';
        $output .= "Running php ../yii batch/models --tables=" . implode(',', $this->importedTables) . " --interactive=0\n";
        $output .= shell_exec($models);
        return $output;

    }
    /**
     * @return string
     */
    private function generateCrud()
    {
        $output = '';
        // Check first if have all tables in database and all necessary models
        $db = Yii::$app->db;
        foreach ($this->importedTables as $key => $table) {
            if ($db->getTableSchema($table) == null) {
                // There is no table in database, unset it from CRUD generation
                unset($this->importedTables[$key]);
            } else {
                // If table exist, make sure that we have model created
                $modelName = Inflector::camelize($table);
                if (!class_exists("\\app\\models\\$modelName")) {
                    // If model not exist, create it first before create CRUD for it
                    $output .= 'No model exist for table ' . $table . ". Creating...\n";
                    shell_exec('php ../yii batch/models --tables=' . $table . ' --interactive=0');
                }
            }
        }
        $cruds = 'php ../yii batch/cruds --tables=' . implode(',', $this->importedTables) . ' --interactive=0';
        $output .= "Running php ../yii batch/cruds --tables=" . implode(',', $this->importedTables) . " --interactive=0\n";
        $output .= shell_exec($cruds);
        return $output;
    }
    /**
     * Generate migration files
     */
    private function generateMigrations()
    {
        $this->removeMigrationFiles();
        $migrationTables = implode(', ', $this->importedTables);
        $migration = new TwGenerator();
        $migration->tableName = $migrationTables;
        $migration->templates = [
            'taktwerk' => Yii::getAlias('@taktwerk-boilerplate/templates/migration/default')
        ];
        $migration->template = 'taktwerk';
        $migration->validateTableName();
        $files = $migration->generate();
        foreach ($files as $file) {
            /**
             * @var $file CodeFile
             */
            $file->save();
        }
    }

    private function removeMigrationFiles()
    {
        $path = Yii::getAlias('@app/migrations');
        $files = FileHelper::findFiles($path, ['with' => '*.php', 'recursive' => false]);
        foreach ($files as $file) {
            @unlink($file);
        }
    }

    /**
     * @return string
     * Generate menu endpoints for generated controllers
     */
    private function generateMenu()
    {
        $controllers = [];
        foreach ($this->importedTables as $importedTable) {
            $controllers[] = Generator::table2Controller($importedTable);
        }
        $menuGenerator = new Generator();
        $menuGenerator->controllers = $controllers;
        $menuGenerator->templates = [
            'taktwerk' => Yii::getAlias('@taktwerk-boilerplate/templates/menu/default')
        ];
        $menuGenerator->template = 'taktwerk';
        $files = $menuGenerator->generate();
        foreach ($files as $file) {
            /**
             * @var $file CodeFile
             */
            $file->save();
        }
        return 'Menu endpoints generated for controllers: ' . implode(', ', $controllers) . "\n";
    }

    /**
     * Import schema into database
     * @param $schema
     * @throws \yii\db\Exception
     */
    private function import($schema)
    {
        $this->drop();
        $db = Yii::$app->db;
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 0')->execute();
        $db->createCommand($schema)->execute();
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 1')->execute();
    }

    /**
     * Analyse and validate a mysql schema.
     * @param $schema
     */
    private function validateSchema($schema)
    {
        $tables = $this->parseTables($schema);

        // Loop all tables and validate them.
        foreach ($tables as $name => $table) {
            // Ignore default Yii2/boilerplate tables
            $this->tablesCrud[] = $name;
            if (!in_array($name, $this->defaultTables)) {
                $this->tablesMigration[] = $name;
                $this->validateTable($name, $table);
            }
        }

        // No tables? That's fishy
        if (empty($this->tablesMigration)) {
            if (empty($schema)) {
                $this->logError("Schema", "The schema was empty. Try submitting only parts of it instead.");
            } else {
                $this->logError("Schema", "Couldn't find any tables in the schema.");
            }
        }
    }

    /**
     * @param $schema
     * @return array
     */
    private function parseTables($schema)
    {
        $tables = [];
        $lines = preg_split('/$\R?^/m', $schema);
        $table = null;
        $tableName = null;
        $lineCount = 0;

        foreach ($lines as $line) {
            $trimmed = trim($line);

            // Look for a CREATE beginning
            if (substr($line, 0, 6) == 'CREATE') {
                $tableName = str_replace('CREATE TABLE IF NOT EXISTS `', '', str_replace('` (', '', $trimmed));
                $table = [
                    'column' => [],
                    'primary' => [],
                    'unique' => [],
                    'index' => [],
                    'foreign' => [],
                ];
            } // We are in a table
            elseif (!empty($tableName)) {
                // Finished parsing a table
                if (substr($trimmed, -1) == ';') {
                    $tables[$tableName] = $table;
                    $tableName = $table = null;
                } else {
                    // Start with some tabs? Might be rows
                    if (substr($line, 0, 2) == '  ') {
                        // Field
                        if (substr($trimmed, 0, 1) == '`') {
                            $col = $this->parseColumn($line);
                            $table['column'][$col['name']] = $col['data'];
                        } elseif (substr($trimmed, 0, 7) == 'PRIMARY') {
                            $table['primary'][] = $this->parsePrimary($trimmed);
                        } elseif (substr($trimmed, 0, 6) == 'UNIQUE') {
                            $table['unique'][] = $trimmed;
                        } elseif (substr($trimmed, 0, 5) == 'INDEX') {
                            $index = $this->parseIndex($trimmed);
                            $table['index'][$index['name']] = $index['fields'];
                        } elseif (substr($trimmed, 0, 10) == 'CONSTRAINT') {
                            $foreign = $this->parseForeign($trimmed . trim($lines[$lineCount + 1]) . trim($lines[$lineCount + 2]));
                            $table['foreign'][$foreign['key']] = $foreign['foreign'];
                        }
                    }
                }
            }
            // Ignore the rest!
            $lineCount++;
        }

        return $tables;
    }

    /**
     * @param $line
     * @return array
     */
    private function parseColumn($line)
    {
        $line = trim($line);
        if (substr($line, -1) != ',') {
            throw new Exception('Column declaration not ending with \',\'. Do you have multiline comments? ' . $line);
        }
        preg_match("/`(?<name>.*?)`(?<data>.*?),/", $line, $data);
        return ['name' => trim($data['name']), 'data' => trim($data['data'])];
    }

    /**
     * @param $line
     * @return array
     */
    private function parsePrimary($line)
    {
        $line = trim($line);
        preg_match("/PRIMARY KEY \(`(?<key>.*?)`\)/", $line, $data);
        return trim($data['key']);
    }

    /**
     * @param $line
     * @return array
     */
    private function parseForeign($line)
    {
        $line = trim($line);
        preg_match("/CONSTRAINT `(?<key>.*?)`\s*FOREIGN KEY \(`(?<name>.*?)`\)\s*REFERENCES `(?<foreign_table>.*?)` \(`(?<foreign_key>.*?)`\)/", $line, $data);
        return ['key' => trim($data['name']), 'foreign' => ['table' => trim($data['foreign_table']), 'key' => trim($data['foreign_key'])]];
    }

    /**
     * @param $line
     * @return array
     */
    private function parseIndex($line)
    {
        $line = trim($line);
        preg_match("/\s*INDEX `(?<name>.*?)`\s* \((?<fields>.*?)\)/", $line, $data);
        $sqlFields = explode(',', $data['fields']);
        $fields = [];
        foreach ($sqlFields as $field) {
            preg_match("/`(?<key>.*?) (?<order>.*)/", $field, $fieldData);
            $fields[$fieldData['key']] = trim($fieldData['order']);
        }
        return ['name' => trim($data['name']), 'fields' => $fields];
    }

    /**
     * Add errors
     * @param $name
     * @param $message
     */
    private function logError($name, $message)
    {
        $this->errors[$name][] = $message;
    }

    /**
     * @param $tableName
     * @param $tableData
     */
    private function validateTable($name, $data)
    {

        if (preg_match('/[a-z_]*/', $name) != 1) {
            $this->logError($name, "The table '$name' in your Schema doesn't follow the Tw MySQL conventions of only lowercase letters a-z or '_'.");
        }

        // Check each column.
        foreach ($data['column'] as $key => $value) {
            $lowerValue = strtolower($value);
            if (preg_match('/^[a-z_]*$/', $key) != 1) {
                $this->logError($name, "The column name '$key' in your Schema does not follow the tw mysql conventions. Please make sure to create columns with only lowercase letters a-z or '_'.");
            }

            // Now the fun part, look at the names.
            if (substr($key, -3) == '_id') {
                // Check that it is an integer and has a foreign key
                if (strpos($lowerValue, 'int') === false) {
                    $this->logError($name, "The column name '$key' isn't an <strong>Integer</strong> (<i>$value</i>).");
                }
                if (!isset($data['foreign'][$key])) {
                    $this->logError($name, "The column name '$key' is an '_id' but doesn't have a foreign key defined.");
                }
            } // Booleans
            elseif (substr($key, 0, 3) == 'is_' || substr($key, 0, 4) == 'has_') {
                if (strpos($lowerValue, 'boolean') === false && strpos($lowerValue, 'tinyint(1)') === false) {
                    $this->logError($name, "The column name '$key' isn't a <strong>Boolean</strong> or a <strong>TinyInt(1)</strong> (<i>$value</i>).");
                }
            } // Dates
            elseif (substr($key, -3) == '_at') {
                if (strpos($lowerValue, 'datetime') === false) {
                    $this->logError($name, "The column name '$key' isn't a <strong>DateTime</strong> (<i>$value</i>)).");
                }
            } elseif (substr($key, -5) == '_date') {
                if (strpos($lowerValue, 'date ') === false) {
                    $this->logError($name, "The column name '$key'  isn't a <strong>Date</strong> (<i>$value</i>)).");
                }
            }
        }

        // Check there is a primary key.
        if (empty($data['primary'])) {
            $this->logError($name, "The table '$name' doesn't seem to have a primary key.");
        }
    }
}
