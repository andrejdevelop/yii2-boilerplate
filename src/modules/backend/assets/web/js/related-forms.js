/*
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by Nikola on 3/21/2017.
 */

(function ($) {
    $.fn.formRelated = function (options) {
        var settings = $.extend({
            selector: null,
            url: null,
            update: false,
            primaryKey: null,
            depend: null,
            dependOn: null,
            relation: null,
            relationId: null,
            mainForm: null
        }, options);
        
        this.selector = settings.selector;
        var temp = this;
        var self = this,
            confirm = function () {
                if (window[self.selector + 'changed'] == true) {
                    if (window.confirm('You are about to lose your changes . Are you sure to close this?')) {
                        return true;
                    } else {
                        return false;
                    }
                }
                return true;
            },
            removeTab = function (checkTouch) {
                var tab = $(".nav-tabs"),
                    lastLi = $(tab).children('li').last(),
                    nextIndex = $(tab).children().length,
                    id = $(tab).attr('id'),
                    tabId = id + '-tab' + (nextIndex - 1),
                    prevTab = id + '-tab' + (nextIndex - 2),
                    mainButtons = $('#main-submit-buttons'),
                    checkTouch = typeof checkTouch !== 'undefined' ? checkTouch : true;

                if (checkTouch && !confirm()) {
                    return false;
                }
                // Remove last tab data
                $('#' + tabId).remove();
                // Remove last tab
                $(lastLi).remove();
                var currentCloseButton = $(tab).children('li').last().find('.closeTab');
                if (currentCloseButton.length > 0) {
                    $(currentCloseButton).attr('disabled', false);
                }
                // Remove overlay from previous tab
                $('#overlay-' + prevTab).remove();
                // If we are closing first opened tab and going to main tab, show main save/delete buttons
                if (nextIndex == 2) {
                    $(mainButtons).show();
                    mainForm = true;
                }
                // Set previous tab as active
                $('a[href="#' + prevTab + '"]').tab('show');
                window[self.selector + 'changed'] = false;
            },
            loadForm = function (update) {
                return new Promise(function (resolve, reject) {
                    var url = settings.url;
                    $('body').addClass('ajax-loading');
                    if (update) {
                        url = settings.url + '&' + settings.primaryKey + '=' + $(settings.selector).val();
                    } else if (settings.depend) {
                        url = settings.url + '&depend=' + settings.depend + '&relation=' + settings.relation + '&relationId=' + settings.relationId + '&relationIdValue=' + $('#' + settings.dependOn).val();
                    }
                    $.get(url)
                        .done(function (data) {
                            $('body').removeClass('ajax-loading');
                            resolve(data);
                        });
                })
            },
            submitForm = function (form) {
                return new Promise(function (resolve, reject) {
                    $('body').addClass('ajax-loading');
                    $.post(
                        form.attr("action"),
                        form.serialize()
                        )
                        .done(function (data) {
                            if ($(self.selector).find("option[value=" + data.id + "]").length) {
                                // Update existing entry
                                var optionSelected = $(self.selector).find("option:selected"),
                                    data_krajee_select2 = $(self.selector).attr('data-krajee-select2');
                                optionSelected.text(data.label).trigger("change");
                                // Destroy select2 widget and recreate using same config
                                $(self.selector).select2("destroy").select2(window[data_krajee_select2]);
                            } else {
                                // Create the DOM option that is pre-selected by default
                                var newState = new Option(data.label, data.id, true, true);
                                // Append it to the select
                                $(self.selector).val(data.id);
                                $(self.selector).append(newState).trigger('change');
                                
                            }
                            $('body').removeClass('ajax-loading');
                            resolve('success');
                        })
                        .fail(function () {
                            $('body').removeClass('ajax-loading');
                            console.log("server error");
                        });
                })
            };
        // Attach event click to current tab content buttons
        $($(this).attr('href')).find('.btn-tab-form').on('click', function () {
            var tabs = $(".nav-tabs"),
                lastLi = $(tabs).children('li').last(),
                nextIndex = $(tabs).children().length,
                id = $(tabs).attr('id'),
                tabId = id + '-tab' + nextIndex,
                prevTab = id + '-tab' + (nextIndex - 1),
                mainButtons = $('#main-submit-buttons'),
                update = $(this).data('update') != undefined;

            // Add new tab
            $(lastLi).after('<li><a href="#' + tabId + '" data-toggle="tab"></a></li>');
            // Set update flag from button
            settings.update = update;
            settings.selector = '#' + $(this).attr('selector');
            settings.url = $(this).attr('url');
            settings.mainForm = false;
            // Attach settings to new tab
            $('a[href="#' + tabId + '"]').formRelated(settings);
            $('.tab-content').append('<div class="tab-pane" id="' + tabId + '" style="position: relative;"></div>');
            // Show new tab and set to active
            $('a[href="#' + tabId + '"]').tab('show');
            //Add overlay to previous tab
            if (nextIndex == 1) {
                $('#' + prevTab).append('<div class="overlay overlay-first" id="overlay-' + prevTab + '"></div>');
            } else {
                $('#' + prevTab).append('<div class="overlay overlay-first overlay-next" id="overlay-' + prevTab + '"></div>');
                //@TODO disable close button of previous tab
                var prevCloseButton = $(lastLi).find('.closeTab');
                if (prevCloseButton.length > 0) {
                    prevCloseButton.attr('disabled', true);
                }
            }
            $(mainButtons).hide();
        });
        // Attach click event to close form button
        $($(this).attr('href')).find('a[name="close"]').on('click', function (e) {
            e.preventDefault();
            removeTab();
        });
        $(this).find('.closeTab').on('click', function (e) {
            e.preventDefault();
            removeTab();
        });
        $($(this).attr('href')).find('form').on('submit', function (e) {
            e.preventDefault();
        });
        $($(this).attr('href')).find('form').on('beforeSubmit', function (e) {
            submitForm($(this)).then(function () {
                removeTab(false);
            });
            return false;
        });
        $($(this).attr('href')).find('input, select, textarea').on('change', function () {
            window[self.selector + 'changed'] = true;
        })
        // If we are clicking on save buttons, remove that something is changed, so to avoid confirm dialog
        $($('body').find('button[name^="submit"]')).on('click', function(){
            window[self.selector + 'changed'] = false;
        });
        $(window).bind("beforeunload", function () {
            if (window[self.selector + 'changed'] == true) {
                return confirm();
            }
        });
        return this
            .on('shown.bs.tab', function (e) {
                var self = $(this);
                tabContentDiv = $(self).attr('href');
                if ($(tabContentDiv).html() == '') {
                    loadForm(settings.update).then(function (data) {
                        $(tabContentDiv).html(data);
                        // Add title to tab
                        $(self).html(($(tabContentDiv).find('form').attr('name')) + '&nbsp;<button class="close closeTab" name="close" type="button" >×</button>');
                        mainForm = false;
                        $('a[href="' + tabContentDiv + '"]').formRelated(settings);
                    });
                }
            })
            .on('submit', 'form', function (e) {
                e.preventDefault();
            })
            .on('beforeSubmit', 'form', function (e) {
                submitForm($(this)).then(function (data) {
                    self.modal('hide');
                });
                return false;
            })
            .on('hidden.bs.modal', function () {
                // When modal is closed, remove all content from it
                $(this).find('.modal-body').html('');
            })
            .on('show.bs.modal', function (e) {
                // When modal is show load related form in it
                if (e.relatedTarget) {
                    var button = $(e.relatedTarget),
                        update = $(button).data('update') != undefined;
                    loadForm(update).then(function (data) {
                        $(self).find('.modal-body').html(data);
                    });
                }
            })
            ;
    };
}(jQuery));
