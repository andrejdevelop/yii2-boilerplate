<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\backend\assets;

use yii\web\AssetBundle;
use yii\helpers\FileHelper;

class SystemInformationAsset extends AssetBundle
{
    public $js = [
        'system-information.js'
    ];

    public $depends = [
        'taktwerk\yiiboilerplate\modules\backend\assets\Flot',
    ];

    public function init()
    {
        $this->sourcePath = '@vendor/taktwerk/yii-boilerplate/src/assets/web/js';
        parent::init();
        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $path = \Yii::getAlias($this->sourcePath);
            $files = FileHelper::findFiles($path);
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch($path, max($mtimes));
        }
    }
}
