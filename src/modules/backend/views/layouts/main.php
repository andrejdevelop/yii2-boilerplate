<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = $this->title;
\taktwerk\yiiboilerplate\assets\TwAsset::register($this);
if (class_exists('\app\modules\backend\assets\AdminAsset')) {
    \app\modules\backend\assets\AdminAsset::register($this);
}

$this->render('@taktwerk-boilerplate/views/blocks/raven');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Theme style -->
    <?php $this->head() ?>
<?php if (Yii::$app->params['disable_external'] === false): ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<?php endif; ?>
</head>

<body
    class="hold-transition <?= isset(Yii::$app->params['adminSkin']) ? Yii::$app->params['adminSkin'] : 'skin-black' ?> sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->

        <a href="<?= Yii::$app->homeUrl ?>" class="logo logo-image">
        <span class="logo-lg">
                <?= getenv('APP_TITLE') ?>
        </span>
        <span class="logo-mini">
        </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only"><?=Yii::t('app', 'Toggle navigation')?></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <?= \taktwerk\yiiboilerplate\modules\notification\widgets\NotificationWidget::widget() ?>
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?= strtoupper(Yii::$app->language) ?>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header"><?=Yii::t('app', 'Languages')?></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <?php foreach (Yii::$app->urlManager->getLanguagesForDropdown() as $language): ?>
                                            <li>
                                                <?= Html::a(
                                                    strtoupper($language['label']),
                                                    $language['url']
                                                ) ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?= Yii::$app->user->identity->toString ?>
                                    <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <?php echo \taktwerk\yiiboilerplate\widget\Gravatar::widget(
                                        [
                                            'email' => !isset(Yii::$app->user->identity->profile->gravatar_email) || Yii::$app->user->identity->profile->gravatar_email === null
                                                ? Yii::$app->user->identity->email
                                                : Yii::$app->user->identity->profile->gravatar_email,
                                            'options' => [
                                                'alt' => Yii::$app->user->identity->toString,
                                                'style' => 'margin: 0 auto;'
                                            ],
                                            'size' => 128,
                                            'width' => 90,
                                            'height' => 90,
                                            'image' => Yii::$app->user->identity->getProfilePicture(['width' => 90, 'height' => 90]),
                                        ]
                                    ); ?>
                                    <p>
                                        <?= Yii::$app->user->identity->toString ?>
                                        <small><?= Yii::$app->user->identity->email ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                        <div class="btn-group">
                                            <div class="pull-left">
                                                <a href="<?= \yii\helpers\Url::to(['/user/settings/profile']) ?>"
                                                   class="btn btn-default btn-flat"><?=Yii::t('app', 'Profile')?></a>
                                            </div>
                                            <?php if (Yii::$app->user->identity->isAdmin): ?>
                                                <a href="#"
                                                   class="btn btn-default btn-flat" data-toggle="modal"
                                                   data-target="#modalApiKey"><?=Yii::t('app', 'API Key')?></a>
                                            <?php endif; ?>
                                        </div>
                                    <div class="pull-right">
                                        <a href="<?= \yii\helpers\Url::to(['/user/security/logout']) ?>"
                                           class="btn btn-default btn-flat" data-method="post"><?=Yii::t('app', 'Sign out')?></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <?= $this->render('_sidebar') ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <small><?= $this->title ?></small>
            </h1>
            <?=
            \yii\widgets\Breadcrumbs::widget([
                'homeLink' => ['label' => Yii::t('app', 'Dashboard'), 'url' => ['/backend']],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->

        <section class="content">
            <?= $content ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>
            <?= \taktwerk\yiiboilerplate\components\Helper::version() ?></strong>
        &copy; <strong><a href="http://taktwerk.ch">taktwerk.ch</a></strong> | 2012-<?= date("Y", time()) ?>
    </footer>
</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
<?php
if (Yii::$app->user->identity->isAdmin) {
    \yii\bootstrap\Modal::begin([
        'header' => '<h3>' . Yii::t('app', 'API Key') . '</h3>',
        'toggleButton' => false,
        'size' => \yii\bootstrap\Modal::SIZE_SMALL,
        'id' => 'modalApiKey',
    ]);
    echo Yii::$app->user->identity->getAuthKey();
    \yii\bootstrap\Modal::end();
}
?>
<?php
\yii\bootstrap\Modal::begin([
    'toggleButton' => false,
    'header' => Yii::t('app', 'Notification'),
    'size' => \yii\bootstrap\Modal::SIZE_SMALL,
    'id' => 'notificationModal',
]);
\yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php
$headJsSideBar = <<<JS
    (function () {
        if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            var body = document.getElementsByTagName('body')[0];
            body.className = body.className + ' sidebar-collapse';
        }
    })();
JS;
$this->registerJs($headJsSideBar, \yii\web\View::POS_BEGIN);
$sideBarClickEvent = <<<JS
    // Click handler can be added latter, after jQuery is loaded...
    $('.sidebar-toggle').click(function(event) {
        event.preventDefault();
        if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            sessionStorage.setItem('sidebar-toggle-collapsed', '');
        } else {
            sessionStorage.setItem('sidebar-toggle-collapsed', '1');
        }
    });
JS;
$this->registerJs($sideBarClickEvent, \yii\web\View::POS_END);
$this->endPage();
