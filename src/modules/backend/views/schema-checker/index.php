<?php

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;

?>

<h1><?= Yii::t('app', 'Schema Checker'); ?></h1>
<p><?= Yii::t('app', 'Paste your MySQL create schema statement to check it against the taktwerk conventions.'); ?></p>
<p><?= Yii::t('app', 'Be careful to start your create statements with'); ?><code>CREATE TABLE IF NOT EXIST `name`
        (</code>.</p>

<?php if (isset($errors)): ?>
    <?php if (!empty($errors)): ?>
        <div class="error-summary">
            <?php foreach ($errors as $table => $tableErrors): ?>
                <p><strong><?= $table ?> :</strong><br/>
                    <?= implode('<br>', $tableErrors) ?><br/>
                </p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>

<div class="branch-label-form">
    <?php $form = ActiveForm::begin([
        'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
        'id' => 'SchemaChecker',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
    ]);
    ?>
    <?= $form->field($model, 'sql_file')->widget(\kartik\file\FileInput::className(), [
        'pluginOptions' => [
            'showUpload' => false,
            'showRemove' => false,
            'showPreview' => false,
            'allowedFileExtensions' => ['sql'],
        ],
    ]) ?>

    <?php if (empty($errors) && !empty($tables) && $output == null) : ?>
        <div class="form-group">
            <label
                class="control-label col-sm-3"><?= Yii::t('app', 'Select tables that you want to generate'); ?></label>
            <div class="col-sm-6">
                <?= Html::hiddenInput('schema', $schema) ?>
                <?php foreach ($tables as $table) : ?>
                    <div class="checkbox">
                        <label class="control-label" for="tables[<?= $table ?>]">
                            <?= \yii\helpers\Html::checkbox('tables[' . $table . ']', true, ['id' => 'tables[' . $table . ']']) ?>
                            <?= $table ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php elseif (!empty($errors)) : ?>
        <div class="col-sm-6 col-sm-offset-3">
            <div class="error-summary">
                <?php foreach ($errors as $table => $tableErrors): ?>
                    <p><strong><?= $table ?> :</strong><br/>
                        <?= implode('<br>', $tableErrors) ?><br/>
                    </p>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group field-defaulttext-text required">
        <div class="col-sm-6 col-sm-offset-3">
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' . (($tables == null || $output != null) ? Yii::t('app', 'Validate') : Yii::t('app', 'Full Generate')),
                [
                    'id' => 'save-schema-checker',
                    'class' => 'btn btn-success',
                    'name' => ($tables == null || $output != null) ? 'submit-default' : 'submit-generate',
                    'data-confirm' => ($tables == null || $output != null) ? false : 'This will remove all migrations from migration folder. Are you sure you want to continue?',

                ]
            );
            ?>
            <?php if ($tables != null && $output == null) : ?>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> Generate Models + CRUDs',
                    [
                        'id' => 'save-schema-checker',
                        'class' => 'btn btn-success',
                        'name' => 'submit-model-crud'
                    ]
                );
                ?>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> Generate Models',
                    [
                        'id' => 'save-schema-checker',
                        'class' => 'btn btn-success',
                        'name' => 'submit-model'
                    ]
                );
                ?>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> Generate CRUDs',
                    [
                        'id' => 'save-schema-checker',
                        'class' => 'btn btn-success',
                        'name' => 'submit-crud'
                    ]
                );
                ?>
            <?php endif; ?>
        </div>
        <?php if ($tables != null && $output == null) : ?>
            <br><br><br>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="info-box">
                    <p>
                    <ul>
                        <li><strong>Full generate</strong> will generate migrations for selected tables, models and
                            CRUDs,
                            but also will
                            completely remove all files in migration folder, so intend of use this is only in beginning
                            of
                            project
                        </li>
                        <li><strong>Generate Models + CRUDs</strong> will generate models and CRUDs for selected tables,
                            but
                            only if there are
                            belonging tables in database created
                        </li>
                        <li><strong>Generate Models</strong> will generate models for selected tables, but only if there
                            are
                            belonging tables in
                            database created
                        </li>
                        <li><strong>Generate CRUDs</strong> will generate CRUDs for selected tables, plus missing
                            models,
                            but only if there are
                            belonging tables in database created
                        </li>
                    </ul>
                    </p>
                    <p>
                    <ul>
                        <li>Never add or change anything in base model file, unless you adding new property, but if we
                            follow this rules it could be done with generator. If need something to be changed do in
                            extended model. In template is added code for merging new rules with base model rules, just
                            uncomment to use it
                        </li>
                        <li>Never add or change anything in base controller file, always change or add actions in
                            extended
                            controller file. In template added code for merging access controls for additional actions.
                            Uncomment to use it
                        </li>
                        <li>Never change manually any view in base folder. If you want to customize it, copy it to
                            folder
                            above with same name, and that view will be used instead of base view. Base views are only
                            meant
                            to be generated by CRUD generator
                        </li>
                    </ul>
                    </p>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($output) : ?>
        <div class="col-sm-6 col-sm-offset-3">
            <div class="info-box">
                <div class="info-box-content">
                    <p><?= Yii::t('app', 'Output result'); ?></p>
                    <code><?= nl2br($output) ?></code>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <hr/>
</div>
<?php ActiveForm::end(); ?>

</div>
