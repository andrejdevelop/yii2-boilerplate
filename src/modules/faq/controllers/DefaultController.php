<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\faq\controllers;

/**
 * DefaultController implements the CRUD actions for Faq model.
 */
class DefaultController extends \taktwerk\yiiboilerplate\modules\faq\controllers\base\FaqController
{
    public function init()
    {
        parent::init();
        $this->layout = '@taktwerk-backend-views/layouts/main.php';
    }
}
