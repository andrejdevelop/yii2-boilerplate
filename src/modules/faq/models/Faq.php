<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\faq\models;

use Yii;
use taktwerk\yiiboilerplate\modules\faq\models\base\Faq as BaseFaq;

/**
 * This is the model class for table "faq".
 */
class Faq extends BaseFaq
{
    
}
