<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\models\base;

use app\models\User;
use Yii;
use \app\models\TwActiveRecord;
use yii\db\Query;

/**
 * This is the base-model class for table "flysystem".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property string $fs_component
 * @property string $alias
 * @property string $root_path
 * @property integer $read_only
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
* @property string $toString
* @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\share\models\FlysystemUser[] $flysystemUsers
 */
class Flysystem extends TwActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flysystem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fs_component'], 'required'],
            [['read_only', 'deleted_by'], 'integer'],
            [['deleted_at'], 'safe'],
            [['fs_component', 'alias', 'root_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fs_component' => Yii::t('app', 'Fs Component'),
            'alias' => Yii::t('app', 'Alias'),
            'root_path' => Yii::t('app', 'Root Path'),
            'read_only' => Yii::t('app', 'Read Only'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->alias;     // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString(){
        return $this->toString();
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'flysystemUsers',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlysystemUsers()
    {
        return $this->hasMany(\taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::className(), ['fs_id' => 'id']);
    }

        public static function FlysystemUsersList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
            if (!is_null($q)) {
            $query = new Query();
            $query->select('id, root_path AS text')
                ->from(\taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::tableName())
                ->andWhere([\taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::tableName() . '.deleted_at' => null])
                ->andWhere(['like', 'root_path', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->name];
        }
        return $out;
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return '';
    }
}
