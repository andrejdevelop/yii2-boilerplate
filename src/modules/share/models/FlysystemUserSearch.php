<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\modules\share\models\FlysystemUser;

/**
 * FlysystemUserSearch represents the model behind the search form about `app\modules\share\models\FlysystemUser`.
 */
class FlysystemUserSearch extends FlysystemUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fs_id', 'user_id', 'root_path', 'overwrite_path', 'read_only', 'overwrite_access', 'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted_by', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlysystemUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $operator = $this->getOperator($this->id);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['id' => $this->id]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['id' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'id', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'id', $operator['first']], ['like', 'id', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'id', $operator['first']], ['like', 'id', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'id', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->fs_id);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['fs_id' => $this->fs_id]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['fs_id' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'fs_id', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'fs_id', $operator['first']], ['like', 'fs_id', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'fs_id', $operator['first']], ['like', 'fs_id', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'fs_id', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->user_id);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['user_id' => $this->user_id]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['user_id' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'user_id', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'user_id', $operator['first']], ['like', 'user_id', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'user_id', $operator['first']], ['like', 'user_id', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'user_id', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->overwrite_path);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['overwrite_path' => $this->overwrite_path]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['overwrite_path' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'overwrite_path', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'overwrite_path', $operator['first']], ['like', 'overwrite_path', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'overwrite_path', $operator['first']], ['like', 'overwrite_path', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'overwrite_path', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->read_only);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['read_only' => $this->read_only]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['read_only' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'read_only', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'read_only', $operator['first']], ['like', 'read_only', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'read_only', $operator['first']], ['like', 'read_only', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'read_only', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->overwrite_access);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['overwrite_access' => $this->overwrite_access]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['overwrite_access' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'overwrite_access', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'overwrite_access', $operator['first']], ['like', 'overwrite_access', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'overwrite_access', $operator['first']], ['like', 'overwrite_access', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'overwrite_access', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->created_by);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['created_by' => $this->created_by]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['created_by' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'created_by', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'created_by', $operator['first']], ['like', 'created_by', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'created_by', $operator['first']], ['like', 'created_by', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'created_by', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->updated_by);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['updated_by' => $this->updated_by]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['updated_by' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'updated_by', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'updated_by', $operator['first']], ['like', 'updated_by', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'updated_by', $operator['first']], ['like', 'updated_by', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'updated_by', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->deleted_by);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['deleted_by' => $this->deleted_by]);
        } elseif (($operator['operator'] == 'OR')) {
        	$query->andFilterWhere(['deleted_by' => $operator['operand']]);
        } elseif (($operator['operator'] !== 'between')) {
        	$query->andFilterWhere([$operator['operator'], 'deleted_by', $operator['operand']]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'deleted_by', $operator['first']], ['like', 'deleted_by', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'deleted_by', $operator['first']], ['like', 'deleted_by', $operator['second']]]);
        } else {
        	$query->andFilterWhere([$operator['operator'], 'deleted_by', $operator['start'], $operator['end']]);
        }

        $operator = $this->getOperator($this->root_path);
        if (!is_array($operator)) {
        	$query->andFilterWhere(['like', 'root_path', $this->root_path]);
        } elseif (($operator['operator'] == 'AndLike')) {
        	$query->andFilterWhere(['AND', ['like', 'root_path', $operator['first']], ['like', 'root_path', $operator['second']]]);
        } elseif (($operator['operator'] == 'OrLike')) {
        	$query->andFilterWhere(['OR', ['like', 'root_path', $operator['first']], ['like', 'root_path', $operator['second']]]);
        }

        if (isset($this->created_at) && $this->created_at != '') {
        	$date_explode = explode(" TO ", $this->created_at);
        	$date1 = trim($date_explode[0]);
        	$date2 = trim($date_explode[1]);
        	$query->andFilterWhere(['between', 'created_at', $date1, $date2]);
        }
        if (isset($this->updated_at) && $this->updated_at != '') {
        	$date_explode = explode(" TO ", $this->updated_at);
        	$date1 = trim($date_explode[0]);
        	$date2 = trim($date_explode[1]);
        	$query->andFilterWhere(['between', 'updated_at', $date1, $date2]);
        }
        if (isset($this->deleted_at) && $this->deleted_at != '') {
        	$date_explode = explode(" TO ", $this->deleted_at);
        	$date1 = trim($date_explode[0]);
        	$date2 = trim($date_explode[1]);
        	$query->andFilterWhere(['between', 'deleted_at', $date1, $date2]);
        }
        return $dataProvider;
    }
}
