<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\share\models\base\FlysystemUser as BaseFlysystemUser;

/**
 * This is the model class for table "flysystem_user".
 */
class FlysystemUser extends BaseFlysystemUser
{
    
}
