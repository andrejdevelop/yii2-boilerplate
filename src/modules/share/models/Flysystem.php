<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\share\models\base\Flysystem as BaseFlysystem;

/**
 * This is the model class for table "flysystem".
 */
class Flysystem extends BaseFlysystem
{

    /**
     * List all flysystem components of application, with convention naming fs_*
     * @return array
     */
    public static function listFsComponents()
    {
        $components = [];
        foreach (array_keys(Yii::$app->components) as $component)
        {
            if (strpos($component, "fs") === 0) {
                //exclude fs_cache since it is not flysystem component
                if ($component != 'fs_cache') {
                    $components[]['name'] = $component;
                }
            }
        }
        return $components;
    }
}
