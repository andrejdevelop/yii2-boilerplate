<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\models;

use taktwerk\yiiboilerplate\modules\share\models\base\FlysystemUser;
use Yii;
use \taktwerk\yiiboilerplate\modules\share\models\base\FlysystemRole as BaseFlysystemRole;

/**
 * This is the model class for table "flysystem_role".
 */
class FlysystemRole extends BaseFlysystemRole
{

//    public function afterSave($insert, $changedAttributes)
//    {
//        parent::afterSave($insert, $changedAttributes);
//        $userIds = Yii::$app->authManager->getUserIdsByRole($this->role);
//        $alreadyAssignedUsers = FlysystemUser::find()->andWhere(['user_id' => $userIds, 'fs_id' => $this->fs_id])->all();
//        $assignedUserIds = [];
//        foreach ($alreadyAssignedUsers as $assignedUser) {
//            $assignedUserIds[] = $assignedUser->user_id;
//        }
//        if ($insert) {
//            $toAssignIds = array_diff($userIds, $assignedUserIds);
//        } else {
//            $toAssignIds = array_intersect($userIds, $assignedUserIds);
//        }
//        echo '<pre>';
//        var_dump($toAssignIds);exit();
//        foreach ($toAssignIds as $id) {
//            $user = new FlysystemUser();
//            $user->user_id = $id;
//            $user->fs_id = $this->fs_id;
//            $user->overwrite_access = $this->overwrite_access;
//            $user->overwrite_path = $this->overwrite_path;
//            $user->root_path = $this->root_path;
//            $user->read_only = $this->read_only;
////            $user->save(false);
//        }
//    }

//    public function afterDelete()
//    {
//        /**
//         * @var FlysystemUser[] $users
//         */
//        $usersIds = Yii::$app->authManager->getUserIdsByRole($this->role);
//        $users = FlysystemUser::find()->andWhere(['user_id' => $usersIds, 'fs_id' => $this->fs_id])->all();
//        foreach ($users as $user) {
//            $user->delete();
//        }
//    }
}
