<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\controllers;

/**
 * This is the class for controller "FlysystemUserController".
 */
class FlysystemUserController extends \taktwerk\yiiboilerplate\modules\share\controllers\base\FlysystemUserController
{
    
}
