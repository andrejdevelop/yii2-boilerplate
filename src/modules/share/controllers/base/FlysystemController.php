<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\controllers\base;

use taktwerk\yiiboilerplate\modules\share\models\Flysystem;
use taktwerk\yiiboilerplate\modules\share\models\FlysystemSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use dmstr\bootstrap\Tabs;
use taktwerk\yiiboilerplate\traits\DependingTrait;
/**
 * FlysystemController implements the CRUD actions for Flysystem model.
 */
class FlysystemController extends Controller
{

    /**
     * Depending Trait for Crud generation
     */
    use DependingTrait;

    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple', 'ajax', 'update-multiple', 'entry-details', 'list', 'depend'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * Lists all Flysystem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlysystemSearch;
        $dataProvider = $searchModel->search($_GET);

        Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Flysystem model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $resolved = \Yii::$app->request->resolve();
        $resolved[1]['_pjax'] = null;
        $url = Url::to(array_merge(['/' . $resolved[0]], $resolved[1]));
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember($url);
        Tabs::rememberActiveState();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Flysystem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Flysystem;
        try {
            if ($model->load($_POST) && $model->save()) {
                if (isset($_POST['submit-default']))
                    return $this->redirect(['update', 'id' => $model->id]);
                else if (isset($_POST['submit-new']))
                    return $this->redirect(['create']);
                else
                    return $this->redirect(['index']);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', [
            'model' => $model,
            ]);
    }

    /**
     * Updates an existing Flysystem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load($_POST) && $model->save()) {
            if (isset($_POST['submit-default']))
                return $this->redirect(['update', 'id' => $model->id]);
            else if (isset($_POST['submit-new']))
                return $this->redirect(['create']);
            else
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing Flysystem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            \Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous());
        }

        // TODO: improve detection
        $isPivot = strstr('$id', ',');
        if ($isPivot == true) {
            return $this->redirect(Url::previous());
        } elseif (isset(\Yii::$app->session['__crudReturnUrl']) && \Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null);
            $url = \Yii::$app->session['__crudReturnUrl'];
            \Yii::$app->session['__crudReturnUrl'] = null;

            return $this->redirect($url);
        } else {
            return $this->redirect(['index']);
        }
    }


    public function actionDeleteMultiple()
    {
        $pk = \Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!$pk) {
            return;
        }
        foreach ($pk as $id) {
            $this->findModel($id)->delete();
        }
        //return Flysystem::deleteAll(['id' => $pk]);
    }

    /**
     * Finds the Flysystem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Flysystem the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Flysystem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }

    /**
     * Call actions
     */
    public function actions()
    {
        return ['ajax' =>
            [
                'class' => '\taktwerk\yiiboilerplate\action\AjaxRelatedAction',
                'viewFile' => '_form',
                'model' => isset($_GET['id']) ? $this->findModel($_GET['id']) : new Flysystem(),
                'depend' => isset($_GET['depend']) ? true : false,
                'dependOn' => isset($_GET['dependOn']) ? true : false,
                'relation' => isset($_GET['relation']) ? $_GET['relation'] : '',
                'relationId' => isset($_GET['relationId']) ? $_GET['relationId'] : '',
                'relationIdValue' => isset($_GET['relationIdValue']) ? $_GET['relationIdValue'] : '',
            ]
        ];
    }


    /**
     * Update multiple models at once
     */
    public function actionUpdateMultiple()
    {
        if (($_POST['no-post'])) {
            if (!isset($_POST['id']) || empty($_POST['id'])) {
                return $this->redirect('index');
            }
            $model = new Flysystem();
            foreach ($_POST as $element => $value) {
                $show[$element] = $value;
            }
            return $this->render('update-multiple', [
                'model' => $model,
                'show' => $show,
                'pk' => $_POST['id'],
            ]);
        } else {
            if (!isset($_POST['pk']) || isset($_POST['close'])) {
                return $this->redirect('index');
            }
            foreach ($_POST['pk'] as $id) {
                $model = $this->findModel($id);
                $model->load($_POST);
                $model->save(false);
            }
            return $this->redirect('index');
        }
        return $this->redirect('index');
    }

    /**
     * Get details of one entry
     */
    public function actionEntryDetails($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $output = [
                'success' => true,
                'data' => $model->entryDetails,
            ];
        } else {
            $output = [
                'success' => false,
                'message' => 'Model does not exist',
            ];
        }
        echo json_encode($output);
    }

    /**
     * Call a function dynamically
     */
    public function actionList($q = null, $id = null, $m)
	{
		$function = $m . 'List';
		return Flysystem::$function($q, $id);
	}
}
