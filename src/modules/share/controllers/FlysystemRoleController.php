<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\controllers;

/**
 * This is the class for controller "FlysystemRoleController".
 */
class FlysystemRoleController extends \taktwerk\yiiboilerplate\modules\share\controllers\base\FlysystemRoleController
{
    
}
