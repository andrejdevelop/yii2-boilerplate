<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\share\models\FlysystemRole $model
 */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => 'Flysystem Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body flysystem-role-create">

        <h1>
            <?= Yii::t('app', 'Flysystem Role') ?>
            <small>
                <?= $model->id ?>
            </small>
        </h1>

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('app', 'Cancel'),
                    \yii\helpers\Url::previous(),
                    ['class' => 'btn btn-default']
                ) ?>
            </div>
        </div>

        <?= $this->render('_form', [
            'model' => $model,
        ]); ?>

    </div>
</div>
