<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\share\models\FlysystemRole $model
 */

$this->title = 'Flysystem Role ' . $model->id . ', ' . Yii::t('app', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => 'Flysystem Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body flysystem-role-update">

        <h1>
            <?= Yii::t('app', 'Flysystem Role') ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
        ]); ?>

    </div>
</div>