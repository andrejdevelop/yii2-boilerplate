<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\share\models\FlysystemRole $model
 */
$copyParams = $model->attributes;

$this->title = 'Flysystem Role ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Flysystem Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>
<div class="box box-default">
    <div class="giiant-crud box-body" id="flysystem-role-view">

        <!-- flash message -->
        <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= \Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>

        <h1>
            <?= Yii::t('app', 'Flysystem Role') ?>
            <small>
                <?= $model->id ?>
            </small>
        </h1>


        <div class="clearfix crud-navigation">
            <!-- menu buttons -->
            <div class='pull-left'>
                <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_update') ? Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-info']) : '' ?>
                <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_create') ? Html::a('<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('app', 'Copy'), ['create', 'id' => $model->id, 'FlysystemRole' => $copyParams], ['class' => 'btn btn-success']) : '' ?>
                <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_create') ? Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New'), ['create'], ['class' => 'btn btn-success']) : '' ?>
            </div>
            <div class="pull-right">
                <?= Html::a('<span class="glyphicon glyphicon-list"></span> ' . Yii::t('app', 'List FlysystemRoles'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>

        </div>


        <?php $this->beginBlock('app\modules\share\models\FlysystemRole'); ?>


        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'fs_id',
                    'value' => ($model->getFs()->one() ? Html::a($model->getFs()->one()->toString, ['flysystem/view', 'id' => $model->getFs()->one()->id,]) : '<span class="label label-warning">?</span>'),
                ],
                'role',
                'root_path',
                'overwrite_path',
                'read_only',
                'overwrite_access',
            ],
        ]); ?>


        <hr/>

        <?=Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_delete') ?
            Html::a(
                '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'),
                ['delete', 'id' => $model->id],
                [
                'class' => 'btn btn-danger',
                'data-confirm' => '' . Yii::t('app', 'Are you sure to delete this item?') . '',
                'data-method' => 'post',
                ]
            ) : '' ?>

        <?php $this->endBlock(); ?>



        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['app\modules\share\models\FlysystemRole'],
                        'active' => true,
                    ],
                ]
            ]
        );
        ?>
        <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $model]) ?>
    </div>
</div>
