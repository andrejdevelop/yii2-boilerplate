<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\share\models\FlysystemUser $model
 */

$this->title = 'Flysystem User ' . $model->id . ', ' . Yii::t('app', 'Edit');
$this->params['breadcrumbs'][] = ['label' => 'Flysystem Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body flysystem-user-update">

        <h1>
            <?= Yii::t('app', 'Flysystem User') ?>
            <small>
                <?= $model->id ?>
            </small>
        </h1>

        <div class="crud-navigation">
            <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('app', 'View'), ['view', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
        ]); ?>

    </div>
</div>