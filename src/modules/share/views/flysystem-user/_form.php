<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\share\models\FlysystemUser $model
 * @var yii\widgets\ActiveForm $form
 * @var boolean $ajax
 */

?>

    <div class="flysystem-user-form">
        <?php $form = ActiveForm::begin([
            'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
            'id' => 'FlysystemUser',
            'layout' => 'horizontal',
            'enableClientValidation' => true,
            'errorSummaryCssClass' => 'error-summary alert alert-error',
        ]);
        ?>

        <div class="">
            <?php $this->beginBlock('main'); ?>

            <p>
                <?php if ($multiple): ?>
                    <?= Html::hiddenInput('update-multiple', true) ?>
                    <?php foreach ($pk as $id): ?>
                        <?= Html::hiddenInput('pk[]', $id) ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if (!$multiple || ($multiple && isset($show['fs_id']))): ?>
                <?= $form->field($model, 'fs_id')->widget(Select2::classname(),
                    [
                        'data' => ArrayHelper::map(taktwerk\yiiboilerplate\modules\share\models\Flysystem::find()->all(), 'id', 'toString'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select a value...'),
                            'id' => 'fs_id' . ($ajax ? '_ajax_' . $owner : ''),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            (count(taktwerk\yiiboilerplate\modules\share\models\Flysystem::find()->all()) > 50 ? 'minimumInputLength' : '') => 3,
                            (count(taktwerk\yiiboilerplate\modules\share\models\Flysystem::find()->all()) > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::to(['list']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                                return {
                                    q:params.term, m: \'Flysystem\'
                                };
                            }')
                            ],
                        ],
                        'pluginEvents' => [
                            "change" => "function() {
                            if (($(this).val() != null) && ($(this).val() != '')) {
                                // Enable edit icon
                                $(this).next().next().children('button').prop('disabled', false);
                                $.post('" . Url::toRoute('//share/flysystem/entry-details?id=', true) . "' + $(this).val(), {
                                    dataType: 'json'
                                })
                                .done(function(json) {
                                    var json = $.parseJSON(json);
                                    if (json.data != '') {
                                        $('#fs_id_well').show();
                                        $('#fs_id_well').html(json.data);
                                    }
                                })
                            } else {
                                // Disable edit icon
                                $(this).next().next().children('button').prop('disabled', true);
                            }
                        }",
                        ],
                        'addon' => $ajax !== true ? [
                            'append' => [
                                'content' =>
                                    [
                                        \taktwerk\yiiboilerplate\widget\AjaxRelated::widget([
                                            'url' => Url::toRoute(["/share/flysystem/ajax", 'from' => 'fs_id']),
                                            'header' => "<strong>" . Yii::t("app", "Edit") . "</strong>",
                                            'label' => Html::icon('pencil'),
                                            'style' => 'border-radius: 0px',
                                            'disabled' => true,
                                            'update' => true
                                        ]),
                                        \taktwerk\yiiboilerplate\widget\AjaxRelated::widget([
                                            'url' => Url::toRoute(["//share/flysystem/ajax", 'from' => 'fs_id']),
                                            'header' => "<strong>" . Yii::t("app", "Create") . "</strong>",
                                            'label' => Html::icon('plus'),
                                            'style' => 'border-radius: 0px',
                                        ])
                                    ],
                                'asButton' => true
                            ],
                        ] : []
                    ])
                ?>
            <div class="col-sm-3"></div>
            <div id="fs_id_well" class="well col-sm-6"
                 style="margin-left: 8px;<?= (taktwerk\yiiboilerplate\modules\share\models\Flysystem::findOne(['id' => $model->fs_id])->entryDetails == '') ? ' display:none;' : '' ?>"><?= (\taktwerk\yiiboilerplate\modules\share\models\Flysystem::findOne(['id' => $model->fs_id])->entryDetails != '') ? \taktwerk\yiiboilerplate\modules\share\models\Flysystem::findOne(['id' => $model->fs_id])->entryDetails : '' ?></div>
            <div class="clearfix"></div>
        <?php
        ?>
        <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['user_id']))): ?>
                <?= $form->field($model, 'user_id')->widget(Select2::classname(),
                    [
                        'data' => ArrayHelper::map(app\models\User::find()->all(), 'id', 'toString'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select a value...'),
                            'id' => 'user_id' . ($ajax ? '_ajax_' . $owner : ''),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            (count(app\models\User::find()->all()) > 50 ? 'minimumInputLength' : '') => 3,
                            (count(app\models\User::find()->all()) > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::to(['list']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                                return {
                                    q:params.term, m: \'User\'
                                };
                            }')
                            ],
                        ],
                        'pluginEvents' => [
                            "change" => "function() {
                            if (($(this).val() != null) && ($(this).val() != '')) {
                                // Enable edit icon
                                $(this).next().next().children('button').prop('disabled', false);
                                $.post('" . Url::toRoute('//modules/share/user/entry-details?id=', true) . "' + $(this).val(), {
                                    dataType: 'json'
                                })
                                .done(function(json) {
                                    var json = $.parseJSON(json);
                                    if (json.data != '') {
                                        $('#user_id_well').show();
                                        $('#user_id_well').html(json.data);
                                    }
                                })
                            } else {
                                // Disable edit icon
                                $(this).next().next().children('button').prop('disabled', true);
                            }
                        }",
                        ],
                        'addon' => $ajax !== true ? [
                            'append' => [
                                'content' =>
                                    [
                                        \taktwerk\yiiboilerplate\widget\AjaxRelated::widget([
                                            'url' => Url::toRoute(["/modules/share/user/ajax", 'from' => 'user_id']),
                                            'header' => "<strong>" . Yii::t("app", "Edit") . "</strong>",
                                            'label' => Html::icon('pencil'),
                                            'style' => 'border-radius: 0px',
                                            'disabled' => true,
                                            'update' => true
                                        ]),
                                        \taktwerk\yiiboilerplate\widget\AjaxRelated::widget([
                                            'url' => Url::toRoute(["//modules/share/user/ajax", 'from' => 'user_id']),
                                            'header' => "<strong>" . Yii::t("app", "Create") . "</strong>",
                                            'label' => Html::icon('plus'),
                                            'style' => 'border-radius: 0px',
                                        ])
                                    ],
                                'asButton' => true
                            ],
                        ] : []
                    ])
                ?>
                <div class="col-sm-3"></div>
                <div id="user_id_well" class="well col-sm-6"
                     style="margin-left: 8px;<?= (app\models\User::findOne(['id' => $model->user_id])->entryDetails == '') ? ' display:none;' : '' ?>"><?= (app\models\User::findOne(['id' => $model->user_id])->entryDetails != '') ? app\models\User::findOne(['id' => $model->user_id])->entryDetails : '' ?></div>
                <div class="clearfix"></div>
                <?php
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['root_path']))): ?>
                <?= $form->field($model, 'root_path', ['selectors' => ['input' => '#' . Html::getInputId($model, 'root_path') . ($ajax ? '_ajax_' . $owner : '')]])->textInput(['maxlength' => true, 'id' => Html::getInputId($model, 'root_path') . ($ajax ? '_ajax_' . $owner : ''),])
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['overwrite_path']))): ?>
                <?= $form->field($model, 'overwrite_path', ['selectors' => ['input' => '#' . Html::getInputId($model, 'overwrite_path') . ($ajax ? '_ajax_' . $owner : '')]])->checkbox(['id' => Html::getInputId($model, 'overwrite_path') . ($ajax ? '_ajax_' . $owner : '')])
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['read_only']))): ?>
                <?= $form->field($model, 'read_only', ['selectors' => ['input' => '#' . Html::getInputId($model, 'read_only') . ($ajax ? '_ajax_' . $owner : '')]])->checkbox(['id' => Html::getInputId($model, 'read_only') . ($ajax ? '_ajax_' . $owner : '')])
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['overwrite_access']))): ?>
                <?= $form->field($model, 'overwrite_access', ['selectors' => ['input' => '#' . Html::getInputId($model, 'overwrite_access') . ($ajax ? '_ajax_' . $owner : '')]])->checkbox(['id' => Html::getInputId($model, 'overwrite_access') . ($ajax ? '_ajax_' . $owner : '')])
                ?>
            <?php endif; ?>
            </p>
            <?php $this->endBlock(); ?>

            <?= Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => Yii::t("app", Inflector::camel2words('FlysystemUser')),
                        'content' => $this->blocks['main'],
                        'active' => true,
                    ],
                ]
            ]);
            ?>
            <hr/>
            <?php echo $form->errorSummary($model); ?>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' .
                ($model->isNewRecord && !$multiple ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-success',
                    'name' => 'submit-default'
                ]
            );
            ?>

            <?php if (!$ajax && !$multiple) { ?>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> ' . ($model->isNewRecord && !$multiple ? Yii::t('app', 'Create & New') : Yii::t('app', 'Save & New')),
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-default',
                        'name' => 'submit-new'
                    ]
                );
                ?>
                <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> ' . ($model->isNewRecord && !$multiple ? Yii::t('app', 'Create & Close') : Yii::t('app', 'Save & Close')),
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-default',
                        'name' => 'submit-close'
                    ]
                );
                ?>

                <?php if (!$model->isNewRecord) { ?>
                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id],
                        [
                            'class' => 'btn btn-danger',
                            'data-confirm' => '' . Yii::t('app', 'Are you sure to delete this item?') . '',
                            'data-method' => 'post',
                        ]
                    );
                    ?>
                <?php } ?>
            <?php } elseif ($multiple) { ?>
                <?= Html::a('<span class="glyphicon glyphicon-exit"></span> ' . Yii::t('app', 'Close'), [],
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-danger',
                        'name' => 'close'
                    ]
                );
                ?>
            <?php } else { ?>
                <?= Html::a('<span class="glyphicon glyphicon-exit"></span> ' . Yii::t('app', 'Close'), [],
                    [
                        'class' => 'btn btn-danger',
                        'data-dismiss' => 'modal'
                    ]
                );
                ?>
            <?php } ?>
            <?php ActiveForm::end(); ?>

        </div>

    </div>

<?php
$js = <<<JS
$(document).ready(function(){
    var overwritePath = $("#flysystemuser-overwrite_path"),
        overwriteAccess = $("#flysystemuser-overwrite_access"),
        rootPath = $(".field-flysystemuser-root_path"),
        readOnly = $(".field-flysystemuser-read_only");

    if (overwritePath.prop('checked')) {
        rootPath.show();
    } else {
        rootPath.hide();
    }
    if (overwriteAccess.prop('checked')) {
        readOnly.show();
    } else {
        readOnly.hide();
    }
    overwritePath.on('click', function() {
        if (this.checked) {
            rootPath.show();
        } else {
            rootPath.hide();
        }
    });
    overwriteAccess.on('click', function() {
        if (this.checked) {
            readOnly.show();
        } else {
            readOnly.hide();
        }
    });
});
JS;

$this->registerJs($js, \yii\web\View::POS_END);

?>