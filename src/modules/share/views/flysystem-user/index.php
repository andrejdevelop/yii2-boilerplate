<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\grid\GridView;
use kartik\export\ExportMenu;
// use yii\grid\GridView;
use yii\web\View;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var taktwerk\yiiboilerplate\modules\share\models\FlysystemUserSearch $searchModel
 */

$this->title = Yii::t('app', 'Flysystem Users');
$this->params['breadcrumbs'][] = $this->title;


/* ------- Multiple-Delete Batch Action ------ */
$inlineScript = 'var gridViewKey = "flysystem-user";';
$this->registerJs($inlineScript, View::POS_HEAD, 'my-inline-js');
taktwerk\yiiboilerplate\assets\TwAsset::register($this);

$gridColumns = [
	[
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fs_id',
        'format' => 'html',
        'content' => function ($model) {
            return Html::a($model->fs->toString, ['flysystem/view', 'id' => $model->fs_id]);
        },
        'filter' => \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\share\models\Flysystem::find()->all(), 'id', 'toString'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => [
                'placeholder' => '',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
    ],
	[
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_id',
        'format' => 'html',
        'content' => function ($model) {
            return Html::a($model->user->toString, ['user/view', 'id' => $model->user_id]);
        },
        'filter' => \yii\helpers\ArrayHelper::map(app\models\User::find()->all(), 'id', 'toString'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => [
                'placeholder' => '',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
    ],
	'overwrite_path',
	'read_only',
	'overwrite_access',
	'root_path',
    [
        'class' => 'yii\grid\ActionColumn',
        'urlCreator' => function ($action, $model, $key, $index) {
            // using the column name as key, not mapping to 'id' like the standard generator
            $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
            $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
            return Url::toRoute($params);
        },
        'contentOptions' => [
            'nowrap' => 'nowrap',
        ],
        'template' => (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_view') ? '{view}' : '') . ' ' .
                    (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_update') ? '{update}' : '') . ' ' .
                    (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_delete') ? '{delete}' : ''),
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'headerOptions' => [
            'class' => 'kartik-sheet-style'
        ]
    ],
];
?>
<?php $this->beginBlock('info');
\yii\bootstrap\Modal::begin([
    'header' => '<h2>' . Yii::t('app','Information') . '</h2>',
    'toggleButton' => [
        'tag' => 'btn',
        'label' => '?',
        'class' => 'btn btn-default',
        'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px',
    ],
]);?><?= $this->render('@taktwerk-boilerplate/views/_info_modal') ?><?php \yii\bootstrap\Modal::end();
$this->endBlock(); ?>
<div class="box box-default">
    <div class="giiant-crud box-body flysystem-user-index">
		<?php 
		// echo $this->render('_search', ['model' =>$searchModel]);
        ?>
        <h1>
            <?= Yii::t('app', 'Flysystem Users') ?>
            <small>
                <?= Yii::t('app', 'List') ?>
            </small>
        </h1>

        <div class="table-responsive" id="flysystem-user-pjax-container">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => [
                    'id' => 'flysystem-user-grid'
                ],
                'columns' => $gridColumns,
                'containerOptions' => [
                    'style' => 'overflow: auto'
                ], // only set when $responsive = false
                'headerRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'filterRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'toolbar' => [
                    [
                        'content' => $this->blocks['info'],
                    ],
                    [
                        'content' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-default', 'title' => Yii::t('kvgrid', 'Reset Grid')])
                    ],
                    '{export}',
                    '{toggleData}',
                    [
                        'content' => \yii\bootstrap\ButtonDropdown::widget([
                            'id' => 'tw-actions',
                            'encodeLabel' => false,
                            'label' => '<span class="glyphicon glyphicon-flash"></span> ' . Yii::t('app', 'Selected'),
                            'dropdown' => [
                                'options' => [
                                    'class' => 'dropdown-menu-right'
                                ],
                                'encodeLabels' => false,
                                'items' => [
                                    (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_delete-multiple') ?
                                    [
                                        'url' => [
                                            '#'
                                        ],
                                        'options' => ['onclick' => 'deleteMultiple(this);', 'data-url' => Url::toRoute('delete-multiple')],
                                        'label' => '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Remove'),
                                    ] : ''),
                                    (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_update-multiple') ?
                                    [
                                        'url' => [
                                            '#'
                                        ],
                                        'linkOptions' => [
                                            'data-pjax' => '0',
                                            'id' => 'update-multiple-ahref',
                                        ],
                                        'options' => [
                                            'onclick' => 'editMultiple();'
                                        ],
                                        'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Update'),
                                    ] : ''),
                                ]
                            ],
                            'options' => [
                                'class' => 'btn-default'
                            ]
                        ])
                    ],
                ],
                'panel' => [
                    'heading' => "<h3 class=\"panel-title\"><i class=\"glyphicon glyphicon-list\"></i>  " . Yii::t('app', 'Flysystem Users') . "</h3>",
                    'type' => 'default',
                    'before' => (\Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_create') ? Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'New'), ['create'], ['class' => 'btn btn-success', 'data-pjax' => '0']) : '') . ' ' . (\Yii::$app->getUser()->can('Administrator') ?
                            \yii\bootstrap\ButtonDropdown::widget([
                                'id' => 'giiant-relations',
                                'encodeLabel' => false,
                                'label' => '<span class="glyphicon glyphicon-paperclip"></span> ' . Yii::t('app', 'Relations'),
                                'dropdown' => [
                                    'options' => [
                                    ],
                                    'encodeLabels' => false,
                                    'items' => [
                                        [
                                            'url' => ['flysystem/index'],
                                            'label' => '<i class="glyphicon glyphicon-arrow-right">&nbsp;</i>' . Yii::t('app', 'Flysystem'),
                                        ],
                                        [
                                            'url' => ['/user/admin'],
                                            'label' => '<i class="glyphicon glyphicon-arrow-right">&nbsp;</i>' . Yii::t('app', 'User'),
                                        ],
                                    ]
                                ],
                                'options' => [
                                    'class' => 'btn-default'
                                ]
                            ])
                            : '')
                    ,
                    'after' => '{pager}',
                    'footer' => false
                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true,
                    'label' => Yii::t('app', 'Export'),
                ],
                'exportConfig' => [
                    GridView::PDF => [],
                    GridView::HTML => [],
                    GridView::CSV => [],
                    GridView::TEXT => [],
                    GridView::JSON => [],
                    GridView::EXCEL => [
                        //Override default export option with ExportMenu Widget
                        'external' => true,
                        'label' => ExportMenu::widget([
                            'asDropdown' => false,
                            'dataProvider' => $dataProvider,
                            'showColumnSelector' => false,
                            'columns' => $gridColumns,
                            'fontAwesome' => true,
                            'exportConfig' => [
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_EXCEL => false,
                                ExportMenu::FORMAT_TEXT => false,
                            ],
                        ]),
                    ],
                ],
                'striped' => true,
                'pjax' => true,
                'hover' => true,
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => Yii::t('app', 'First'),
                    'lastPageLabel' => Yii::t('app', 'Last')
                ],
            ])
            ?>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    'header' => '<h4>' . Yii::t('app', 'Choose fields to edit') . ':</h4>',
    'id' => 'edit-multiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?= Html::beginForm(['update-multiple'], 'POST'); ?>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-1">
            <div class="checkbox">
                <label for="fs_id">
                    <input type="checkbox" id="fs_id" name="fs_id" value="1">Fs
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-1">
            <div class="checkbox">
                <label for="user_id">
                    <input type="checkbox" id="user_id" name="user_id" value="1">User
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-1">
            <div class="checkbox">
                <label for="overwrite_path">
                    <input type="checkbox" id="overwrite_path" name="overwrite_path" value="1">Overwrite Path
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-1">
            <div class="checkbox">
                <label for="read_only">
                    <input type="checkbox" id="read_only" name="read_only" value="1">Read Only
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-1">
            <div class="checkbox">
                <label for="overwrite_access">
                    <input type="checkbox" id="overwrite_access" name="overwrite_access" value="1">Overwrite Access
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-1">
            <div class="checkbox">
                <label for="root_path">
                    <input type="checkbox" id="root_path" name="root_path" value="1">Root Path
                </label>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
<button type="submit" class="btn btn-success" id="submit-multiple">Update</button>
<?= Html::endForm(); ?>

<?php \yii\bootstrap\Modal::end();
?>

<?php
$js = <<<JS
jQuery.fn.addHidden = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};
$(document).ready(function () {
    $('#update-multiple-ahref').click(function(event) {
        event.preventDefault();
    });
    $('#submit-multiple').on('click', function(e){
        e.preventDefault();
        var keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows');
        var form = $(this).closest('form')
        form.addHidden('no-post', true);
        $.each(keys , function (key, value) {
            form.addHidden('id[]', value);
        });
        form.submit();
    });
});
JS;
$this->registerJs($js);
