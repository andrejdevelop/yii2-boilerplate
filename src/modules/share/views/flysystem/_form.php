<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;

/**
 * @var yii\web\View $this
 * @var app\modules\share\models\Flysystem $model
 * @var yii\widgets\ActiveForm $form
 * @var boolean $ajax
 */

?>

<div class="flysystem-form">
    <?php $form = ActiveForm::begin([
        'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
        'id' => 'Flysystem',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
    ]);
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            <?php if ($multiple): ?>
                <?= Html::hiddenInput('update-multiple', true) ?>
                <?php foreach ($pk as $id): ?>
                    <?= Html::hiddenInput('pk[]', $id) ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['fs_component']))): ?>
                <?= $form->field($model, 'fs_component')->widget(Select2::classname(),
                    [
                        'data' => ArrayHelper::map(\taktwerk\yiiboilerplate\modules\share\models\Flysystem::listFsComponents(), 'name', 'name'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Select a value...'),
                            'id' => 'fs_id' . ($ajax ? '_ajax_' . $owner : ''),
                        ],
                    ])
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['alias']))): ?>
                <?= $form->field($model, 'alias', ['selectors' => ['input' => '#' . Html::getInputId($model, 'alias') . ($ajax ? '_ajax_' . $owner : '')]])->textInput(['maxlength' => true, 'id' => Html::getInputId($model, 'alias') . ($ajax ? '_ajax_' . $owner : ''),])
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['root_path']))): ?>
                <?= $form->field($model, 'root_path', ['selectors' => ['input' => '#' . Html::getInputId($model, 'root_path') . ($ajax ? '_ajax_' . $owner : '')]])->textInput(['maxlength' => true, 'id' => Html::getInputId($model, 'root_path') . ($ajax ? '_ajax_' . $owner : ''),])
                ?>
            <?php endif; ?>
            <?php if (!$multiple || ($multiple && isset($show['read_only']))): ?>
                <?= $form->field($model, 'read_only', ['selectors' => ['input' => '#' . Html::getInputId($model, 'read_only') . ($ajax ? '_ajax_' . $owner : '')]])->checkbox(['id' => Html::getInputId($model, 'read_only') . ($ajax ? '_ajax_' . $owner : '')])
                ?>
            <?php endif; ?>                                            </p>
        <?php $this->endBlock(); ?>

        <?= Tabs::widget([
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => Yii::t("app", Inflector::camel2words('Flysystem')),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
            ]
        ]);
        ?>
        <hr/>
        <?php echo $form->errorSummary($model); ?>
        <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord && !$multiple ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'submit-default'
            ]
        );
        ?>

        <?php if (!$ajax && !$multiple) { ?>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' . ($model->isNewRecord && !$multiple ? Yii::t('app', 'Create & New') : Yii::t('app', 'Save & New')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-default',
                    'name' => 'submit-new'
                ]
            );
            ?>
            <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> ' . ($model->isNewRecord && !$multiple ? Yii::t('app', 'Create & Close') : Yii::t('app', 'Save & Close')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-default',
                    'name' => 'submit-close'
                ]
            );
            ?>

            <?php if (!$model->isNewRecord) { ?>
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id],
                    [
                        'class' => 'btn btn-danger',
                        'data-confirm' => '' . Yii::t('app', 'Are you sure to delete this item?') . '',
                        'data-method' => 'post',
                    ]
                );
                ?>
            <?php } ?>
        <?php } elseif ($multiple) { ?>
            <?= Html::a('<span class="glyphicon glyphicon-exit"></span> ' . Yii::t('app', 'Close'), [],
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-danger',
                    'name' => 'close'
                ]
            );
            ?>
        <?php } else { ?>
            <?= Html::a('<span class="glyphicon glyphicon-exit"></span> ' . Yii::t('app', 'Close'), [],
                [
                    'class' => 'btn btn-danger',
                    'data-dismiss' => 'modal'
                ]
            );
            ?>
        <?php } ?>
        <?php ActiveForm::end(); ?>

    </div>

</div>

