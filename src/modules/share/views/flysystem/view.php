<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\modules\share\models\Flysystem $model
 */
$copyParams = $model->attributes;

$this->title = 'Flysystem ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Flysystems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');
?>
<div class="box box-default">
    <div class="giiant-crud box-body" id="flysystem-view">

        <!-- flash message -->
        <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= \Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>

        <h1>
            <?= Yii::t('app', 'Flysystem') ?>
            <small>
                <?= $model->id ?>
            </small>
        </h1>


        <div class="clearfix crud-navigation">
            <!-- menu buttons -->
            <div class='pull-left'>
                <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_update') ? Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-info']) : '' ?>
                <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_create') ? Html::a('<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('app', 'Copy'), ['create', 'id' => $model->id, 'Flysystem' => $copyParams], ['class' => 'btn btn-success']) : '' ?>
                <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_create') ? Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'New'), ['create'], ['class' => 'btn btn-success']) : '' ?>
            </div>
            <div class="pull-right">
                <?= Html::a('<span class="glyphicon glyphicon-list"></span> ' . Yii::t('app', 'List Flysystems'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>

        </div>


        <?php $this->beginBlock('app\modules\share\models\Flysystem'); ?>

        
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
				'id',
				'fs_component',
				'alias',
				'root_path',
				'read_only',
            ],
        ]); ?>

        
        <hr/>

        <?= Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . \Yii::$app->controller->id . '_delete') ? Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data-confirm' => '' . Yii::t('app', 'Are you sure to delete this item?') . '',
                'data-method' => 'post',
            ]) : '' ?>

        <?php $this->endBlock(); ?>


        
		<?php $this->beginBlock('Flysystem Users'); ?>
		<div style='position: relative'>
			<div style='position:absolute; right: 0px; top: 0px;'>
				<?= Html::a(
        	'<span class="glyphicon glyphicon-list"></span>' . Yii::t('app', 'List All') . ' ' . Yii::t('app', 'Flysystem  Users'),
        	['flysystem-user/index'],
        	['class' => 'btn text-muted btn-xs']
        ) ?>
				<?= Html::a(
        	'<span class="glyphicon glyphicon-plus"></span>' . Yii::t('app', 'New') . ' ' . Yii::t('app', 'Flysystem  User'),
        	['flysystem-user/create', 'FlysystemUser' => ['fs_id' => $model->id]],
        	['class' => 'btn btn-success btn-xs']
        ); ?>
			</div>
		</div>
		
		<div class='clearfix'></div>
		<?php Pjax::begin(['id' => 'pjax-FlysystemUsers', 'enableReplaceState' => false, 'linkSelector' => '#pjax-FlysystemUsers ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>
		<?= '<div class="table-responsive">' . \yii\grid\GridView::widget([
    'layout' => '{summary}{pager}<br/>{items}{pager}',
    'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getFlysystemUsers(), 'pagination' => ['pageSize' => 20, 'pageParam'=>'page-flysystemusers']]),
    'pager'        => [
        'class'          => yii\widgets\LinkPager::className(),
        'firstPageLabel' => Yii::t('app', 'First'),
        'lastPageLabel'  => Yii::t('app', 'Last')
    ],
    'columns' => [[
    'class'      => 'yii\grid\ActionColumn',
    'template'   => '{view} {update}',
    'contentOptions' => ['nowrap'=>'nowrap'],
    'urlCreator' => function ($action, $model, $key, $index) {
        // using the column name as key, not mapping to 'id' like the standard generator
        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
        $params[0] = 'flysystem-user' . '/' . $action;
        return $params;
    },
    'buttons'    => [
        
    ],
    'controller' => 'flysystem-user'
],
'id',
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'class' => yii\grid\DataColumn::className(),
    'attribute' => 'user_id',
    'value' => function ($model) {
        if ($rel = $model->getUser()->one()) {
            return Html::a($rel->toString, ['user/view', 'id' => $rel->id,], ['data-pjax' => 0]);
        } else {
            return '';
        }
    },
    'format' => 'raw',
],
'root_path',
'overwrite_path',
'read_only',
'overwrite_access',
]
]) . '</div>' ?>
		<?php Pjax::end() ?>
		<?php $this->endBlock() ?>


        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['app\modules\share\models\Flysystem'],
                        'active' => true,
                    ],
                    [
                        'content' => $this->blocks['Flysystem Users'],
                        'label' => '<small>' . Yii::t('app', 'Flysystem Users') . '<span class="badge badge-default">' . count($model->getFlysystemUsers()->asArray()->all()) . '</span></small>',
                        'active' => false,
                    ],
                ]
            ]
        );
        ?>
        <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $model]) ?>
    </div>
</div>
