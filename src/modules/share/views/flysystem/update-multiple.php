<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\share\models\Flysystem $model
 */

$this->title = 'Flysystem ' . $model->id . ', ' . Yii::t('app', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => 'Flysystems', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body flysystem-update">

        <h1>
            <?= Yii::t('app', 'Flysystem') ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
        ]); ?>

    </div>
</div>