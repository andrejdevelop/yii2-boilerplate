<?php

namespace taktwerk\yiiboilerplate;

use app\models\User;
use dektrium\user\Finder;
use yii\base\BootstrapInterface;
use yii\base\Application;
use Yii;
use schmunk42\giiant\generators\crud\callbacks\base\Callback;
use schmunk42\giiant\generators\crud\callbacks\yii\Db;
use schmunk42\giiant\generators\crud\callbacks\yii\Html;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\modules\user\controllers\SecurityController;
use yii\base\Event;
use dektrium\user\events\AuthEvent;
use yii\web\UrlRule;

class Bootstrap implements BootstrapInterface
{

    /**
     * @param $app Application
     */
    public function bootstrap($app)
    {
        // Before the rest, we want to set up some aliases
        $this->registerAliases();

        $this->configureCommon($app);
        $this->configureWeb($app);
        $this->configureConsole($app);

        // Register Settings
        $this->registerConfig($app);
        $this->registerViews($app);
        //$this->registerAudit($app);
    }

    /**
     * Register Aliases for the app
     */
    protected function registerAliases()
    {
        Yii::setAlias('@taktwerk-boilerplate', '@vendor/taktwerk/yii-boilerplate/src/');
        Yii::setAlias('@taktwerk-views', '@vendor/taktwerk/yii-boilerplate/src/views');
        Yii::setAlias('@taktwerk-backend-views', '@vendor/taktwerk/yii-boilerplate/src/modules/backend/views');
        Yii::setAlias('@pages', '@app/modules/backend/views');
    }

    /**
     * @param $app Application
     *             Moved all general common configuration from starter-kit to here
     */
    protected function configureCommon($app)
    {

        $this->registerCommonComponents($app);

        $app->get('i18n')->translations = array_merge(
            $app->get('i18n')->translations,
            [
                '*' => [
                    'class' => 'taktwerk\yiiboilerplate\components\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en',
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => YII_DEBUG ? false : true,
                    'forceTranslation' => getenv('APP_FORCE_TRANSLATION'),
                ],
                'app' => [
                    'class' => 'taktwerk\yiiboilerplate\components\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en',
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => YII_DEBUG ? false : true,
                    'forceTranslation' => getenv('APP_FORCE_TRANSLATION'),

                ],
            ]
        );

        $this->registerCommonModules($app);

        // Register Gii and Giiant
        $this->configGiiant($app);
        $this->configGii($app);
    }

    /**
     * @param $app Application
     *             Moved all general web configuration from starter-kit to here
     */
    protected function configureWeb($app)
    {
        if (Yii::$app instanceof \yii\web\Application) {
            $app->get('request')->cookieValidationKey = getenv('APP_COOKIE_VALIDATION_KEY');
            $app->get('user')->identityClass = 'app\models\User';

            $this->registerWebComponents($app);
            $this->registerWebModules($app);
            $this->configureElfinder($app);
            $this->registerCrawlerLogin($app);
            $this->registerWebDAV($app);
            $app->controllerMap = array_merge($app->controllerMap, [
                'google-spreadsheet' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\GoogleSpreadsheetController',
                    'viewPath' => '@taktwerk-views/google-spreadsheet',
                ],
            ]);

        }
    }

    /**
     * @param $app Application
     *             Moved all general console configuration from starter-kit to here
     */
    protected function configureConsole($app)
    {
        if (Yii::$app instanceof \yii\console\Application) {
            $this->registerConsoleComponents($app);
            $this->registerConsoleModules($app);
            $app->controllerNamespace = 'app\commands';
            $app->controllerMap = array_merge(
                [
                    'db' => 'dmstr\console\controllers\MysqlController',
                    'migrate' => [
                        'class' => 'dmstr\console\controllers\MigrateController',
                        'templateFile' => '@taktwerk-boilerplate/views/migration.php',
                    ],
                    'translate' => '\lajax\translatemanager\commands\TranslatemanagerController',
                    'schedule' => 'taktwerk\yiiboilerplate\controllers\ScheduleController',
                    'crawler' => 'taktwerk\yiiboilerplate\commands\CrawlerController',
                    'import' => [
                        'class' => 'taktwerk\yiiboilerplate\modules\import\commands\ImportController',
                    ],
                ],
                $app->controllerMap
            );
        }
    }

    /**
     * Register View data
     * @param Application $app
     */
    protected function registerViews(Application $app)
    {
//        $app->assetManager->bundles['dmstr\web\AdminLteAsset']['skin'] = false; // skin loading not working with assets-prod generation

        // Override dektrium user views with ours
        if (!isset($app->view->theme->pathMap['@dektrium/user/views/admin'])) {
            $app->view->theme->pathMap['@dektrium/user/views/admin'] = '@taktwerk-backend-views/users/admin';
        }
    }

    /**
     * Register changed to the application
     * @param Application $app
     */
    protected function registerConfig(Application $app)
    {
        $this->registerMigrationPaths($app);
        $app->params = array_merge([
            'adminEmail' => getenv('APP_ADMIN_EMAIL'),
            // Presets for CkEditor
            'richTextSimple' => [
                'source', '|',
                'undo', 'redo', 'selectall', '|',
                'preview', 'print', '|',
                'fontname', 'fontsize', 'bold', 'italic', 'underline', 'strikethrough', '|',
                'justifyleft', 'justifycenter', 'justifyright', '|',
            ],
            'richTextStandard' => [
                'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', '|',
                'formatblock', 'forecolor', 'hilitecolor', 'hr', '|',
            ],
            'richTextAdvanced' => [
                'subscript', 'superscript', 'clearhtml', 'quickformat', '|',
                'image', 'multiimage', 'flash', 'media', 'insertfile', '|',
                'table', 'emoticons', 'pagebreak', 'anchor', 'link', 'unlink', '|',
            ],
            'GoogleCredentials' => '@app/config/secret/client_secret.json',
            'disable_external' => false, // Disable all external calls for apps behind proxies and such
        ], $app->params);

        //Disable prefix for icons in Menu widget
        \dmstr\widgets\Menu::$iconClassPrefix = '';
    }

    /**
     * Override the configuration of the Giiant package
     */
    protected function configGiiant(Application $app)
    {
        if (YII_ENV == 'dev' || YII_ENV == 'test') {
            $aceEditorField = function ($attribute, $model, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\trntv\\aceeditor\\AceEditor::className())";
            };

            /**
             * Checkbox-Field | Provider for TINYINT(1) Fields
             * -------------------------------------
             * @author: Benjamin Bur | taktwerk.ch
             */
            $checkBoxField = function ($attribute, $model, $generator) {
                return "\$form->field(\$model, '{$attribute}')->checkbox()";
            };

            /**
             * Date-Field | Provider
             * -------------------------------------
             * @author: Benjamin Bur | taktwerk.ch
             */
            $dateTimePickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\datecontrol\\DateControl::classname(), [
                'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \\kartik\\datetime\\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ])";
            };
            $datePickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\datecontrol\\DateControl::classname(), [
                'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATE,
                'options' => [
                    'type' => \\kartik\\date\\DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ])";
            };
            $timePickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\datecontrol\\DateControl::classname(), [
                'type' => \\kartik\\datecontrol\\DateControl::FORMAT_TIME,
                'options' => [
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control',
                        'showSeconds' => false,
                    ]
                ],
            ])";
            };
            $colourPickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\color\\ColorInput::classname(), [
                'options' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'class' => 'form-control',
                    ]
                ],
            ])";
            };

            Yii::$container->set(
                'taktwerk\yiiboilerplate\templates\crud\providers\CallbackProvider',
                [
                    'columnFormats' => [
                        // hide system fields, but not ID in table
                        'created_at$|updated_at$|created_by$|updated_by$|deleted_at$|deleted_by$' => Callback::false(),
                        // hide all TEXT or TINYTEXT columns
                        '.*' => Db::falseIfText(),
                    ],
                    'activeFields' => [
                        // hide system fields in form
                        'id$' => Db::falseIfAutoIncrement(),
                        'id$|created_at$|updated_at$|created_by$|updated_by$|deleted_at$|deleted_by$' => Callback::false(),
                        '_at$' => $dateTimePickerField,
                        '_date$' => $datePickerField,
                        'is_' => $checkBoxField,
                        'has_' => $checkBoxField,
                        '_time$' => $timePickerField,
                        '_colour$' => $colourPickerField, // Europeans happy
                        '_color$' => $colourPickerField, // Americans happy
                    ],
                    'attributeFormats' => [
                        // render HTML output
                        '_html$' => Html::attribute(),
                    ]
                ]
            );

            $app->controllerMap = array_merge(
                $app->controllerMap,
                [
                    'batch' => [
                        'class' => 'schmunk42\giiant\commands\BatchController',
                        'overwrite' => true,
                        'singularEntities' => false,
                        'modelNamespace' => 'app\\models',
                        'modelQueryNamespace' => 'app\\models\\query',
                        'crudTidyOutput' => true,
                        'crudAccessFilter' => true,
                        'crudControllerNamespace' => 'app\\controllers',
                        'crudSearchModelNamespace' => 'app\\models\\search',
                        'crudViewPath' => '@app/views',
                        'crudPathPrefix' => null,
                        'crudMessageCategory' => 'app',
                        'crudProviders' => [
                            'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\OptsProvider',
                            'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\CallbackProvider',
                            'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\DateTimeProvider',
                            'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\DateProvider',
                            'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\EditorProvider',
                            'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\RelationProvider',
                        ],
                        'tablePrefix' => 'app_',
                        /*'tables' => [
                            'app_profile',
                        ]*/
                    ],
                ]
            );
        }
        return true;
    }

    protected function configGii(Application $app)
    {
        if (YII_ENV == 'dev' || YII_ENV == 'test') {
            $this->registerModule('gii', [
                'class' => 'taktwerk\yiiboilerplate\modules\gii\Module',
                'allowedIPs' => explode(',', getenv('ALLOWED_IPS')),
                'generators' => [
                    'migrik' => 'taktwerk\yiiboilerplate\templates\migration\TwGenerator',
                    'giiant-model' => [
                        'class' => 'taktwerk\yiiboilerplate\templates\model\Generator',
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/model/default'
                        ]
                    ],
                    'giiant-crud' => [
                        'class' => 'taktwerk\yiiboilerplate\templates\crud\Generator',
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/crud/default'
                        ]
                    ],
                    'giiant-test' => [
                        'class' => 'taktwerk\yiiboilerplate\templates\test\Generator',
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/test/default'
                        ]
                    ],
                    'giiant-fixture' => [
                        'class' => 'taktwerk\yiiboilerplate\templates\test\FixtureGenerator',
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/test/default'
                        ]
                    ],
                    'giiant-menu' => [
                        'class' => 'taktwerk\yiiboilerplate\templates\menu\Generator',
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/menu/default'
                        ]
                    ],
                ],
            ], true, false);
        }
    }

    /**
     * @param Application $app
     * Register migrations lookup paths
     */
    protected function registerMigrationPaths(Application $app)
    {
        $app->params = ArrayHelper::merge([
            'yii.migrations' => [
                getenv('APP_MIGRATION_LOOKUP'),
                '@yii/rbac/migrations',
                '@vendor/lajax/yii2-translate-manager/migrations',
//                '@vendor/pheme/yii2-settings/migrations',
                '@dektrium/user/migrations',
                '@bupy7/activerecord/history/migrations',
                '@taktwerk-boilerplate/migrations',
                '@taktwerk-boilerplate/modules/import/migrations',
                '@taktwerk-boilerplate/modules/notification/migrations',
                '@taktwerk-boilerplate/modules/userParameter/migrations',
                '@taktwerk-boilerplate/modules/user/migrations',
            ],
        ], $app->params);
    }

    protected function configureElfinder($app)
    {
        // If controllerMap elfinder-backend is defined within main config file, do not override it,
        // used for backend (logged users only)
        if (!array_key_exists('elfinder-backend', $app->controllerMap)) {
            $app->controllerMap = array_merge($app->controllerMap, [
                'elfinder-backend' => [
                    'class' => 'mihaildev\elfinder\Controller',
                    'disabledCommands' => ['netmount'],
                    'access' => ['?', '@'],
                    'roots' => [
                        [
                            'class' => 'taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume',
                            'component' => 'fs',
                            'name' => 'Storage'
                        ],
                    ],
                ],
            ]);
        }
        // If controllerMap elfinder is defined within main config file, do not override it, used for frontend
        if (!array_key_exists('elfinder', $app->controllerMap)) {
            $app->controllerMap = array_merge($app->controllerMap, [
                'elfinder' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\ElFinderController',
                    'disabledCommands' => ['netmount'],
                    'access' => ['?', '@'],
                    'roots' => [
                        [
                            'class' => 'taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume',
                            'component' => 'fs',
                            'name' => 'Storage',
                            'options' => [
                                'dispInlineRegex' =>
                                    '^(?:(?:image|text)|application/x-shockwave-flash|application/pdf$)'
                            ]
                        ],
                        [
                            'class' => 'taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume',
                            'component' => 'fs_deploy',
                            'name' => 'Deploy Storage',
                            'options' => [
                                'dispInlineRegex' =>
                                    '^(?:(?:image|text)|application/x-shockwave-flash|application/pdf$)'
                            ]
                        ],
                    ],
                ],
            ]);
        }
        // If controllerMap file is defined within main config file, do not override it, used for Sharing
        if (!array_key_exists('file', $app->controllerMap)) {
            $app->controllerMap = array_merge($app->controllerMap, [
                'file' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\ElFinderShareController',
                    'disabledCommands' => ['netmount'],
                    'access' => ['?', '@'],
                    'roots' => [
                        [
                            'class' => 'taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume',
                            'component' => 'fs_seafile',
                            'name' => 'Storage',
                            'options' => [
                                'dispInlineRegex' =>
                                    '^(?:(?:image|text)|application/x-shockwave-flash|application/pdf$)'
                            ]
                        ],
                    ],
                ],
            ]);
        }
    }

    /**
     * Register Audit module
     * @param Application $app
     * in main view file, add: <?php echo \taktwerk\yiiboilerplate\widget\Audit::widget(['view' => $this]) ?>
     * in order to register JS asset
     */
    protected function registerAudit(Application $app)
    {
        $app->setModule('audit', [
            'class' => \bedezign\yii2\audit\Audit::className(),
            'maxAge' => 14, // Set to lower for big projects. This can lead to a HUGE db.
            'panels' => [
                'audit/trail',
                // 'audit/request', // Don't log every request ever
                // 'audit/error', // Covered by sentry
            ],
            'ignoreActions' => ['*'], // Ignore every action
        ]);

        // Register audit commands
        if ($app instanceof \yii\console\Application) {
            $app->controllerMap['twaudit'] = \taktwerk\yiiboilerplate\commands\AuditController::className();
        }
    }

    /**
     * @param Application $app
     * Components that need to be defined for Web and Console applications
     */
    private function registerCommonComponents(Application $app)
    {
        $this->registerComponent('formatter', [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'Europe/Zurich',
            'locale' => 'de-DE',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
        ]);
        $this->registerComponent('authManager', [
            'class' => 'dektrium\rbac\components\DbManager'
        ]);
        // Set the default file cache
        $this->registerComponent('cache', [
            'class' => 'yii\caching\FileCache',
        ]);

        // Set the filesystem cache. Needs to be DbCache on balanced environments.
        $this->registerComponent('fs_cache', [
            'class' => 'yii\caching\FileCache',
        ]);

        $this->registerComponent('db', [
            'class' => '\yii\db\Connection',
            'dsn' => getenv('DATABASE_DSN') . (YII_ENV === 'test' ? '-test' : ''),
            'username' => getenv('DATABASE_USER'),
            'password' => getenv('DATABASE_PASSWORD'),
            'charset' => 'utf8',
            'tablePrefix' => getenv('DATABASE_TABLE_PREFIX'),
            'enableSchemaCache' => YII_ENV_PROD ? true : false,
        ]);

        // Note: enable db sessions, if multiple containers are running
        /*$this->registerComponent('session', [
            'class' => 'yii\web\DbSession'
        ]);*/

//        $this->registerComponent('settings', [
//            'class' => 'pheme\settings\components\Settings',
//        ]);

        $this->registerComponent('fs', [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path' => '@app/../storage',
        ]);

        $disabledFs = !empty($app->params['disabled_fs']) && in_array('assetsprod', $app->params['disabled_fs']);
        if (!$disabledFs) {
            $this->registerComponent('fs_assetsprod', [
                'class' => 'creocoder\flysystem\LocalFilesystem',
                'path' => '@webroot/storage-public',
            ]);
        }

        $disabledFs = !empty($app->params['disabled_fs']) && in_array('seafile', $app->params['disabled_fs']);
        if (!$disabledFs) {
            $this->registerComponent('fs_seafile', [
                'class' => 'taktwerk\yiiboilerplate\components\storage\seafile\SeafileFilesystem',
                'token' => 'a8c8ab9bad35e8afe109a9c768f3dce992c6d278',
                'server' => 'https://tw-cloud.taktwerk.ch',
                'library' => 'test',
            ]);
        }

        $this->registerComponent('userUi', [
            'class' => 'taktwerk\yiiboilerplate\components\UserUi',
        ]);
    }

    /**
     * @param Application $app
     * Modules that need to be defined for Web and Console applications
     */
    private function registerCommonModules(Application $app)
    {
        $this->registerModule('logreader', [
            'class' => 'taktwerk\yiiboilerplate\modules\logreader\Module',
        ]);
        $this->registerModule('queue', [
            'class' => 'taktwerk\yiiboilerplate\modules\queue\Module',
        ]);
        $this->registerModule('notification', [
            'class' => 'taktwerk\yiiboilerplate\modules\notification\Module',
        ]);
//        $this->registerModule('settings', [
//            'class' => 'pheme\settings\Module',
//            'layout' => '@taktwerk-backend-views/layouts/box',
//            'accessRoles' => ['settings-module'],
//        ]);
        $this->registerModule('gridview', [
            'class' => \kartik\grid\Module::className(),
        ]);

        $this->registerModule('import', [
            'class' => 'taktwerk\yiiboilerplate\modules\import\Module',
            'keepLogs' => false,
        ]);

        $this->registerModule('translatemanager', [
            'class' => 'lajax\translatemanager\Module',
            'root' => '@app/views',               // The root directory of the project scan.
            //'scanRootParentDirectory' => true, // Whether scan the defined `root` parent directory, or the folder itself.
            // IMPORTANT: for detailed instructions read the chapter about root configuration.
            'layout' => 'language',         // Name of the used layout. If using own layout use 'null'.
            'allowedIPs' => ['*'],  // IP addresses from which the translation interface is accessible.
            'roles' => ['@'],               // For setting access levels to the translating interface.
            'tmpDir' => '@runtime',         // Writable directory for the client-side temporary language files.
            // IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
            'phpTranslators' => ['::t'],    // list of the php function for translating messages.
            'jsTranslators' => ['lajax.t'], // list of the js function for translating messages.
            'patterns' => ['*.js', '*.php'],// list of file extensions that contain language elements.
            'ignoredCategories' => ['yii', 'language'], // these categories won't be included in the language database.
            'ignoredItems' => ['config'],   // these files will not be processed.
            'scanTimeLimit' => null,        // increase to prevent "Maximum execution time" errors, if null the default max_execution_time will be used
            'searchEmptyCommand' => '!',    // the search string to enter in the 'Translation' search field to find not yet translated items, set to null to disable this feature
            'defaultExportStatus' => 1,     // the default selection of languages to export, set to 0 to select all languages by default
            'defaultExportFormat' => 'json',// the default format for export, can be 'json' or 'xml'
        ]);
        $this->registerModule('arhistory', [
            'class' => \bupy7\activerecord\history\Module::className()
        ], true);
    }

    /**
     * @param Application $app
     * Modules that need to be defined only for Web applications
     */
    private function registerWebModules(Application $app)
    {
        $this->registerModule('backend', [
            'class' => 'taktwerk\yiiboilerplate\modules\backend\Module',
            'layout' => '@taktwerk-backend-views/layouts/main',
        ]);

        $this->registerModule('datecontrol', [
            'class' => '\kartik\datecontrol\Module',
            'ajaxConversion' => true,
            'displaySettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => Yii::$app->formatter->dateFormat,
                \kartik\datecontrol\Module::FORMAT_TIME => Yii::$app->formatter->timeFormat,
                \kartik\datecontrol\Module::FORMAT_DATETIME => Yii::$app->formatter->datetimeFormat,
            ],
            'saveSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:Y-m-d',
                \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
            'saveTimezone' => 'UTC',
            'displayTimezone' => 'UTC'
        ]);

        $this->registerModule('rbac', [
            'class' => 'dektrium\rbac\Module',
            'layout' => '@taktwerk-backend-views/layouts/box',
            'enableFlashMessages' => false,
            'as access' => $app->getBehavior('access')
        ]);

        $this->registerModule('adminer', [
            'class' => 'taktwerk\yiiboilerplate\modules\adminer\Module',
        ]);

        $this->registerModule('share', [
            'class' => 'taktwerk\yiiboilerplate\modules\share\Module',
        ]);

        $this->registerModule('webshell', [
            'class' => \samdark\webshell\Module::className(),
            'allowedIPs' => ['*', '127.0.0.1', '192.168.50.1', '178.220.62.51'],
            'checkAccessCallback' => function (\yii\base\Action $action) {
                return Yii::$app->user->identity->isAdmin;
            },
            'controllerNamespace' => '\taktwerk\yiiboilerplate\modules\webshell\controllers',
            'defaultRoute' => 'index',
            'viewPath' => '@taktwerk-boilerplate/modules/webshell/view',
            'yiiScript' => Yii::getAlias('@root') . '/yii',
        ]);

        // We need to tell the cms module to use the correct layout
        if (!$app->hasModule('cms')) {
            $app->setModules(['cms' => [
                'class' => 'nullref\\cms\\Module',
                'layout' => '@taktwerk-backend-views/layouts/main',
                'components' => [
                    'blockManager' => 'taktwerk\pages\components\BlockManager'
                ]
            ]]);
        } else {
            $cms = $app->getModule('cms');
            $cms->layout = '@taktwerk-backend-views/layouts/main';
            //$cms->components['blockManager'] = 'taktwerk\pages\components\BlockManager'; out because errors with unit tests
        }
    }

    /**
     * @param Application $app
     * @return bool
     * Components that need to be defined only for Web applications
     */
    private function registerWebComponents(Application $app)
    {
        $this->registerAuthClients();
        return false;
    }

    /**
     * @param Application $app
     * @return bool
     * Modules that need to be defined only for Console applications
     */
    private function registerConsoleModules(Application $app)
    {
        return false;
    }

    /**
     * @param Application $app
     * @return bool
     * Components that need to be defined only for Console applications
     */
    private function registerConsoleComponents(Application $app)
    {
        return false;
    }

    /**
     * @param      $component
     * @param      $options
     * @param bool $check   We at most times check if component is already defined in application config, so not to
     *                      override, but sometimes we need because some of components are always defined in application
     *                      core
     * @param bool $production
     *                      Should this component need to be enabled for production environment or only for dev and test
     * @return bool
     */
    private function registerComponent($component, $options, $check = true, $production = true)
    {
        /**
         * Only set config for DEV/TEST
         */
        if (!$production && !YII_ENV_DEV && !YII_ENV_TEST) {
            return false;
        }
        if ($check && !Yii::$app->has($component)) {
            Yii::$app->setComponents([$component => $options]);
        } elseif (!$check) {
            Yii::$app->setComponents([$component => $options]);
        }
    }

    /**
     * @param      $module
     * @param      $options
     * @param bool $check
     *                      We at most times check if module is already defined in application config, so not to
     *                      override, but sometimes we need because some of module are defined by 3rd party vendor
     *                      extensions
     * @param bool $production
     *                      Should this module need to be enabled for production environment or only for dev and test
     * @return bool
     */
    private function registerModule($module, $options, $check = true, $production = true)
    {
        /**
         * Only set config for DEV/TEST
         */
        if (!$production && !YII_ENV_DEV && !YII_ENV_TEST) {
            return false;
        }
        if ($check && !Yii::$app->hasModule($module)) {
            Yii::$app->setModules([$module => $options]);
        } elseif (!$check) {
            Yii::$app->setModules([$module => $options]);
        }
    }

    /**
     * Allowing authenticate over headers, used for Crawler to test backend pages
     * @param $app \yii\base\Application
     */
    private function registerCrawlerLogin($app)
    {
        // Only allow in dev environment
        if (YII_ENV === 'dev') {
            $app->on(\yii\web\Application::EVENT_BEFORE_REQUEST, function () use ($app) {
                if ($app->request->headers->has('Action')
                    && $app->request->headers->get('Action') == 'login'
                    && $app->request->headers->has('Username')
                    && $app->request->headers->has('Password')
                    && ($user = Yii::$container->get(Finder::className())
                        ->findUserByUsernameOrEmail($app->request->headers->get('Username')))
                    && \dektrium\user\helpers\Password::validate(
                        $app->request->headers->get('Password'),
                        $user->password_hash
                    )
                ) {
                    $app->user->login($user);
                }
            });
        }
    }

    /**
     *
     */
    private function registerAuthClients()
    {
        $clients = [];
        if (getenv('FACEBOOK_APP_ID') && getenv('FACEBOOK_SECRET_KEY')) {
            $clients['facebook'] = [
                'class' => 'dektrium\user\clients\Facebook',
                'clientId' => getenv('FACEBOOK_APP_ID'),
                'clientSecret' => getenv('FACEBOOK_SECRET_KEY'),
            ];
        }
        if (getenv('TWITTER_CONSUMER_KEY') && getenv('TWITTER_CONSUMER_SECRET')) {
            $clients['twitter'] = [
                'class' => 'dektrium\user\clients\Twitter',
                'consumerKey' => getenv('TWITTER_CONSUMER_KEY'),
                'consumerSecret' => getenv('TWITTER_CONSUMER_SECRET'),
            ];
        }
        if (getenv('GOOGLE_CLIENT_ID') && getenv('GOOGLE_CLIENT_SECRET')) {
            $clients['google'] = [
                'class' => 'dektrium\user\clients\Google',
                'clientId' => getenv('GOOGLE_CLIENT_ID'),
                'clientSecret' => getenv('GOOGLE_CLIENT_SECRET'),
            ];
        }
        if (getenv('LINKEDIN_CLIENT_ID') && getenv('LINKEDIN_CLIENT_SECRET')) {
            $clients['linkedin'] = [
                'class' => 'dektrium\user\clients\LinkedIn',
                'clientId' => getenv('LINKEDIN_CLIENT_ID'),
                'clientSecret' => getenv('LINKEDIN_CLIENT_SECRET'),
            ];
        }
        if (isset(\Yii::$app->params['authClients'])) {
            $clients = ArrayHelper::merge($clients, Yii::$app->params['authClients']);
        }
        $options = [
            'class' => \yii\authclient\Collection::className(),
            'clients' => $clients,
        ];
        $this->registerComponent('authClientCollection', $options, false);

        // Register events
        Event::on(SecurityController::className(), SecurityController::EVENT_AFTER_AUTHENTICATE, function (AuthEvent $e) {
            // if user account was not created we should not continue
            if ($e->account->user === null) {
                return;
            }
            // we are using switch here, because all networks provide different sets of data
            switch ($e->client->getName()) {
                case 'facebook':
                    $e->account->user->profile->updateAttributes([
                        'name' => $e->client->getUserAttributes()['name'],
                    ]);
                case 'google':
                    $e->account->user->profile->updateAttributes([
                        'name' => $e->client->getUserAttributes()['displayName'],
                    ]);
                case 'twitter':
                    //@TODO
                case 'linkedin':
                    //@TODO
            }

            // after saving all user attributes will be stored under account model
            // Yii::$app->identity->user->accounts['facebook']->decodedData
        });
    }

    /**
     * @param $app Application
     */
    protected function registerWebDAV($app)
    {
        $app->controllerMap = array_merge($app->controllerMap, [
            'dav' => [
                'class' => 'taktwerk\yiiboilerplate\controllers\DavController',
            ],
        ]);
        $rules = [
            'route' => 'dav/index',
            'mode' => \yii\web\UrlRule::PARSING_ONLY,
            'pattern' => 'dav/?<path:.+>?',
        ];
        $app->urlManager->addRules([$rules]);
        Yii::$app->urlManager->ignoreLanguageUrlPatterns = array_merge(
            Yii::$app->urlManager->ignoreLanguageUrlPatterns,
            ['#^dav/index#' => '#^dav#']
        );
    }
}
