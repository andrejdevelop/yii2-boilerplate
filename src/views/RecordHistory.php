<div class="record-history text-muted">
    <?php if ($created_by != null) { ?>
        <?= Yii::t("general", "Created by") ?>: <?= $created_by->username ?> (<?= Yii::$app->formatter->asDatetime($model->created_at) ?>)
    <?php } ?>
    <?php if ($created_at != null) { ?>
        <br/><?= Yii::t("general", "Created at") ?>: <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
    <?php } ?>
    <?php if ($updated_by != null) { ?>
        <br/><?= Yii::t("general", "Updated by") ?>: <?= $updated_by->username ?> (<?= Yii::$app->formatter->asDatetime($model->updated_at) ?>)
    <?php } ?>
    <?php if ($updated_at != null) { ?>
        <br/><?= Yii::t("general", "Updated at") ?>: <?= Yii::$app->formatter->asDatetime($model->updated_at) ?>
    <?php } ?>
    <?php if ($deleted_by != null) { ?>
        <br/><?= Yii::t("general", "Deleted by") ?>: <?= $deleted_by->username ?> (<?= Yii::$app->formatter->asDatetime($model->deleted_at) ?>)
    <?php } ?>
    <?php if ($deleted_at != null) { ?>
        <br/><?= Yii::t("general", "Deleted at") ?>: <?= Yii::$app->formatter->asDatetime($model->deleted_at) ?>
    <?php } ?>
</div>