<?php
/**
 * /srv/www/taktwerk-erp/src/../runtime/giiant/375156467ad52c46593e305e25d54430
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\models\GoogleSpreadsheet $model
 * @var string $relatedTypeForm
 */
$this->title = Yii::t('app', 'Google Spreadsheet') . ' ' . $model->title . ', ' . Yii::t('cruds', 'Edit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Google Spreadsheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body google-spreadsheet-update">

        <div class="crud-navigation">
            <?php echo Html::a(
	'<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('cruds', 'View'),
	['view', 'id' => $model->id],
	['class' => 'btn btn-default']
) ?>
        </div>

        <?php echo $this->render('_form', [
		'model' => $model,
		'inlineForm' => $inlineForm,
		'relatedTypeForm' => $relatedTypeForm,
	]); ?>

    </div>
</div>
