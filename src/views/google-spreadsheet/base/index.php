<?php
    /**
     * /srv/www/taktwerk-erp/src/../runtime/giiant/8d7617987c202c306c0b7e366206e7e4
     *
     * @package default
     */


    use yii\helpers\Html;
    use yii\helpers\Url;
    use taktwerk\yiiboilerplate\grid\GridView;
    use kartik\export\ExportMenu;
    // use yii\grid\GridView;
    use yii\web\View;

    /**
     *
     * @var yii\web\View                                           $this
     * @var yii\data\ActiveDataProvider                            $dataProvider
     * @var boolean                                                $useModal
     * @var boolean                                                $importer
     * @var taktwerk\yiiboilerplate\models\GoogleSpreadsheetSearch $searchModel
     */
    $this->title = Yii::t('cruds', 'Google Spreadsheets');
    $this->params['breadcrumbs'][] = $this->title;


    /* ------- Multiple-Delete Batch Action ------ */
    $inlineScript = 'var gridViewKey = "google-spreadsheet", useModal = ' . ($useModal ? 'true' : 'false') . ';';
    $this->registerJs($inlineScript, View::POS_HEAD, 'my-inline-js');

    $gridColumns = [
        [
            'class' => '\kartik\grid\DataColumn',
            'attribute' => 'user_id',
            'format' => 'html',
            'content' => function ($model) {
                if ($model->user) {
                    return Html::a($model->user->toString, ['user/view', 'id' => $model->user_id]);
                }
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'data' => taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? null : \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\models\User::find()->all(), 'id', 'toString'),
                'initValueText' => taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\models\User::find()->andWhere(['id' => $searchModel->user_id])->all(), 'id', 'toString') : '',
                'options' => [
                    'placeholder' => '',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    (taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                    (taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? 'ajax' : '') => [
                        'url' => \yii\helpers\Url::to(['list']),
                        'dataType' => 'json',
                        'data' => new \yii\web\JsExpression('function(params) {
                        return {
                            q:params.term, m: \'User\'
                        };
                    }')
                    ],
                ]
            ],
            'visible' => Yii::$app->user->can('Administrator'),
        ],
        'title',
        [
            'attribute' => 'url',
            'format' => 'raw',
            'content' => function ($model) {
                return Html::a($model->url, $model->url, ['target' => '_blank']);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function ($action, $model, $key, $index) {

                /**
                 *
                 * @var \yii\db\ActiveRecord $model
                 */
                // using the column name as key, not mapping to 'id' like the standard generator
                $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                return Url::toRoute($params);
            },
            'contentOptions' => [
                'nowrap' => 'nowrap',
            ],
            'template' => (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
                    '_' .
                    \Yii::$app->controller->id . '_view') ? '{view}' : '') .
                ' ' .
                (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
                    '_' .
                    \Yii::$app->controller->id . '_update') ? '{update}' : '') .
                ' ' .
                (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
                    '_' .
                    \Yii::$app->controller->id . '_delete') ? '{delete}' : ''),
            'buttons' => $useModal ? [
                'update' => function ($url, $model, $key) use ($useModal) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $useModal ? '#modalForm' : $url, [
                        'title' => Yii::t('app', 'Update'),
                        'data-toggle' => 'modal',
                        'data-url' => $url,
                        'data-pjax' => 1,
                    ]);
                },
                'delete' => function ($url, $model, $key) use ($useModal) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $useModal ? '#modalForm' : $url, [
                        'title' => Yii::t('app', 'Delete'),
                        'data-url' => $url,
                        'data-pjax' => 1,
                        'class' => 'ajaxDelete',
                    ]);
                },
                'view' => function ($url, $modal, $key) use ($useModal) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $useModal ? '#modalForm' : $url, [
                        'title' => Yii::t('app', 'View'),
                        'data-toggle' => 'modal',
                        'data-url' => $url,
                        'data-pjax' => 1,
                    ]);
                },
            ] : [],
        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'headerOptions' => [
                'class' => 'kartik-sheet-style'
            ]
        ],
    ];
    $exportColumns = $gridColumns;
    foreach ($exportColumns as $column => $value) {
        // Remove checkbox and action columns from Excel export
        if (isset($value['class']) &&
            (strpos($value['class'], 'CheckboxColumn') !== false || strpos($value['class'], 'ActionColumn') !== false)
        ) {
            unset($exportColumns[$column]);
        }
    }
?>
<?php $this->beginBlock('info');
    \yii\bootstrap\Modal::begin([
        'header' => '<h2>' . Yii::t('app', 'Information') . '</h2>',
        'toggleButton' => [
            'tag' => 'btn',
            'label' => '?',
            'class' => 'btn btn-default',
            'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px',
        ],
    ]); ?><?php echo $this->render('@taktwerk-boilerplate/views/_info_modal') ?><?php \yii\bootstrap\Modal::end();
    $this->endBlock(); ?>
<div class="box box-default">
    <div class="giiant-crud box-body google-spreadsheet-index">
        <?php
            // echo $this->render('_search', ['model' =>$searchModel]);
        ?>

        <div class="table-responsive">
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="pageSize"]',
                'options' => [
                    'id' => 'google-spreadsheet-grid'
                ],
                'columns' => $gridColumns,
                'containerOptions' => [
                    'style' => 'overflow: auto'
                ], // only set when $responsive = false
                'headerRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'filterRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'toolbar' => [
                    [
                        'content' => $this->blocks['info'],
                    ],
                    [
                        'content' => \taktwerk\yiiboilerplate\widget\FilterWidget::widget(['model' => $searchModel, 'pjaxContainer' => 'google-spreadsheet-pjax-container'])
                    ],
                    [
                        'content' => Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            ['index', 'reset' => 1],
                            ['class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')]
                        )
                    ],
                    '{export}',
                    ($importer && Yii::$app->user->can('import')) ?
                        \taktwerk\yiiboilerplate\modules\import\widget\Importer::widget([
                            'dataProvider' => $dataProvider,
                            'header' => Yii::t('app', 'Import'),
                            'pjaxContainerId' => 'google-spreadsheet-pjax-container',
                        ]) : '',
                    [
                        'content' => \taktwerk\yiiboilerplate\widget\PageSize::widget([
                            'pjaxContainerId' => 'google-spreadsheet-pjax-container',
                            'model' => $searchModel,
                        ])
                    ],
                    [
                        'content' => \yii\bootstrap\ButtonDropdown::widget([
                            'id' => 'tw-actions',
                            'encodeLabel' => false,
                            'label' => '<span class="glyphicon glyphicon-flash"></span> ' . Yii::t('app', 'Selected'),
                            'dropdown' => [
                                'options' => [
                                    'class' => 'dropdown-menu-right'
                                ],
                                'encodeLabels' => false,
                                'items' => [
                                    (Yii::$app->getUser()->can(
                                        Yii::$app->controller->module->id .
                                        '_' .
                                        \Yii::$app->controller->id .
                                        '_delete-multiple'
                                    ) ?
                                        [
                                            'url' => [
                                                false
                                            ],
                                            'options' => [
                                                'onclick' => 'deleteMultiple(this);',
                                                'data-url' => Url::toRoute('delete-multiple')
                                            ],
                                            'label' => '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' .
                                                Yii::t('app', 'Remove'),
                                        ] : ''),
                                    (Yii::$app->getUser()->can(
                                        Yii::$app->controller->module->id .
                                        '_' .
                                        \Yii::$app->controller->id .
                                        '_update-multiple'
                                    ) ?
                                        [
                                            'url' => '#edit-multiple',
                                            'linkOptions' => [
                                                'data-toggle' => 'modal',
                                            ],
                                            'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;' .
                                                Yii::t('app', 'Update'),
                                        ] : ''),
                                ]
                            ],
                            'options' => [
                                'class' => 'btn-default'
                            ]
                        ])
                    ],
                ],
                'panel' => [
                    'heading' => "<h3 class=\"panel-title\"><i class=\"glyphicon glyphicon-list\"></i>  " .
                        Yii::t('cruds', 'Google Spreadsheets') .
                        "</h3>" . \taktwerk\yiiboilerplate\modules\import\widget\ImportResult::widget(),
                    'type' => 'default',
                    'before' => (\Yii::$app->getUser()->can(
                            Yii::$app->controller->module->id .
                            '_' .
                            \Yii::$app->controller->id .
                            '_create'
                        ) ?
                            Html::a(
                                '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'New'),
                                [$useModal ? '#modalForm' : 'create'],
                                [
                                    'class' => 'btn btn-success',
                                    'data-pjax' => $useModal,
                                    'data-toggle' => 'modal',
                                    'data-url' => Url::toRoute('create')
                                ]
                            ) : '') . ' ' . (\Yii::$app->getUser()->can('Authority') ?
                            \yii\bootstrap\ButtonDropdown::widget([
                                'id' => 'giiant-relations',
                                'encodeLabel' => false,
                                'label' => '<span class="glyphicon glyphicon-paperclip"></span> ' .
                                    Yii::t('cruds', 'Relations'),
                                'dropdown' => [
                                    'options' => [
                                    ],
                                    'encodeLabels' => false,
                                    'items' => [
                                        [
                                            'url' => ['/user/admin'],
                                            'label' => '<i class="glyphicon glyphicon-arrow-right">&nbsp;</i>' .
                                                Yii::t('cruds', 'User'),
                                        ],
                                    ]
                                ],
                                'options' => [
                                    'class' => 'btn-default'
                                ]
                            ])
                            : '')
                    ,
                    'after' => '{pager}',
                    'footer' => false
                ],
                // set export properties
                'export' => [
                    'fontAwesome' => true,
                    'label' => Yii::t('kvgrid', 'Export'),
                ],
                'exportConfig' => [
                    GridView::PDF => [],
                    GridView::HTML => [],
                    GridView::CSV => [],
                    GridView::TEXT => [],
                    GridView::JSON => [],
                    GridView::EXCEL => [
                        //Override default export option with ExportMenu Widget
                        'external' => true,
                        'label' => ExportMenu::widget([
                            'asDropdown' => false,
                            'dataProvider' => $dataProvider,
                            'showColumnSelector' => false,
                            'columns' => $exportColumns,
                            'fontAwesome' => true,
                            'exportConfig' => [
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_EXCEL => false,
                                ExportMenu::FORMAT_TEXT => false,
                            ],
                        ]),
                    ],
                ],
                'striped' => true,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'google-spreadsheet-pjax-container',
                    ],
                    'clientOptions' => [
                        'method' => 'POST'
                    ]
                ],
                'hover' => true,
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => Yii::t('kvgrid', 'First'),
                    'lastPageLabel' => Yii::t('kvgrid', 'Last')
                ],
            ])
            ?>
        </div>
    </div>
</div>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    'header' => '<h4>' . Yii::t('app', 'Choose fields to edit') . ':</h4>',
    'id' => 'edit-multiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php echo Html::beginForm(['update-multiple'], 'POST'); ?>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="user_id">
                <input
                    type="checkbox"
                    id="user_id"
                    name="user_id"
                    value="1"
                >User
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="title">
                <input
                    type="checkbox"
                    id="title"
                    name="title"
                    value="1"
                >Title
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6 col-sm-offset-1">
        <div class="checkbox">
            <label for="url">
                <input
                    type="checkbox"
                    id="url"
                    name="url"
                    value="1"
                >Url
            </label>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<button type="submit" class="btn btn-success" id="submit-multiple">Update</button>
<?php echo Html::endForm(); ?>

<?php \yii\bootstrap\Modal::end();
?>

<?php
    $js = <<<JS
jQuery.fn.addHidden = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};
$(document).ready(function () {
    $('#submit-multiple').on('click', function(e){
        e.preventDefault();
        var keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
            form = $(this).closest('form');
        // Remove old values to prevent duplicated Ids submit
        form.find('input[name="id[]"]').remove();
        form.addHidden('no-post', true);
        $.each(keys , function (key, value) {
            form.addHidden('id[]', value);
        });
        if (useModal) {
            $('body').addClass('kv-grid-loading');
            $.post(
                form.attr("action"),
                form.serialize()
            )
            .done(function (data) {
                $('#modalFormMultiple').modal('show');
                $('#modalFormMultiple').find('.modal-body').html(data);
                $('body').removeClass('kv-grid-loading');
                formSubmitEvent();
                $('.closeMultiple').on('click', function(e){
                    e.preventDefault();
                    $('#modalFormMultiple').modal('hide');
                });
            })
            return false;
        } else {
            form.submit();
        }
    });
});
JS;
    $this->registerJs($js);
?>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_LARGE,
    'id' => 'modalFormMultiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \yii\bootstrap\Modal::end();
?>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_LARGE,
    'id' => 'modalForm',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \yii\bootstrap\Modal::end();
?>
<?php
    if ($useModal) {
        \taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
    }
?>
