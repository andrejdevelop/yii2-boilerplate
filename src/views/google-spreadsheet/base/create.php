<?php
/**
 * /srv/www/taktwerk-erp/src/../runtime/giiant/550b5d6e91bda0f79d4dbde409c53f05
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\models\GoogleSpreadsheet $model
 * @var string $relatedTypeForm
 */
$this->title = Yii::t('cruds', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Google Spreadsheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body google-spreadsheet-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?php echo Html::a(
	Yii::t('cruds', 'Cancel'),
	\yii\helpers\Url::previous(),
	[
		'class' => 'btn btn-default'
	]
) ?>
            </div>
        </div>

        <?php echo $this->render('_form', [
		'model' => $model,
		'inlineForm' => $inlineForm,
		'action' => $action,
		'relatedTypeForm' => $relatedTypeForm,
	]); ?>

    </div>
</div>
