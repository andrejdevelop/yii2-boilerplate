<?php
/**
 * @link http://www.agence-inspire.com/
 */

namespace taktwerk\yiiboilerplate\action;

use Yii;
use yii\base\Action;
use yii\web\Response;

/**
 * The ajax new action.
 *
 * Handles form submitting.
 *
 * @author Mehdi Achour <mehdi.achour@agence-inspire.com>
 */
class AjaxRelatedAction extends Action
{

    /**
     * @var yii\db\ActiveRecord The model instance
     */
    public $model = null;
    /**
     * @var string The view file holding the form. It must use the $model variable for the model instance
     */
    public $viewFile = null;

    /**
     * @var bool
     */
    public $update = false;

    /**
     * @var array of yii\db\ActiveRecord The translations instances
     */
    public $translations = null;

    /**
     * @var array
     */
    public $languageCodes = null;

    /**
     * @var integer
     */
    public $modelId;

    /**
     * @var string
     */
    public $modelTranslationName;

    /**
     * @var string
     */
    public $modelTranslationNamespace;

    /**
     * @var string
     */
    public $modelTranslationAttribute;

    public $depend;

    public $dependOn;

    public $relation;

    public $relationId;

    public $relationIdValue;

    public $inlineForm;

    /**
     *
     */
    public function run()
    {
        $pass = true;
        $this->controller->layout = false;
        $options = [];
        $model = $this->model;
        // Always use model primary key for fetching
        $pk = $model::primaryKey()[0];
        if (!is_null($this->translations)) {
            $options['translations'] = $this->translations;
            if (\Yii::$app->request->isPost) {
                foreach ($_POST[$this->modelTranslationName] as $related) {
                    $model = $this->modelTranslationNamespace;
                    $translation = new $model();
                    $translation->load($related);
                    if ($this->update) {
                        $translation->{$this->modelTranslationAttribute} = $this->model->id;
                        if (!$translation->validate()) {
                            $pass = false;
                            break;
                        }
                    } else {
                        $attributes = $translation->attributes;
                        unset($attributes[$this->modelTranslationAttribute]);
                        foreach ($attributes as $attribute => $value) {
                            $validateAttributes[] = $attribute;
                        }
                        if (!$translation->validate($validateAttributes)) {
                            $pass = false;
                            break;
                        }
                    }
                }
            }
        }
        if (!is_null($this->languageCodes)) {
            $options['languageCodes'] = $this->languageCodes;
        }

        if ($pass && $this->model->load($_POST) && $this->model->save()) {
            if (!is_null($this->translations)) {
                foreach ($_POST[$this->modelTranslationName] as $related) {
                    $model = $this->modelTranslationNamespace;
                    if ($this->update) {
                        $translation = $model::findOne(['language_id' => $related[$this->modelTranslationName]['language_id'], $this->modelTranslationAttribute => $this->model->id]);
                        if (!$translation) {
                            $translation = new $model();
                        }
                        $translation->load($related);
                        $translation->save();
                    } else {
                        $translation = new $model();
                        $translation->load($related);
                        $translation->{$this->modelTranslationAttribute} = $this->model->id;
                        $translation->save();
                    }
                }
            }
            if ($this->depend) {
                $reflection = new \ReflectionClass($this->model);
                $ns = $reflection->getNamespaceName();
                $junkModel = $ns . '\\' . $this->relation;
                $reflection = new \ReflectionClass($this->model);
                $modelName = $reflection->getShortName();
                $relationModel = new $junkModel();
                $relationId = $this->relationId;
                $relationModel->$relationId = $this->relationIdValue;
                $foreignKey = strtolower($modelName) . '_id';
                $relationModel->$foreignKey = $this->model->id;
                $relationModel->save();
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'id' => $this->model->$pk,
                'label' => $this->model->toString,

            ];
        }
        return $this->controller->renderAjax($this->viewFile, array_merge([
            'model' => $this->model,
            'ajax' => true,
            'owner' => Yii::$app->getRequest()->get('from'),
            'inlineForm' => $this->inlineForm,
        ], $options));
    }

}