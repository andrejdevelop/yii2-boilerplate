<?php
/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var yii\gii\generators\crud\Generator $generator
 */
echo "    <script src=\"https://code.jquery.com/jquery-1.9.1.min.js\"></script>";
echo "<script type='text/javascript'>
(function( $ ){
    $( document ).ready(function() {
        $('#generator-modelclass').blur(function(){
            var modelClass = $('#generator-modelclass').val();
            $('#generator-searchmodelclass').val(modelClass.replace(/models/gi, 'models\\\search'));
            $('#generator-controllerclass').val(modelClass.replace(/models/gi,'controllers') + 'Controller');
            $('#generator-viewpath').val(modelClass.replace(/models?/gi,'views').replace(/\\\[^\\\]*$/,'').replace(/\\\/gi,'/').replace(/app/gi,'@app') );
        });
        $('#check-all').click(function(){
            var checked = this.checked;
            // iterate .default-view-files
            //$(':checkbox').each(function () { this.checked = checked });
             $('.default-view-files')
               .find(\"input[type='checkbox']\")
               .prop('checked', this.checked);
        });

        $(':checkbox').each(function () { this.checked = true });
     });
})( jQuery );

</script>";
echo $form->field($generator, 'modelClass');
echo $form->field($generator, 'searchModelClass');
echo $form->field($generator, 'controllerClass');
echo $form->field($generator, 'controllerApiModule');
echo $form->field($generator, 'overwriteRestControllerClass')->checkbox();
echo $form->field($generator, 'baseControllerClass');
echo $form->field($generator, 'viewPath');
echo $form->field($generator, 'pathPrefix');
echo $form->field($generator, 'enableI18N')->checkbox();
echo $form->field($generator, 'singularEntities')->checkbox();
echo $form->field($generator, 'indexWidgetType')->dropDownList(
    [
        'grid' => 'GridView',
        'list' => 'ListView',
    ]
);
echo $form->field($generator, 'formLayout')->dropDownList(
    [
        /* Form Types */
        'vertical' => 'vertical',
        'horizontal' => 'horizontal',
        'inline' => 'inline',
    ]
);
echo $form->field($generator, 'twoColumnsForm')->dropDownList(
    [
        /* Form Types */
        true => 'Yes',
        false => 'No',
    ]
);
echo $form->field($generator, 'actionButtonClass')->dropDownList(
    [
        'yii\\grid\\ActionColumn' => 'Default',
    ]
);
echo $form->field($generator, 'providerList')->checkboxList($generator->generateProviderCheckboxListData());
