<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace taktwerk\yiiboilerplate\templates\crud;

use taktwerk\yiiboilerplate\helpers\DebugHelper;
use Yii;
use yii\base\Exception;
use yii\db\ColumnSchema;
use yii\gii\CodeFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use schmunk42\giiant\generators\crud\Generator as SchmunkGenerator;
use yii\db\Schema;

/**
 * This generator generates an extended version of CRUDs.
 * @author Tobais Munk <schmunk@usrbin.de>
 * @since 1.0
 */
class Generator extends SchmunkGenerator
{
    use ProviderTrait, ModelTrait;
    // Default Values by taktwerk ...
    public $template = 'taktwerk';
    public $enableI18N = true;
    public $searchModelClass = 'app\\models\\search\\';
    public $controllerClass = 'app\\controllers\\';
    public $controllerApiModule = 'v1';
    public $modelClass = 'app\\models\\';
    public $twoColumnsForm = true;
    public $baseControllerClass = 'taktwerk\\yiiboilerplate\\controllers\\TwCrudController';
    public $selectModal = false;
    private $_p = [];


    /**
     * @var null comma separated list of provider classes
     */
    public $providerList = null;

    /**
     * @todo review
     * @var string
     */
    public $actionButtonClass = 'yii\grid\ActionColumn';

    /**
     * @var array relations to be excluded in UI rendering
     */
    public $skipRelations = [];

    /**
     * @var string default view path
     */
    public $viewPath = '@backend/views';

    public $tablePrefix = null;
    public $pathPrefix = null;
    public $formLayout = 'horizontal';

    /**
     * @var string translation catalogue
     */
    public $messageCatalog = 'app';

    /**
     * @var int maximum number of columns to show in grid
     */
    public $gridMaxColumns = 8;

    /**
     * @return string
     */
    public function getName()
    {
        return 'Giiant TW-CRUD';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'This generator generates an extended version of CRUDs.';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['twoColumnsForm', 'safe'],
        ]);
    }

    /**
     * Generates code for active field
     * @param string $attribute
     * @param string $name
     * @param boolean $hidden
     * @param string $id
     * @return string
     */
    public function generateActiveField(
        $attribute,
        $name = null,
        $hidden = false,
        $id = null,
        $translate = false,
        $indentMore = 0
    ) {
        $indentMore = str_repeat(' ', 4 * $indentMore);
        $tableSchema = $this->getTableSchema();

        // What is this scenario? No tableschema the column isn't set? Feels like generating a login page of some sort.
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->hint(\$model->getAttributeHint('$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')->hint(\$model->getAttributeHint('$attribute'))";
            }
        }

        $model = $name === null ? "\$model" : "\$translation";
        $name = $name !== null ? "'name' => \"$name\", " : '';
        $selector = ",
$indentMore                [
$indentMore                    'selectors' => [
$indentMore                        'input' => '#' .
$indentMore                        " .
            ($id !== null ? ($translate ? "'$attribute' . " : "$attribute") .
                "\$key" : "    Html::getInputId($model, '$attribute')") . " . \$owner
$indentMore                    ]
$indentMore                ]";
        $id = "$indentMore'id' => " .
            ($id !== null ? "'$attribute' . \$key" : "Html::getInputId($model, '$attribute')") . " . \$owner";
        $column = $tableSchema->columns[$attribute];
        $comment = $this->extractComments($column);
        $nameid = trim(rtrim(trim($name) . "\n" . str_repeat(' ', 28) . $id, ','));
        if ($hidden) {
            return "HTML::activeHiddenInput(
$indentMore                $model,
$indentMore                '$attribute',
$indentMore                [
$indentMore                    $nameid
$indentMore                ]
$indentMore            );";
        } elseif ($column->phpType === 'boolean' ||
            $column->dbType === 'tinyint(1)' ||
            substr($column->name, 0, 3) == 'is_' ||
            substr($column->name, 0, 4) == 'has_' ||
            ($comment && $comment->inputtype === 'checkbox')
        ) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                     ->checkbox(
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (($column->type === 'text' && ($comment && $comment->inputtype === 'editor'))
            || (strpos($column->name, '_html') !== false)
        ) {
            $toolset = 'standard';
            if ($comment && $comment->toolset) {
                $toolset = $comment->toolset;
            }
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         \\taktwerk\\yiiboilerplate\\widget\\CKEditor::class,
$indentMore                         [
$indentMore                             $nameid
$indentMore                             'editorOptions' => \\mihaildev\\elfinder\\ElFinder::ckeditorOptions(
$indentMore                                 'elfinder-backend',
$indentMore                                 [
$indentMore                                     'height' => 300,
$indentMore                                     'preset' => '$toolset',
$indentMore                                 ]
$indentMore                             ),
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (($column->type === 'string' && ($comment && strtolower($comment->inputtype) === 'qrcode'))) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         \\taktwerk\\yiiboilerplate\\widget\\QrInput::class,
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (($column->type === 'string' && ($comment && strtolower($comment->inputtype) === 'googlemap'))) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         \\taktwerk\\yiiboilerplate\\widget\\GoogleMapInput::class,
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            // HTML 5 input fields
        } elseif (strpos($column->name, 'email') !== false || ($comment && $comment->inputtype === 'email')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'email',
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (strpos($column->name, 'telephone') !== false ||
            strpos($column->name, '_tel') !== false ||
            ($comment && $comment->inputtype === 'telephone') ||
            strpos($column->name, 'phone') !== false ||
            ($comment && $comment->inputtype === 'phone')
        ) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'tel',
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (strpos($column->name, 'search') !== false || ($comment && $comment->inputtype === 'search')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'search',
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (strpos($column->name, 'url') !== false || ($comment && $comment->inputtype === 'url')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                    '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'url',
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->type === 'text' || ($comment && $comment->inputtype === 'text')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textarea(
$indentMore                        [
$indentMore                            'rows' => 6,
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->dbType === 'date' || ($comment && $comment->inputtype === 'date')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )->widget(
$indentMore                \\kartik\\datecontrol\\DateControl::class,
$indentMore                [
$indentMore                    $nameid,
$indentMore                    'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATE,
$indentMore                    'options' => [
$indentMore                        'type' => \\kartik\\date\\DatePicker::TYPE_COMPONENT_APPEND,
$indentMore                        'pluginOptions' => [
$indentMore                            'todayHighlight' => true,
$indentMore                            'autoclose' => true,
$indentMore                            'class' => 'form-control'
$indentMore                        ]
$indentMore                    ],
$indentMore                ]
$indentMore            )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->dbType === 'datetime' || ($comment && $comment->inputtype === 'datetime')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )->widget(
$indentMore                \\kartik\\datecontrol\\DateControl::class,
$indentMore                [
$indentMore                    $nameid,
$indentMore                    'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATETIME,
$indentMore                    'ajaxConversion' => true,
$indentMore                    'options' => [
$indentMore                        'type' => \\kartik\\datetime\\DateTimePicker::TYPE_COMPONENT_APPEND,
$indentMore                        'pickerButton' => ['icon' => 'time'],
$indentMore                        'pluginOptions' => [
$indentMore                            'todayHighlight' => true,
$indentMore                            'autoclose' => true,
$indentMore                            'class' => 'form-control'
$indentMore                        ]
$indentMore                    ],
$indentMore                ]
$indentMore            )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->dbType === 'time' || ($comment && $comment->inputtype === 'time')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->widget(
$indentMore                \\kartik\\time\\TimePicker::class,
$indentMore                [
$indentMore                    $nameid,
$indentMore                    'pluginOptions' => [
$indentMore                        'showSeconds' => true,
$indentMore                        'class' => 'form-control'
$indentMore                    ],
$indentMore                ]
$indentMore            )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (!empty($column) && $this->checkIfUploaded($column)) {
            $limitation = false;
            if ($comment) {
                if ($comment->fileSize) {
                    $limitation = "\n" . str_repeat(' ', 24) . "'maxFileSize' => $comment->fileSize,";
                }
                if ($comment->allowedExtensions && is_array($comment->allowedExtensions)) {
                    $limitation .= "\n" . str_repeat(' ', 24) . "'allowedExtensions' => [";
                    foreach ($comment->allowedExtensions as $extension) {
                        $limitation .= "'$extension', ";
                    }
                    $limitation = rtrim($limitation, ', ') . '],';
                }
            }
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->widget(
$indentMore                \\taktwerk\\yiiboilerplate\\widget\\FileInput::class,
$indentMore                [
$indentMore                    'pluginOptions' => [
$indentMore                        " . (!$column->allowNull ? "'required' => true," : '') . "
$indentMore                        " . ($limitation ? $limitation : '') . "
$indentMore                        'initialPreview' => (!empty($model" . "->$attribute) ? [
$indentMore                            $model" . "->getFileUrl('$attribute')
$indentMore                        ] : ''),
$indentMore                        'initialCaption' => $model" . "->$attribute,
$indentMore                        'initialPreviewAsData' => true,
$indentMore                        'initialPreviewFileType' => $model" . "->getFileType('$attribute'),
$indentMore                        'fileActionSettings' => [
$indentMore                            'indicatorNew' => $model->$attribute === null ? '' : '<a href=\" ' . $model->" . "getFileUrl('$attribute') . '\" target=\"_blank\"><i class=\"glyphicon glyphicon-hand-down text-warning\"></i></a>',
$indentMore                            'indicatorNewTitle' => \\Yii::t('app','Download'),
$indentMore                        ],
$indentMore                        'overwriteInitial' => true
$indentMore                    ],
$indentMore                     'pluginEvents' => [
$indentMore                         'fileclear' => 'function() { var prev = $(\"input[name=\\'\" + $(this).attr(\"name\") + \"\\']\")[0]; $(prev).val(-1); }',
$indentMore                        " . (!$column->allowNull ? "'fileclear' => 'function() { var prev = $(\"input[name=\\'\" + $(this).attr(\"name\") + \"\\']\")[0]; $(prev).val($(this)[0].files[0].name); }'," : '') . "
$indentMore                     ],
$indentMore                    'options' => [
$indentMore                        $nameid
$indentMore                    ]
$indentMore                ]
$indentMore            )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name) ||
                ($comment && $comment->inputtype === 'password')
            ) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if ((is_array($column->enumValues) && count($column->enumValues) > 0) ||
                ($comment && $comment->inputtype === 'enum')
            ) {
                $dropOptions = $indentMore . "[";
                foreach ($column->enumValues as $enumValue) {
                    $dropOptions .= "\n$indentMore" . str_repeat(" ", 32) . "'" .
                        $enumValue . "' => Yii::t('app', '" . Inflector::humanize($enumValue) . "'),";
                }
                $dropOptions .= "\n$indentMore" . str_repeat(" ", 28) . "$indentMore]";
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->widget(
$indentMore                        Select2::class,
$indentMore                        [
$indentMore                            'options' => [
$indentMore                                $nameid
$indentMore                            ],
$indentMore                            'data' => $dropOptions,
$indentMore                            'hideSearch' => 'true',
$indentMore                            'pluginOptions' => [
$indentMore                                'allowClear' => " . ($column->allowNull ? "true" : "false") . "
$indentMore                            ],
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            } elseif ($column->phpType === 'integer' || $column->phpType === 'double') {
                $step = $column->phpType === 'double' ? 'any' : '1';
                $min = $column->unsigned ? "'min' => '0'," : '';
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'number',
$indentMore                            'step' => '$step',
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $min
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            } elseif ($column->phpType !== 'string' ||
                $column->size === null ||
                ($comment && $comment->inputtype === 'string')
            ) {
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->$input(
$indentMore                        [
$indentMore                            $nameid,
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            } else {
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->$input(
$indentMore                        [
$indentMore                            'maxlength' => true,
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            }
        }
    }

    /**
     * Specify the API Controller Location in modules/{module}/controllers
     * @inheritdoc
     */
    public function generate()
    {
        if ($this->singularEntities) {
            $this->modelClass = Inflector::singularize($this->modelClass);
            $this->controllerClass = Inflector::singularize(
                    substr($this->controllerClass, 0, strlen($this->controllerClass) - 10)
                ) . 'Controller';
            $this->searchModelClass = Inflector::singularize($this->searchModelClass);
        }

        $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');
        $baseControllerFile = StringHelper::dirname($controllerFile) .
            '/base/' . StringHelper::basename($controllerFile);
        $apiDir = StringHelper::dirname(Yii::getAlias('@' . str_replace('\\', '/', ltrim('\\app\\modules\\' .
                $this->controllerApiModule . '\\controllers\\', '\\'))));
        $restControllerFile = $apiDir . '/' . StringHelper::basename($controllerFile);

        if (!file_exists($apiDir)) {
            throw new Exception("Rest API Dir does not exist: " . $apiDir);
        }
        $files[] = new CodeFile($baseControllerFile, $this->render('controller.php'));
        $params['controllerClassName'] = \yii\helpers\StringHelper::basename($this->controllerClass);

        if ($this->overwriteControllerClass || !is_file($controllerFile)) {
            $files[] = new CodeFile($controllerFile, $this->render('controller-extended.php', $params));
        }

        if ($this->overwriteRestControllerClass || !is_file($restControllerFile)) {
            $files[] = new CodeFile($restControllerFile, $this->render('controller-rest.php', $params));
        }

        if (!empty($this->searchModelClass)) {
            $searchModel = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->searchModelClass, '\\') . '.php'));
            $files[] = new CodeFile($searchModel, $this->render('search.php'));
        }

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views';
        foreach (scandir($templatePath) as $file) {
            if (empty($this->searchModelClass) && $file === '_search.php') {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file"));
            }
        }

        /*$viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views/base';
        foreach (scandir($templatePath) as $file) {
            if (empty($this->searchModelClass) && $file === '_search.php') {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/base/$file", $this->render("views/base/$file"));
            }
        }*/

        return $files;
    }

    /**
     * Get Foreign Model-ClassName (with namespace)
     * @param $attribute
     * @return mixed|string
     */
    public function fetchForeignClass($attribute)
    {
        foreach ($this->getModelRelations($this->modelClass) as $modelRelation) {
            foreach ($modelRelation->link as $linkKey => $link) {
                if ($attribute == $link) {
                    return $foreignModelClass = $modelRelation->modelClass;
                }
            }
        }
        return null;
    }

    /**
     * @param $attribute
     * @return int|null|string
     */
    public function fetchForeignAttribute($attribute)
    {
        $foreignModelClass = null;
        foreach ($this->getModelRelations($this->modelClass) as $modelRelation) {
            foreach ($modelRelation->link as $linkKey => $link) {
                if ($attribute == $link) {
                    return $linkKey;
                }
            }
        }
        return null;
    }

    /**
     * @param ColumnSchema $column
     * @param $comment
     * @return string
     */
    public function activeFieldDepend(ColumnSchema $column, $comment)
    {
        $attribute = $column->name;
        $tableSchema = $this->getTableSchema();

        $fullModel = $this->fetchForeignClass($attribute);
        $foreignPk = $fullModel::primaryKey()[0];
        $reflection = new \ReflectionClass($fullModel);
        $attributeLabel = $this->fetchForeignAttribute($attribute);

        $on = $comment->inputtype->depend->on;
        $onFullModel = $reflection->getNamespaceName() . '\\' . $on;
        $onAttribute = $comment->inputtype->depend->onAttribute;
        $onRelation = $comment->inputtype->depend->onRelation;

        if ($attributeLabel == null) {
            $attributeLabel = $attribute;
        }
        $column = $tableSchema->columns[$attribute];
        $foreignController = $this->getForeignController($fullModel);

        $ajaxRelatedDep = $on . ucfirst(Inflector::singularize($onRelation));
        $ajaxRelatedDepOn = lcfirst($on) . '_id';
//        $append = !$this->twoColumnsForm ? "<div class=\"col-sm-3\"></div>" : '';
        $append = '';
        $this->selectModal = true;
        return "\$form->field(
                \$model,
                '$attribute'
            )
                    ->widget(
                        DepDrop::class,
                        [
                            'type'=>DepDrop::TYPE_SELECT2,
                            'data'=>($onFullModel::findOne(\$model->$onAttribute)) ? ArrayHelper::map($onFullModel::findOne(\$model->$onAttribute)->$onRelation, '$attributeLabel', 'toString') : [],
                            'options'=>[
                                'id' => '$attribute' . \$owner,
                                'placeholder' => Yii::t('app', 'Select a value...'),
                            ],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => " . ($column->allowNull ? "true" : "false") . ",
                                ],
                            'addon' => (!\$relatedForm || (\$relatedType != RelatedForms::TYPE_MODAL && !\$useModal)) ? [
                                'append' => [
                                    'content' => [
                                        RelatedForms::widget([
                                            'relatedController' => '$foreignController',
                                            'type' => \$relatedTypeForm,
                                            'selector' => '$attribute' . (\$ajax || \$useModal ? '_ajax_' . \$owner : ''),
                                            'primaryKey' => '$foreignPk',
                                            'depend' => true,
                                            'dependOn' => '$onAttribute',
                                            'relation' => '$ajaxRelatedDep',
                                            'relationId' => '$ajaxRelatedDepOn',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                            ],
                            'pluginOptions'=>[
                                'depends' => ['$onAttribute'],
                                'url' => Url::to(['depend', 'on' => '$on', 'onRelation' => '$onRelation']),
                            ]
                        ]
                    )
                ?>
                $append
                <?php";
    }

    /**
     * DropDown-Field Select2 Generator Method / by taktwerk.com
     *
     * Generates code for active field dropdown by using the provider queue
     *
     * @param ColumnSchema $column
     * @param null $model
     *
     * @return mixed|string
     */
    public function activeFieldDropDown(ColumnSchema $column, $model = null)
    {
        if (($comment = $this->extractComments($column)) &&
            isset($comment->inputtype->depend) &&
            isset($comment->inputtype->depend->on) &&
            isset($comment->inputtype->depend->onAttribute) &&
            isset($comment->inputtype->depend->onRelation)
        ) {
            return $this->activeFieldDepend($column, $comment);
        }
        $attribute = $column->name;
        $tableSchema = $this->getTableSchema();

        $foreignLabelAttribute = "toString";
        $fullModel = $this->fetchForeignClass($attribute);
        $foreignPk = $fullModel::primaryKey()[0];
        $reflection = new \ReflectionClass($fullModel);
        $shortModel = $reflection->getShortName();
        $attributeLabel = $this->fetchForeignAttribute($attribute);

        if ($attributeLabel == null) {
            $attributeLabel = $attribute;
        }

        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];
        $foreignController = $this->getForeignController($fullModel);
        $this->selectModal = true;
//        $append = !$this->twoColumnsForm ? "<div class=\"col-sm-3\"></div>" : '';
        $append = '';
        return "\$form->field(
                \$model,
                '$attribute'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => $fullModel::find()->count() > 50 ? null : ArrayHelper::map($fullModel::find()->all(), '$attributeLabel', '$foreignLabelAttribute'),
                            'initValueText' => $fullModel::find()->count() > 50 ? \\yii\\helpers\\ArrayHelper::map($fullModel::find()->andWhere(['$attributeLabel' => \$model->{$attribute}])->all(), '$attributeLabel', '$foreignLabelAttribute') : '',
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select a value...'),
                                'id' => '$attribute' . (\$ajax || \$useModal ? '_ajax_' . \$owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => " . ($column->allowNull ? "true" : "false") . ",
                                ($fullModel::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                ($fullModel::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \\yii\\helpers\\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \\yii\\web\\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \\'" . $shortModel . "\\'
                                        };
                                    }')
                                ],
                            ],
                            'addon' => (!\$relatedForm || (\$relatedType != RelatedForms::TYPE_MODAL && !\$useModal)) ? [
                                'append' => [
                                    'content' => [
                                        RelatedForms::widget([
                                            'relatedController' => '$foreignController',
                                            'type' => \$relatedTypeForm,
                                            'selector' => '$attribute' . (\$ajax || \$useModal ? '_ajax_' . \$owner : ''),
                                            'primaryKey' => '$foreignPk',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    )
                    ?>
                $append
            <?php";
    }

    /**
     * Generates code for active field by using the provider queue.
     * @param $attribute
     * @param null $model
     * @return mixed|string|void
     */
    public function activeField($attribute, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        if ($code !== null) {
            Yii::trace("found provider for '{$attribute}'", __METHOD__);

            return $code;
        } else {
            $column = $this->getColumnByAttribute($attribute);
            if (!$column) {
                return;
            } else {
                return $this->generateActiveField($attribute);
            }
        }
    }

    /**
     * Extract comments from database
     * @param $column
     * @return bool|mixed
     */
    public function extractComments($column)
    {
        $output = json_decode($column->comment);
        if (is_object($output)) {
            return $output;
        }
        return false;
    }

    /**
     * Helper function to get controller name of foreign model for ajax call
     * @param string $model
     * @return string
     */
    protected function getForeignController($model)
    {
        $modelName = $foreignController = substr($model, strrpos($model, '\\') + 1);
        $controllerName = strtolower(Inflector::slug(Inflector::camel2words($modelName), '-'));
        $ns = \yii\helpers\StringHelper::dirname(ltrim($this->controllerClass, '\\'));
        $url = str_replace(['\\app', 'app', '\\controllers'], '', $ns);
        $url = str_replace('\\', '/', $url);
        return $url . '/' . $controllerName;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        $dateConditions = [];
        $conditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_TIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "\$this->applyHashOperator('{$column}', \$query);";
                    break;
                case Schema::TYPE_DATETIME:
                    $dateConditions[] = "\$this->applyDateOperator('{$column}', \$query, true);";
                    break;
                case Schema::TYPE_DATE:
                    $dateConditions[] = "\$this->applyDateOperator('{$column}', \$query);";
                    break;
                default:
                    $likeConditions[] = "\$this->applyLikeOperator('{$column}', \$query);";
                    break;
            }
        }

        if (!empty($hashConditions)) {
            $conditions[] = implode("\n" . str_repeat(' ', 8), $hashConditions);
        }
        if (!empty($likeConditions)) {
            $conditions[] = implode("\n" . str_repeat(' ', 8), $likeConditions);
        }
        if (!empty($dateConditions)) {
            $conditions[] = implode("\n" . str_repeat(' ', 8), $dateConditions);
        }


        return $conditions;
    }

    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][] = $column->name;
                    break;
            }
        }

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[
                [
                    '" . implode("',\n" . str_repeat(' ', 20) . "'", $columns) . "'\n" . str_repeat(' ', 16) . "],
                '$type'
            ]";
        }

        return $rules;
    }

    /**
     * List of primary keys
     * @return array
     */
    public function getPrimaryKeys()
    {
        $schema = $this->getTableSchema();
        $primaryKeys = [];
        foreach ($schema->columns as $column) {
            if ($column->isPrimaryKey) {
                $primaryKeys[] = $column->name;
            }
        }

        return $primaryKeys;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getUploadFields($class = null)
    {
        if ($class === null) {
            $class = $this->modelClass;
        }
        /** @var \yii\db\ActiveRecord $model */
        $model = new $class;
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        if (empty($safeAttributes)) {
            $safeAttributes = $model->getTableSchema()->columnNames;
        }
        $out = [];
        foreach ($safeAttributes as $attribute) {
            $column = ArrayHelper::getValue($this->getTableSchema()->columns, $attribute);
            if (!empty($column) && $this->checkIfUploaded($column)) {
                $out[] = $attribute;
            }
        }
        return $out;
    }

    /**
     * @param ColumnSchema $column
     * @return bool
     */
    public function checkIfUploaded(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_upload|_file)$/i', $column->name) ||
            ($comment && ($comment->inputtype === 'upload' || $comment->inputtype === 'file'))
        ) {
            return true;
        }
        return false;
    }
}
