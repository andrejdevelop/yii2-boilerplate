<?php

namespace taktwerk\yiiboilerplate\background;

use \yii\base\Object;
use \trntv\bus\middlewares\BackgroundCommandTrait;
use \trntv\bus\interfaces\SelfHandlingCommand;
use \trntv\bus\interfaces\BackgroundCommand;

/**
 * https://github.com/trntv/yii2-command-bus
 * Create background command
 * 
 * Setup:
 * 
 * Config file:
 * 'controllerMap' => [
 *   'background-bus' => [
 *       'class' => 'trntv\bus\console\BackgroundBusController',
 *   ]
 *  ],
 *  'components' => [
 *       'commandBus' =>[
 *           ...
 *           'middlewares' => [
 *               [
 *                   'class' => '\trntv\bus\middlewares\BackgroundCommandMiddleware',
 *                   'backgroundHandlerPath' => '@console/yii',
 *                   'backgroundHandlerRoute' => 'background-bus/handle',
 *               ]                
 *           ]
 *       ]        
 *  ],
 * 
 * 
 * Somewhere in application:
 * 
 *  Yii::$app->commandBus->handle(new ReportCommand([
 *   'async' => true,
 *   'someImportantData' => [ // data // ]
 *  ]))
 */

class ReportCommand extends Object implements BackgroundCommand, SelfHandlingCommand
{
    use BackgroundCommandTrait;

    public $someImportantData;

    public function handle($command) {
        // do what you need
    }
}

