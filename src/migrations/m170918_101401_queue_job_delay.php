<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170918_101401_queue_job_delay extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%queue_job}}', 'is_reexecuted_on_error', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
        $this->addColumn('{{%queue_job}}', 'fail_minute_delay', Schema::TYPE_SMALLINT . " DEFAULT 15");
        $this->addColumn('{{%queue_job}}', 'execute_at', Schema::TYPE_DATETIME . " NULL");
        $this->createIndex('queue_job_execution_idx', '{{%queue_job}}', 'execute_at');
    }

    public function down()
    {
        $this->removeColumn('{{%queue_job}}', 'execute_at');
        $this->removeColumn('{{%queue_job}}', 'is_reexecuted_on_error');
        $this->removeColumn('{{%queue_job}}', 'fail_minute_delay');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
