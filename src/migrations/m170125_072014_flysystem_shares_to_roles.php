<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170125_072014_flysystem_shares_to_roles extends TwMigration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%flysystem_role}}',
            [
                'id' => Schema::TYPE_PK . "",
                'fs_id' => Schema::TYPE_INTEGER,
                'role' => Schema::TYPE_STRING,
                'root_path' => Schema::TYPE_STRING,
                'overwrite_path' => ' TINYINT(1) DEFAULT 0',
                'read_only' => ' TINYINT(1) DEFAULT 0',
                'overwrite_access' => ' TINYINT(1) DEFAULT 0',
            ],
            $tableOptions
        );

        $this->createIndex('fs_id_idx', '{{%flysystem_role}}', 'fs_id');
        $this->addForeignKey('flysystem_role_fk_flysystem_id', '{{%flysystem_role}}', 'fs_id', '{{%flysystem}}', 'id');
    }

    public function down()
    {
        echo "m170125_072014_flysystem_shares_to_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
