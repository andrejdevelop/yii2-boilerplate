<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170518_054951_add_missing_profiles extends TwMigration
{
    public function up()
    {
        $users = \app\models\User::find()->all();
        foreach ($users as $user) {
            if (!$user->profile || $user->profile == null) {
                $profile = new \taktwerk\yiiboilerplate\modules\user\models\Profile();
                $profile->user_id = $user->id;
                $profile->name = $user->fullname ? $user->fullname : $user->username;
                $profile->save(false);
            }
        }
    }

    public function down()
    {
        echo "m170518_054951_add_missing_profiles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
