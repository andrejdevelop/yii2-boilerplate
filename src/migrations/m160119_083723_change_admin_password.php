<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m160119_083723_change_admin_password extends TwMigration
{
    public function up()
    {
        $password_hash = Yii::$app->security->generatePasswordHash('passepartout.0', '10');
        $this->update('{{%user}}', ['password_hash' => $password_hash], ['username' => 'admin']);
    }

    public function down()
    {
        echo "m160119_083723_change_admin_password cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
