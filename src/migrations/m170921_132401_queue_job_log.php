<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170921_132401_queue_job_log extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%queue_job}}', 'log', $this->text()->null());
    }

    public function down()
    {
        $this->removeColumn('{{%queue_job}}', 'log');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
