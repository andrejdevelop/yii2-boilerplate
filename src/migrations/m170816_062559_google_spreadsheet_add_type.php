<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170816_062559_google_spreadsheet_add_type extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%google_spreadsheet}}', 'type', "enum('export','import')");
        $this->createIndex('type_idx', '{{%google_spreadsheet}}', 'type');
    }

    public function down()
    {
        echo "m170816_062559_google_spreadsheet_add_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
