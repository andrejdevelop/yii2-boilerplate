<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170814_083011_google_spreadsheet extends TwMigration
{
    public function up()
    {
        if ($this->db->getTableSchema('{{%google_spreadsheet}}', true) === null) {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

            $this->createTable(
                '{{%google_spreadsheet}}',
                [
                    'id' => Schema::TYPE_PK . "",
                    'user_id' => Schema::TYPE_INTEGER,
                    'title' => Schema::TYPE_STRING . "(255) NOT NULL",
                    'url' => Schema::TYPE_STRING . "(255) NOT NULL",
                ],
                $tableOptions
            );

            $this->createIndex('user_idx', '{{%google_spreadsheet}}', 'user_id');
            $this->addForeignKey('google_spreadsheet_fk_user_id', '{{%google_spreadsheet}}', 'user_id', '{{%user}}', 'id');
        }

        $auth = $this->getAuth();
        $permission = $auth->createPermission('app_google-spreadsheet');
        $permission->description = 'Google Spreadsheet controller';
        $auth->add($permission);

        $role = $auth->getRole('Public');
        $auth->addChild($role, $permission);

    }

    public function down()
    {
        echo "m170814_083011_google_spreadsheet cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
