<?php

namespace taktwerk\yiiboilerplate\widget;

use Yii;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\web\View;
use yii\base\InvalidConfigException;

class AjaxRelated extends Modal
{


    public $clientOptions = ['backdrop' => 'static', 'keyboard' => false];

    public $size = Modal::SIZE_LARGE;

    public $toggleButton = [
        'id' => 'toggle-modal',
        'label' => 'New',
        'tag' => 'button',
        'class' => 'btn',
    ];

    public $header = 'Ajax new';

    public $url = null;

    public $selector = null;

    public $disabled = false;

    public $label = null;

    public $update = false;

    public $style;

    public $depend;

    public $dependOn;

    public $relation;

    public $relationId;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        Widget::init();

        if (is_null($this->url)) {
            throw new InvalidConfigException('No url specified for AjaxNew Widget.');
        }

        $this->toggleButton['data-target'] = '#' . $this->id;
        if ($this->disabled)
            $this->toggleButton['disabled'] = 'disabled';
        $this->toggleButton['label'] = $this->label;
        $this->toggleButton['style'] = $this->style;
        $this->initOptions();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo $this->renderToggleButton() . "\n";

        Yii::$app->view->on(View::EVENT_END_BODY, function () {
            echo Html::beginTag('div', $this->options) . "\n";
            echo Html::beginTag('div', ['class' => 'modal-dialog ' . $this->size]) . "\n";
            echo Html::beginTag('div', ['class' => 'modal-content']) . "\n";
            echo $this->renderHeader() . "\n";
            echo $this->renderBodyBegin() . "\n";
            echo "\n" . $this->renderBodyEnd();
            echo "\n" . $this->renderFooter();
            echo "\n" . Html::endTag('div'); // modal-content
            echo "\n" . Html::endTag('div'); // modal-dialog
            echo "\n" . Html::endTag('div');
        });

        $this->registerPlugin('modal');

        $js = <<<JS

$("#{$this->id}").ajaxRelated({
    selector: $('[data-target="#{$this->id}"]').closest("div.input-group").children("select"),
    url: "{$this->url}",
    update: "{$this->update}",
    depend: "{$this->depend}",
    dependOn: "{$this->dependOn}",
    relation: "{$this->relation}",
    relationId: "{$this->relationId}"
});
$(document ).ready(function(){
    if ($('[data-target="#{$this->id}"]').closest("div.input-group").children("select").val() != '') {
        $('[data-target="#{$this->id}"]').prop('disabled',false);
    }
});
JS;
        $this->getView()->registerJs($js);

        \taktwerk\yiiboilerplate\assets\AjaxRelatedAsset::register($this->getView());

    }
}
