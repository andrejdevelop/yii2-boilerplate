<?php
/**
 * Created by PhpStorm.
 * User: ben-g
 * Date: 05.02.2016
 * Time: 08:08
 */

namespace taktwerk\yiiboilerplate;

use yii\filters\auth\AuthMethod;

class TwXAuth extends AuthMethod
{
    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'access-token';


    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->getHeaders()->get('X-Auth-Token');
        if (is_string($accessToken)) {
            $identity = $user->loginByAccessToken($accessToken, get_class($this));
            if ($identity !== null) {
                return $identity;
            }
        }
        if ($accessToken !== null) {
            $this->handleFailure($response);
        }
        return null;
    }
}
