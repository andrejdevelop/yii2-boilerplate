<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/31/2017
 * Time: 1:33 PM
 */

namespace taktwerk\yiiboilerplate\controllers;

use app\models\User;
use dektrium\user\Finder;
use taktwerk\yiiboilerplate\components\dav\auth\Auth;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use taktwerk\yiiboilerplate\components\dav\Directory;
use Sabre\DAV\Server;
use Sabre\HTTP;
use Yii;

class DavController extends Controller
{
    /**
     * 
     */
    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => ['@', '?']
                    ]
                ]
            ]
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $shares = [];
        foreach ($this->registerFs() as $component) {
            $shares[] = new Directory($component);
        }
        $server = new Server($shares);
        $server->setBaseUri('/dav');

        $server->addPlugin(new \Sabre\DAV\Auth\Plugin(new Auth()));
        $server->addPlugin(new \Sabre\DAV\Browser\Plugin());
        $server->addPlugin(new \Sabre\DAV\Browser\GuessContentType());
        $this->getCredentials();
        $server->exec();
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function registerFs()
    {
        $user = Yii::$container->get(Finder::className())->findUserByUsernameOrEmail($this->getCredentials()[0]);
        if (!$user) {
            return [];
        }
        Yii::$app->user->login($user);
        $controller = new ElFinderShareController(
            'elfinder',
            new Module('app'),
            ['roots' => Yii::$app->controllerMap['file']['roots']]
        );
        $controller->init();
        $components = [];
        foreach ($controller->roots as $fs) {
            $components[] = [
                'component' => $fs['component'],
                'name' => $fs['name'],
                'path' => $fs['path'],
            ];
        }
        return $components;
    }

    /**
     * @return array|null
     */
    protected function getCredentials()
    {
        $auth = Yii::$app->request->headers->get('Authorization');

        if (!$auth) {
            return null;
        }

        if (strtolower(substr($auth, 0, 6)) !== 'basic ') {
            return null;
        }

        $credentials = explode(':', base64_decode(substr($auth, 6)), 2);

        if (2 !== count($credentials)) {
            return null;
        }

        return $credentials;
    }
}
