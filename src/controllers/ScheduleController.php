<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */


namespace taktwerk\yiiboilerplate\controllers;

use taktwerk\yiiboilerplate\components\Schedule;
use yii\di\Instance;
use Yii;

class ScheduleController extends \omnilight\scheduling\ScheduleController
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        if ($this->scheduleFile == null) {
            $this->scheduleFile = '@app/config/schedule.php';
        }
        if (Yii::$app->has($this->schedule)) {
            $this->schedule = Instance::ensure($this->schedule, Schedule::className());
        } else {
            $this->schedule = Yii::createObject(Schedule::className());
        }
    }
}
