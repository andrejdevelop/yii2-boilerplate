<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/23/2017
 * Time: 9:01 AM
 */

namespace taktwerk\yiiboilerplate\traits;

use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use Yii;

trait SearchTrait
{
    /**
     * @param $qryString
     * @return array|string
     */
    public function getOperator($qryString)
    {
        $operator = [];
        if (is_array($qryString)) {
            $operator['operator'] = 'OR';
            foreach ($qryString as $operand) {
                $operator['operand'][] = $operand;
            }
        } elseif (strpos($qryString, '<>') === 0) {
            $operator = [
                'operator' => '!=',
                'operand' => str_replace('<>', '', $qryString),
            ];
        } elseif (strpos($qryString, '!=') === 0) {
            $operator = [
                'operator' => '!=',
                'operand' => str_replace('!=', '', $qryString),
            ];
        } elseif (strpos($qryString, '!') === 0) {
            $operator = [
                'operator' => '!=',
                'operand' => str_replace('!', '', $qryString),
            ];
        } elseif (strpos($qryString, '=>') === 0) {
            $operator = [
                'operator' => '>=',
                'operand' => str_replace('=>', '', $qryString),
            ];
        } elseif (strpos($qryString, '>=') === 0) {
            $operator = [
                'operator' => '>=',
                'operand' => str_replace('>=', '', $qryString),
            ];
        } elseif (strpos($qryString, '>') === 0) {
            $operator = [
                'operator' => '>',
                'operand' => str_replace('>', '', $qryString),
            ];
        } elseif (strpos($qryString, '=<') === 0) {
            $operator = [
                'operator' => '<=',
                'operand' => str_replace('=<', '', $qryString),
            ];
        } elseif (strpos($qryString, '<=') === 0) {
            $operator = [
                'operator' => '<=',
                'operand' => str_replace('<=', '', $qryString),
            ];
        } elseif (strpos($qryString, '<') === 0) {
            $operator = [
                'operator' => '<',
                'operand' => str_replace('<', '', $qryString),
            ];
        } elseif (strpos(strtolower($qryString), ' to ') !== false) {
            $ranges = explode(' to ', $qryString);
            $operator = [
                'operator' => 'between',
                'start' => trim($ranges[0]),
                'end' => trim($ranges[1]),
            ];
        } elseif (strpos(strtolower($qryString), ' between ') !== false) {
            $ranges = explode(' between ', $qryString);
            $operator = [
                'operator' => 'between',
                'start' => trim($ranges[0]),
                'end' => trim($ranges[1]),
            ];
        } elseif (strpos($qryString, '...') !== false) {
            $ranges = explode('...', $qryString);
            $operator = [
                'operator' => 'between',
                'start' => trim($ranges[0]),
                'end' => trim($ranges[1]),
            ];
        } elseif (strpos(strtolower($qryString), ' and ') !== false) {
            $operands = explode(' and ', str_replace([' AND ', ' And '], ' and ', $qryString));
            $operator = [
                'operator' => 'AndLike',
                'first' => trim($operands[0]),
                'second' => trim($operands[1]),
            ];
        } elseif (strpos(strtolower($qryString), ' or ') !== false) {
            $operands = explode(' or ', str_replace([' OR ', ' Or '], ' or ', $qryString));
            $operator = [
                'operator' => 'OrLike',
                'first' => trim($operands[0]),
                'second' => trim($operands[1]),
            ];
        } else {
            $operator = '';
        }
        return $operator;
    }

    /**
     * @param $attribute
     * @param $query ActiveQuery
     * @param string $alias
     * @param string $table
     */
    public function applyHashOperator($attribute, &$query, $alias = null, $table = null)
    {
        $operator = $this->getOperator($this->$attribute);
        $attribute_copy = $attribute;
        if ($alias && $table) {
            $attribute = $table.'.'.$alias;
        }
        if (!is_array($operator)) {
            $query->andFilterWhere(
                [
                    $attribute => $this->$attribute_copy
                ]
            );
        } elseif (($operator['operator'] == 'OR')) {
            $query->andFilterWhere(
                [
                    $attribute => $operator['operand']
                ]
            );
        } elseif (($operator['operator'] !== 'between')) {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $attribute,
                    $operator['operand']
                ]
            );
        } elseif (($operator['operator'] == 'AndLike')) {
            $query->andFilterWhere(
                [
                    'AND',
                    [
                        'like',
                        $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $attribute,
                        $operator['second']
                    ]
                ]
            );
        } elseif (($operator['operator'] == 'OrLike')) {
            $query->andFilterWhere(
                [
                    'OR',
                    [
                        'like',
                        $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $attribute,
                        $operator['second']
                    ]
                ]
            );
        } else {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $attribute,
                    $operator['start'],
                    $operator['end']
                ]
            );
        }
    }

    /**
     * @param      $attribute
     * @param      $query
     * @param bool $datetime
     */
    public function applyDateOperator($attribute, &$query, $datetime = false, $alias = null, $table = null)
    {
        if (isset($this->$attribute) && $this->$attribute != '') {
            $attribute_copy = $attribute;
            if ($alias && $table) {
                $attribute = $table.'.'.$alias;
            }
            $format = 'Y-m-d';
            $format .= $datetime ? ' H:i:s' : '';
            $fromFormat = $datetime ?  Yii::$app->formatter->momentJsDateTimeFormat :  Yii::$app->formatter->momentJsDateFormat;
            $date_explode = explode(" TO ", $this->$attribute_copy);
            $date1 = trim($date_explode[0]);
            $convert = \DateTime::createFromFormat(Helper::convertMomentToPHFormat($fromFormat), $date1);
            $date1 = $convert->format($format);
            $date2 = trim($date_explode[1]);
            $convert = \DateTime::createFromFormat(Helper::convertMomentToPHFormat($fromFormat), $date2);
            $date2 = $convert->format($format);
            $query->andFilterWhere(
                [
                    'between',
                    $attribute,
                    $date1,
                    $date2
                ]
            );
        }
    }

    /**
     * @param $attribute
     * @param $query ActiveQuery
     */
    public function applyLikeOperator($attribute, &$query, $alias = null, $table = null)
    {
        $operator = $this->getOperator($this->$attribute);
        $attribute_copy = $attribute;
        if ($alias && $table) {
            $attribute = $table.'.'.$alias;
        }
        if (!is_array($operator)) {
            $query->andFilterWhere(
                [
                    'like',
                    $attribute,
                    $this->$attribute_copy
                ]
            );
        } elseif (($operator['operator'] == 'AndLike')) {
            $query->andFilterWhere(
                [
                    'AND',
                    [
                        'like',
                        $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $attribute,
                        $operator['second']
                    ]
                ]
            );
        } elseif (($operator['operator'] == 'OrLike')) {
            $query->andFilterWhere(
                [
                    'OR',
                    [
                        'like',
                        $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $attribute,
                        $operator['second']
                    ]
                ]
            );
        } elseif (in_array($operator['operator'], ['!='])) {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $attribute,
                    $operator['operand']
                ]
            );
        }
    }

    /**
     * @param $model
     * @param $params
     */
    public function parseSearchParams($model, &$params)
    {
        if ($params['reset']) {
            Yii::$app->get('userUi')->set('Grid', $model, $params);
        } else {
            Yii::$app->get('userUi')->set(
                'Grid',
                $model,
                array_merge((Yii::$app->get('userUi')->get('Grid', $model) ?: []), $params)
            );
        }
        $params =  Yii::$app->get('userUi')->enableSave ?  Yii::$app->get('userUi')->get('Grid', $model) : $params;
    }

    /**
     * @param $model
     * @return array
     */
    public function parseSortParams($model)
    {
        $order = SORT_ASC;
        $attribute = self::primaryKey()[0];
        /*$value = Yii::$app->get('userUi')->get('Grid', $model)['sort'];
        if (isset($value)) {
            $attribute =  Yii::$app->get('userUi')->get('Grid', $model)['sort'];
            if (strncmp($attribute, '-', 1) === 0) {
                $order = SORT_DESC;
                $attribute = substr($attribute, 1);
            }
        }*/
        return [$attribute => $order];
    }

    /**
     * @param $model
     * @return int
     */
    public function parsePageParams($model)
    {
        return Yii::$app->request->get('page', 1);
        $value = Yii::$app->get('userUi')->get('Grid', $model)['page'];
        return isset($value) ? $value : Yii::$app->request->get('page', 1);
    }

    /**
     * Determine the page size for the grid based on the model
     * @param $model
     * @return int
     */
    public function parsePageSize($model)
    {
        // First try using the user's userUi model
        return  Yii::$app->request->get('pageSize', 20);
        $value = Yii::$app->get('userUi')->get('Grid', $model)['pageSize'];
        if (Yii::$app->get('userUi')->enableSave && isset($value)) {
            return  Yii::$app->get('userUi')->get('Grid', $model)['pageSize'];
        } elseif (Yii::$app->hasModule('user-parameter') && !empty(UserParameter::get('pageSize'))) {
            // Using the UserParameter module
            return  Yii::$app->request->get('pageSize', UserParameter::get('pageSize'));
        }
        // Default value of 20 if no user or app defined default.
        return 20;
    }
}
