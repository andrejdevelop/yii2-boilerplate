<?php

namespace taktwerk\yiiboilerplate\models;

use Yii;
use \taktwerk\yiiboilerplate\models\base\GoogleSpreadsheet as BaseGoogleSpreadsheet;

/**
 * This is the model class for table "google_spreadsheet".
 */
class GoogleSpreadsheet extends BaseGoogleSpreadsheet
{

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * @return bool
     */
    public static function isMenuVisible()
    {
        if (Yii::$app->user->can('Administrator')) {
            return true;
        }
        return (count(self::find()->andWhere(['user_id' => Yii::$app->user->id])->all()) > 0);
    }
}
