<?php
/**
 * /srv/www/taktwerk-erp/src/../runtime/giiant/e0080b9d6ffa35acb85312bf99a557f2
 *
 * @package default
 */


namespace taktwerk\yiiboilerplate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\models\GoogleSpreadsheet;

/**
 * GoogleSpreadsheetSearch represents the model behind the search form about `taktwerk\yiiboilerplate\models\GoogleSpreadsheet`.
 */
class GoogleSpreadsheetSearch extends GoogleSpreadsheet
{

	use SearchTrait;

	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'title',
					'url',
					'created_by',
					'created_at',
					'updated_by',
					'updated_at',
					'deleted_by',
					'deleted_at'
				],
				'safe'
			],
		];
	}


	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}


	/**
	 * Creates data provider instance with search query applied
	 *
	 *
	 * @param array   $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = GoogleSpreadsheet::find();

		$this->parseSearchParams(GoogleSpreadsheet::className(), $params);

		$dataProvider = new ActiveDataProvider([
				'query' => $query,
				'sort' => [
					'defaultOrder' => $this->parseSortParams(GoogleSpreadsheet::className()),
				],
				'pagination' => [
					'pageSize' => $this->parsePageSize(GoogleSpreadsheet::className()),
					'params' => [
						'page' => $this->parsePageParams(GoogleSpreadsheet::className()),
					]
				],
			]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$this->applyHashOperator('id', $query);
		$this->applyHashOperator('user_id', $query);
		$this->applyHashOperator('created_by', $query);
		$this->applyHashOperator('updated_by', $query);
		$this->applyHashOperator('deleted_by', $query);
		$this->applyLikeOperator('title', $query);
		$this->applyLikeOperator('url', $query);
		$this->applyDateOperator('created_at', $query, true);
		$this->applyDateOperator('updated_at', $query, true);
		$this->applyDateOperator('deleted_at', $query, true);
		if (!Yii::$app->getUser()->can('Authority')) {
			$query->andFilterWhere(['user_id' => Yii::$app->user->id]);
		}
		$query->andFilterWhere(['type' => self::TYPE_EXPORT]);
		return $dataProvider;
	}


	/**
	 *
	 * @return int
	 */
	public function getPageSize() {
		return $this->parsePageSize(GoogleSpreadsheet::className());
	}


}
