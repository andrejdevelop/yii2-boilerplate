(function ($) {

    $.fn.ajaxRelated = function (options) {

        var settings = $.extend({
            selector: null,
            url: null,
            update: false,
        }, options);

        var self = $(this);

        return this.on('submit', 'form', function () {
            var form = $(this);
            $.post(
                form.attr("action"),
                form.serialize()
                )
                .done(function (data) {
                    if ('object' == typeof data) {
                        self.modal('hide');
                        if (!settings.update) {
                            $(settings.selector)
                                .append($('<option />', {value: data.id}).text(data.label))
                                .val(data.id);
                        } else {
                            //@TODO find way to change and update existing select with new text
                            var optionSelected = $(settings.selector).find("option:selected");
                            optionSelected.text(data.label).change();
                        }
                        $(settings.selector).val(data.id).change();
                    } else { // html
                        form.find('.modal-body').html($('<div>' + data + '</div>'));
                    }
                })
                .fail(function () {
                    console.log("server error");
                });
            return false;
        }).on('hidden.bs.modal', function () {
            $(this).find('.modal-body').html('');

        }).on('show.bs.modal', function () {
            var _self = this;
            var url;
            ajaxindicatorstart();
            if (settings.update) {
                $.get(settings.url + '&id=' + $(settings.selector).val(), function (data) {
                        $(_self).find('.modal-body').html(data);
                    })
                    .done(function (data) {
                        ajaxindicatorend();
                    });
            } else {
                if (settings.depend == true) {
                    url = settings.url + '&depend=' + settings.depend + '&relation=' + settings.relation + '&relationId=' + settings.relationId + '&relationIdValue=' + $('#' + settings.dependOn).val()
                } else {
                    url = settings.url;
                }
                $.get(url, function (data) {
                        $(_self).find('.modal-body').html(data);
                    })
                    .done(function (data) {
                        ajaxindicatorend();
                    });
            }
        });

    };

}(jQuery));
function ajaxindicatorend() {
    $('#resultLoading').remove();
    $('body').css('cursor', 'auto');

};
function ajaxindicatorstart() {
    if ($('body').find('#resultLoading').attr('id') != 'resultLoading') {
        $('body').append('<div id="resultLoading" style="display:none"><div><i class="fa fa-spinner fa-spin fa-3x fa-fw" aria-hidden="true"></i></div><div class="bg"></div></div>');
    }

    $('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    $('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    $('#resultLoading>div:first').css({
        'width': '250px',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    $('#resultLoading .bg').height('100%');
    $('#resultLoading').fadeIn(300);
    $('body').css('cursor', 'wait');
}
