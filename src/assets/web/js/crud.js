$(document).ready(function () {
    $(document.body).on("change",".select2-hidden-accessible", function(){
        if (($(this).val() != null) && ($(this).val() != '')) {
            $(this).parent('div').find('button.edit_select').removeAttr('disabled');
        } else {
            $(this).parent('div').find('button.edit_select').attr('disabled', 'disabled');
        }
    });
    $('select').each(function () {
        if ($(this).val()) {
            $(this).parent('div').find('button.edit_select').removeAttr('disabled');
            $(this).parent('div').find('button.add_select').removeAttr('disabled');
        }
    })
})

function deleteMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    $.post(url,{pk : keys},
        function (){
            if (useModal != true) {
                $.pjax.reload({container: "#" + gridViewKey + "-pjax-container"});
            }
        }
    );
}

function editMultiple(){
    $('#edit-multiple').modal('show');
}