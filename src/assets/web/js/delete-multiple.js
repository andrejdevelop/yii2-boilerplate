function deleteMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    $.post(url,{pk : keys},
        function (){
            if (useModal != true) {
                $.pjax.reload({container: "#" + gridViewKey + "-pjax-container"});
            }
        }
    );
}