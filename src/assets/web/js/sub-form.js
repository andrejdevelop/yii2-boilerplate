/*
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by Nikola on 2/13/2017.
 */

function ajaxindicatorend() {
    $('#resultLoading').remove();
    $('body').css('cursor', 'auto');

};
function ajaxindicatorstart() {
    if ($('body').find('#resultLoading').attr('id') != 'resultLoading') {
        $('body').append('<div id="resultLoading" style="display:none"><div><i class="fa fa-spinner fa-spin fa-3x fa-fw" aria-hidden="true"></i></div><div class="bg"></div></div>');
    }

    $('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    $('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    $('#resultLoading>div:first').css({
        'width': '250px',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    $('#resultLoading .bg').height('100%');
    $('#resultLoading').fadeIn(300);
    $('body').css('cursor', 'wait');
}
initClickEvent = function() {
    $('.btn-sub-form').on('click', function () {
        var formDiv = $(this).closest("div.form-group"),
            elementDiv = formDiv.children("div.input-group"),
            selector = elementDiv.children('select'),
            divSubForm = formDiv.siblings('div.sub-form'),
            update = ($(this).attr('update') === undefined) ? false : true,
            depend = ($(this).attr('depend') === undefined) ? false : true,
            icon = $(this).children('span'),
            siblingButton = $(this).closest('div').siblings('div').children('button.btn-sub-form'),
            siblingIcon = siblingButton.children('span'),
            defaultIcon = 'glyphicon glyphicon-' + (update ? 'pencil' : 'plus'),
            defaultSiblingIcon = 'glyphicon glyphicon-' + (update ? 'plus' : 'pencil');


        // Toggle div and buttons
        if ($(divSubForm).is(':visible') && siblingIcon.hasClass('glyphicon glyphicon-minus')) {
            // If form is showed and opened by other
            $(icon).removeClass();
            $(icon).addClass('glyphicon glyphicon-minus');
            $(siblingIcon).removeClass();
            $(siblingIcon).addClass(defaultSiblingIcon);
            loadSubForm(selector, update, depend);
        } else if ($(divSubForm).is(':visible') && siblingIcon.hasClass(defaultSiblingIcon)) {
            $(icon).removeClass();
            $(icon).addClass(defaultIcon);
            divSubForm.slideUp();
        } else {
            $(icon).removeClass();
            $(icon).addClass('glyphicon glyphicon-minus');
            loadSubForm(selector, update, depend);
        }
    });
};
//$('.btn-sub-form').on('click', function () {
//    var formDiv = $(this).closest("div.form-group"),
//        elementDiv = formDiv.children("div.input-group"),
//        selector = elementDiv.children('select'),
//        divSubForm = formDiv.siblings('div.sub-form'),
//        update = ($(this).attr('update') === undefined) ? false : true,
//        depend = ($(this).attr('depend') === undefined) ? false : true,
//        icon = $(this).children('span'),
//        siblingButton = $(this).closest('div').siblings('div').children('button.btn-sub-form'),
//        siblingIcon = siblingButton.children('span'),
//        defaultIcon = 'glyphicon glyphicon-' + (update ? 'pencil' : 'plus'),
//        defaultSiblingIcon = 'glyphicon glyphicon-' + (update ? 'plus' : 'pencil');
//
//
//    // Toggle div and buttons
//    if ($(divSubForm).is(':visible') && siblingIcon.hasClass('glyphicon glyphicon-minus')) {
//        // If form is showed and opened by other
//        $(icon).removeClass();
//        $(icon).addClass('glyphicon glyphicon-minus');
//        $(siblingIcon).removeClass();
//        $(siblingIcon).addClass(defaultSiblingIcon);
//        loadSubForm(selector, update, depend);
//    } else if ($(divSubForm).is(':visible') && siblingIcon.hasClass(defaultSiblingIcon)) {
//        $(icon).removeClass();
//        $(icon).addClass(defaultIcon);
//        divSubForm.slideUp();
//    } else {
//        $(icon).removeClass();
//        $(icon).addClass('glyphicon glyphicon-minus');
//        loadSubForm(selector, update, depend);
//    }
//});

function loadSubForm(selector, update, depend) {
    var id = $(selector).val(),
        elementDiv = selector.closest('div.form-group'),
        divSubForm = elementDiv.siblings('div.sub-form'),
        button = elementDiv.find('button.btn-sub-form')[0],
        buttonCreate = elementDiv.find('button.btn-sub-form')[1],
        url = $(button).attr('url'),
        pk = $(button).attr('pk'),
        relation = $(buttonCreate).attr('relation'),
        relationId = $(buttonCreate).attr('relationId'),
        dependOn = $(buttonCreate).attr('dependOn');

    divSubForm.html('');
    ajaxindicatorstart();
    if (update === true) {
        url = url + (url.indexOf('?') !== -1 ? '&' : '?') + pk + '=' + id;
    } else {
        if (depend) {
            url = url + '&depend=' + depend + '&relation=' + relation + '&relationId=' + relationId + '&relationIdValue=' + $('#' + dependOn).val()
        }
    }
    $.get(url, function (data) {
            divSubForm.html(data);
        })
        .done(function (data) {
            ajaxindicatorend();
            $(divSubForm).slideDown();
            initClickEvent();
        });
}
$(document).ready(function(){
    initClickEvent();
});


